﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class LICH_SU_KIEM_TRA_THIET_BI
    {
        public int MA_BO_THIET_BI { get; set; }
        public string TEN_BO_THIET_BI { get; set; }
        public DateTime NGAY { get; set; }
        public string GHI_CHU { get; set; }

        public int IS_HOAN_TAT { get; set; }
        public int ID { get; set; }
    }
}
