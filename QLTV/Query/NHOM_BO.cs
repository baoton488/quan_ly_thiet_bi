﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class NHOM_BO
    {
        public int NHOM_BO_ID { get; set; }
        public string NHOM_BO_TEN { get; set; }
        public bool SU_DUNG { get; set; }
    }
}
