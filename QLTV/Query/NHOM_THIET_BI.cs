﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class NHOM_THIET_BI
    {
        public int MA_NTB { get; set; }
        public string TEN_NTB { get; set; }
        public int SU_DUNG { get; set; }
    }
}
