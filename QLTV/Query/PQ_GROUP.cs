﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class PQ_GROUP
    {
        public int GROUP_ID { get; set; }
        public string GROUP_MA { get; set; }
        public string GROUP_TEN { get; set; }
        public bool BIKHOA { get; set; }
        public string LY_DO_KHOA { get; set; }
        public bool SU_DUNG { get; set; }
        public DateTime NGAY_TAO { get; set; }
        public string OPTION1 { get; set; }
    }
}
