﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class QueryNS_NhanVien
    {
        public string NV_ID = "@NV_ID";
        public string DON_VI_ID = "@DON_VI_ID";
        public string NV_MA = "@NV_MA";
        public string NV_TEN = "@NV_TEN";
        public string NGAY_VAO_LAM = "@NGAY_VAO_LAM";
        public string TINH_TRANG = "@TINH_TRANG";
        public string SU_DUNG = "@SU_DUNG";
        public string GHI_CHU = "@GHI_CHU";
        public string queryupdatens_nhanvien()
        {
            string sql = "update ns_nhan_vien set NV_TEN=" + NV_TEN + ",NGAY_VAO_LAM = " + NGAY_VAO_LAM + ",SU_DUNG=1, GHI_CHU = " + GHI_CHU + " where NV_ID=" + NV_ID + "";
            //string sql = "update nhom_hang set NHOM_TEN=@NHOM_TEN,DON_GIA_VON=@DON_GIA_VON,DON_GIA_MUA=@DON_GIA_MUA,DON_GIA_BAN=@DON_GIA_BAN,DON_GIA_CAM=@DON_GIA_CAM where NHOMHANGID=@NHOMHANGID";
            return sql;
        }

        public string queryaddns_nhanvien()
        {
            string sql = "insert into ns_nhan_vien(NV_TEN,DON_VI_ID,SU_DUNG,GHI_CHU)" +
                                         " values(" + NV_TEN + "," + DON_VI_ID + ",'1'," + GHI_CHU + ") ";
            return sql;
        }

        public string queryloadns_nhanvien()
        {
            string sql = "select * from ns_nhan_vien where su_dung = 1";
            return sql;
        }

        public string querydeletens_nhanvien()
        {
            string sql = "update ns_nhan_vien set SU_DUNG = 0 where NV_ID = " + NV_ID + " ";
            return sql;
        }

    }
}
