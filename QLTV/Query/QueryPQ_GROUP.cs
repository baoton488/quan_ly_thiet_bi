﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class QueryPQ_GROUP
    {
        Connect cn = new Connect();
        public string GROUP_ID = "@GROUP_ID";
        public string GROUP_MA = "@GROUP_MA";
        public string GROUP_TEN = "@GROUP_TEN";
        public string BIKHOA = "@BIKHOA";
        public string LY_DO_KHOA = "@LY_DO_KHOA";
        public string SU_DUNG = "@SU_DUNG";
        public string NGAY_TAO = "@NGAY_TAO";
        public string queryupdatepq_group()
        {
            string sql = "update PQ_GROUP set GROUP_TEN=" + GROUP_TEN + ",LY_DO_KHOA =" + LY_DO_KHOA + ",BIKHOA=" + BIKHOA + ",SU_DUNG=1 where GROUP_ID=" + GROUP_ID + "";
            //string sql = "update nhom_hang set NHOM_TEN=@NHOM_TEN,DON_GIA_VON=@DON_GIA_VON,DON_GIA_MUA=@DON_GIA_MUA,DON_GIA_BAN=@DON_GIA_BAN,DON_GIA_CAM=@DON_GIA_CAM where NHOMHANGID=@NHOMHANGID";
            return sql;
        }

        public string queryaddpq_group()
        {
            string sql = "insert into PQ_GROUP(GROUP_MA,GROUP_TEN,BIKHOA,LY_DO_KHOA,SU_DUNG)" +
                                         " values("+GROUP_MA+"," + GROUP_TEN + ","+BIKHOA+","+LY_DO_KHOA+",'1') ";
            return sql;
        }

        public string queryloadpq_group()
        {
            string sql = "select GROUP_ID,GROUP_MA,GROUP_TEN,BIKHOA,LY_DO_KHOA,IF(SU_DUNG = True, '1', N'0') AS SU_DUNG,NGAY_TAO from PQ_GROUP where su_dung = 1";
            return sql;
        }

        public string querydeletepq_group()
        {
            string sql = "update PQ_GROUP set SU_DUNG = 0 where GROUP_ID = " + GROUP_ID + " ";
            return sql;
        }
        public DataTable getDataTable()
        {
            cn.openconnection();
            MySqlCommand cmd = new MySqlCommand("select GROUP_ID,GROUP_MA,GROUP_TEN from PQ_GROUP where su_dung = 1", cn.conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch { }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
            return dt;
        }
    }
}
