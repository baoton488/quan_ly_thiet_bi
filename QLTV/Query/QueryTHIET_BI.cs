﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class QueryTHIET_BI
    {
        Connect cn = new Connect();
        public string MA_THIET_BI = "@MA_THIET_BI";
        public string THIET_BI_MA = "@THIET_BI_MA";
        public string MA_BO = "@MA_BO";
        public string MA_NTB = "@MA_NTB";
        public string TEN_THIET_BI = "@TEN_THIET_BI";
        public string QUY_CACH = "@QUY_CACH";
        public string NGAY = "@NGAY";
        public string GHI_CHU = "@GHI_CHU";
        public string ANH_THIET_BI = "@ANH_THIET_BI";
        public string SU_DUNG = "@SU_DUNG";
        public string VI_TRI_KHO = "@VI_TRI_KHO";
        public string NHOM_BO_ID = "@NHOM_BO_ID";
        public string ID = "@ID";
        public string queryupdatethietbi()
        {
            string sql = "update THIET_BI set MA_BO=" + MA_BO + ",TEN_THIET_BI= " + TEN_THIET_BI + ",GHI_CHU = " + GHI_CHU + ",QUY_CACH = " + QUY_CACH + ",MA_NTB = " + MA_NTB + ", THIET_BI_MA = "+THIET_BI_MA+" where MA_THIET_BI=" + MA_THIET_BI + "";
            //string sql = "update nhom_hang set NHOM_TEN=@NHOM_TEN,DON_GIA_VON=@DON_GIA_VON,DON_GIA_MUA=@DON_GIA_MUA,DON_GIA_BAN=@DON_GIA_BAN,DON_GIA_CAM=@DON_GIA_CAM where NHOMHANGID=@NHOMHANGID";
            return sql;
        }

        public string queryaddthietbi()
        {
            string sql = "insert into THIET_BI(MA_BO,THIET_BI_MA,TEN_THIET_BI,GHI_CHU,QUY_CACH,SU_DUNG,NGAY,MA_NTB,VI_TRI_KHO)" +
                                         " values(" + MA_BO + "," + THIET_BI_MA + "," + TEN_THIET_BI + "," + GHI_CHU + "," + QUY_CACH + ",'1',now()," + MA_NTB + "," + VI_TRI_KHO + ") ";
            return sql;
        }

        public string queryloadchucvu()
        {
            string sql = "select * from ns_chuc_vu where su_dung = 1";
            return sql;
        }

        public string querydeletethietbi()
        {
            string sql = "DELETE FROM `THIET_BI` WHERE `MA_THIET_BI` = "+MA_THIET_BI+"";
            return sql;
        }
    }
}
