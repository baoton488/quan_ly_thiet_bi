﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class QueryNHAN_VIEN
    {
        Connect cn = new Connect();
        public string MA_NHAN_VIEN = "@MA_NHAN_VIEN";
        public string TEN_NHAN_VIEN = "@TEN_NHAN_VIEN";
        public string GIOI_TINH = "@GIOI_TINH";
        public string NGAY_SINH = "@NGAY_SINH";
        public string GHI_CHU = "@GHI_CHU";
        public string SU_DUNG = "@SU_DUNG";
        public string queryupdatenhanvien()
        {
            string sql = "update NHAN_VIEN set TEN_NHAN_VIEN=" + TEN_NHAN_VIEN + ",GIOI_TINH= " + GIOI_TINH + ",GHI_CHU = " + GHI_CHU + " ,NGAY_SINH = " + NGAY_SINH + " where MA_NHAN_VIEN=" + MA_NHAN_VIEN + "";
            return sql;
        }

        public string queryaddnhanvien()
        {
            string sql = "insert into NHAN_VIEN(TEN_NHAN_VIEN,GIOI_TINH,SU_DUNG,GHI_CHU,NGAY_SINH)" +
                                         " values(" + TEN_NHAN_VIEN + "," + GIOI_TINH + ",'1',"+GHI_CHU+","+NGAY_SINH+") ";
            return sql;
        }

        public string queryloadchucvu()
        {
            string sql = "select * from ns_chuc_vu where su_dung = 1";
            return sql;
        }

        public string querydeletenhanvien()
        {
            string sql = "update NHAN_VIEN set SU_DUNG = 0 where MA_NHAN_VIEN = " + MA_NHAN_VIEN + " ";
            return sql;
        }
        public DataTable getDataTable()
        {
            cn.openconnection();
            MySqlCommand cmd = new MySqlCommand("select * from ns_chuc_vu where su_dung = 1", cn.conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch { }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
            return dt;
        }
    }
}
