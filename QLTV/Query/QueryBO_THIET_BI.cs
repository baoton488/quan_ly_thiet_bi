﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class QueryBO_THIET_BI
    {
        Connect cn = new Connect();
        public string MA_BO = "@MA_BO";
        public string TEN_BO = "@TEN_BO";
        public string GHI_CHU = "@GHI_CHU";
        public string MUC_DO_SU_DUNG = "@MUC_DO_SU_DUNG";
        public string NGAY_SU_DUNG = "@NGAY_SU_DUNG";
        public string SU_DUNG = "@SU_DUNG";
        public string NHOM_BO_ID = "@NHOM_BO_ID";
        public string queryupdatebothietbi()
        {
            string sql = "update BO_THIET_BI set TEN_BO=" + TEN_BO + ",GHI_CHU = " + GHI_CHU + ",NHOM_BO_ID = " + NHOM_BO_ID + " where MA_BO=" + MA_BO + "";
            //string sql = "update nhom_hang set NHOM_TEN=@NHOM_TEN,DON_GIA_VON=@DON_GIA_VON,DON_GIA_MUA=@DON_GIA_MUA,DON_GIA_BAN=@DON_GIA_BAN,DON_GIA_CAM=@DON_GIA_CAM where NHOMHANGID=@NHOMHANGID";
            return sql;
        }

        public string queryaddbothietbi()
        {
            string sql = "insert into BO_THIET_BI(TEN_BO,GHI_CHU,SU_DUNG,NHOM_BO_ID)" +
                                         " values(" + TEN_BO + "," + GHI_CHU + ",'1','1') ";
            return sql;
        }

        public string queryloadchucvu()
        {
            string sql = "select * from ns_chuc_vu where su_dung = 1";
            return sql;
        }

        public string querydeletebothietbi()
        {
            string sql = "update BO_THIET_BI set SU_DUNG = 0 where MA_BO = " + MA_BO + " ";
            return sql;
        }
        public DataTable getDataTable()
        {
            cn.openconnection();
            MySqlCommand cmd = new MySqlCommand("select * from ns_chuc_vu where su_dung = 1", cn.conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch { }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
            return dt;
        }
    }
}
