﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class NHAN_VIEN
    {
        public int MA_NHAN_VIEN { get; set; }
        public string TEN_NHAN_VIEN { get; set; }
        public bool GIOI_TINH { get; set; }
        public DateTime NGAY_SINH { get; set; }
        public string GHI_CHU { get; set; }
        public int SU_DUNG { get; set; }
    }
}
