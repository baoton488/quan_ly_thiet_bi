﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class QueryPQ_User
    {
        private static QueryPQ_User m_intance;
        public int user_id;
        public string ten_dn;
        public string GROUP_TEN;

        public string USER_ID = "@USER_ID";
        public string USER_MA = "@USER_MA";
        public string USER_TEN = "@USER_TEN";
        public string MAT_KHAU = "@MAT_KHAU";
        public string BIKHOA = "@BIKHOA";
        public string LY_DO_KHOA = "@LY_DO_KHOA";
        public string NGAY_TAO = "@NGAY_TAO";
        public string GROUP_ID = "@GROUP_ID";
        //public string NV_ID = "@NV_ID";
        public string SU_DUNG = "@SU_DUNG";
        public string MA_NHAN_VIEN = "@MA_NHAN_VIEN";
        public string queryupdatepq_user()
        {
            string sql = "update PQ_USER set GROUP_ID = " + GROUP_ID + ",USER_TEN=" + USER_TEN + ",MAT_KHAU= " + MAT_KHAU + ",LY_DO_KHOA =" + LY_DO_KHOA + ",BIKHOA=" + BIKHOA + ",SU_DUNG=1 where USER_ID=" + USER_ID + "";
            //string sql = "update nhom_hang set NHOM_TEN=@NHOM_TEN,DON_GIA_VON=@DON_GIA_VON,DON_GIA_MUA=@DON_GIA_MUA,DON_GIA_BAN=@DON_GIA_BAN,DON_GIA_CAM=@DON_GIA_CAM where NHOMHANGID=@NHOMHANGID";
            return sql;
        }

        public string queryaddpq_user()
        {
            string sql = "insert into PQ_USER(GROUP_ID,USER_MA,USER_TEN,MAT_KHAU,BIKHOA,LY_DO_KHOA,NGAY_TAO,SU_DUNG,MA_NHAN_VIEN)" +
                                         " values(" + GROUP_ID + "," + USER_MA + "," + USER_TEN + "," + MAT_KHAU + "," + BIKHOA + "," + LY_DO_KHOA + "," + NGAY_TAO + ",'1'," + MA_NHAN_VIEN + ") ";
            return sql;
        }

        public string queryloadpq_user()
        {
            string sql = "select * from PQ_USER where su_dung = 1";
            return sql;
        }

        public string querydeletepq_user()
        {
            string sql = "update PQ_USER set SU_DUNG = 0 where USER_ID = " + USER_ID + "";
            return sql;
        }

        public static QueryPQ_User intance(){
        if(m_intance == null)
        {
            m_intance = new QueryPQ_User();
        }
            return m_intance;
        }
    }
}
