﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class THIET_BI
    {
        public int MA_THIET_BI { get; set; }
        public int MA_NTB { get; set; }
        public int MA_BO { get; set; }
        public int NHOM_BO_ID { get; set; }
        public string TEN_THIET_BI { get; set; }
        public DateTime NGAY { get; set; }
        public string GHI_CHU { get; set; }
        public string ANH_THIET_BI { get; set; }
        public string QUY_CACH { get; set; }
        public int SU_DUNG { get; set; }
        public int SO_LAN_SU_DUNG { get; set; }
        public string VI_TRI_KHO { get; set; }
        public string THIET_BI_MA { get; set; }
        public int IS_SU_DUNG { get; set; }
    }
}
