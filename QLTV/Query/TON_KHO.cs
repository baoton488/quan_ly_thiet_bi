﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class TON_KHO
    {
        public int MA_THIET_BI { get; set; }
        public int MA_BO { get; set; }
        public int SL_TON { get; set; }
        public string THIET_BI_MA { get; set; }
    }
}
