﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class BO_THIET_BI1
    {
        public int MA_BO { get; set; }
        public string TEN_BO { get; set; }
        public string GHI_CHU { get; set; }
        public STATUS MUC_DO_SU_DUNG { get; set; }
        public DateTime NGAY_SU_DUNG { get; set; }
        public int SU_DUNG { get; set; }
        public enum STATUS 
        {
            DANG_MO = 1,
            CHO_RUA,
            CHO_HAP,
            HU,
            DANG_SUA_CHUA            
        }


    }
}
