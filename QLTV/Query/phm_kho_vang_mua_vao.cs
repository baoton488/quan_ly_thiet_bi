﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLTV.Query
{
    public class phm_kho_vang_mua_vao
    {
        public int KHO_ID { get; set; }
        public int NHOMHANGID { get; set; }
        public string MA_HANG_HOA { get; set; }
        public string TEN_HANG_HOA { get; set; }
        public int DVT_ID { get; set; }
        public int NHOM_ID { get; set; }
        public int SO_LUONG { get; set; }
        public string PHIEU_MA { get; set; }
        public DateTime NGAY_NHAP { get; set; }
        public DateTime NGAY_PHIEU { get; set; }
        public int KHACH_HANG_ID { get; set; }
        public decimal CAN_TONG { get; set; }
        public decimal TL_HOT { get; set; }
        public decimal DON_GIA { get; set; }
        public int DA_XUAT { get; set; }
        public int SU_DUNG { get; set; }
        public string GHI_CHU { get; set; }
        public decimal TL_LOC { get; set; }
        public decimal TL_THUC { get; set; }
        public decimal THANH_TIEN { get; set; }

        public phm_kho_vang_mua_vao() { }

        public phm_kho_vang_mua_vao(int kho_id, string ten_hang_hoa, int nhomhangid, int so_luong, string phieu_ma, DateTime ngay_nhap, DateTime ngay_phieu, int khach_hang_id, decimal can_tong, decimal tl_hot, decimal don_gia, int da_xuat, int su_dung, string ghi_chu, decimal tl_loc)
        {
            KHO_ID = kho_id;
            TEN_HANG_HOA = ten_hang_hoa;
            NHOMHANGID = nhomhangid;
            SO_LUONG = so_luong;
            PHIEU_MA = phieu_ma;
            NGAY_NHAP = ngay_nhap;
            NGAY_PHIEU = ngay_phieu;
            KHACH_HANG_ID = khach_hang_id;
            CAN_TONG = can_tong;
            TL_HOT = tl_hot;
            DON_GIA = don_gia;
            DA_XUAT = da_xuat;
            SU_DUNG = su_dung;
            GHI_CHU = ghi_chu;
            TL_LOC = tl_loc;
        }
    }
}
