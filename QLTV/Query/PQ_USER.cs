﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
	public class PQ_USER
	{
		public int USER_ID { get; set; }
		public int GROUP_ID { get; set; }
		public int MA_NHAN_VIEN { get; set; }
		public string USER_MA { get; set; }
		public string USER_TEN{ get; set; }
		public string MAT_KHAU { get; set; }
		public bool BIKHOA { get; set; }
		public string LY_DO_KHOA_national { get; set; }
		public DateTime NGAY_TAO { get; set; }
		public bool SU_DUNG { get; set; }
		public string REALM { get; set; }
		public string EMAIL { get; set; }
		public string EMAILVERIFIED { get; set; }
		public string VERIFICATIONTOKEN { get; set; }
		public string MAC { get; set; }
		public string OPTION1 { get; set; }
	}
}
