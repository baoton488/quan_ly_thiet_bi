﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class NHAT_KY_SU_DUNG_CHI_TIET
    {
        public int ID_NK_SD { get; set; }
        public int MA_BO { get; set; }
        public string TEN_BO { get; set; }
        public int IS_DAT { get; set; }
        public DateTime NGAY_UPDATE { get; set; }
        public int TRANG_THAI { get; set; }
    }
}
