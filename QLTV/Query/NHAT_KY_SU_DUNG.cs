﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class NHAT_KY_SU_DUNG
    {
        public int ID_NK_SD { get; set; }
        public int USER_ID { get; set; }
        public string TEN_CA_MO { get; set; }
        public DateTime NGAY { get; set; }
        public string TEN_BAC_SI { get; set; }
        public string PHONG { get; set; }
        public string GHI_CHU { get; set; }
        public string NGUOI_NHAN { get; set; }
        public bool IS_SU_DUNG { get; set; }
        public DateTime NGAY_TRA { get; set; }
        public bool IS_DAT { get; set; }
        public string TEN_BO { get; set; }
        public string NK_SD_MA { get; set; }

    }
}
