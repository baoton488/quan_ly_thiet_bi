﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class PHONG
    {
        public int ID { get; set; }
        public string TEN_PHONG { get; set; }
        public string GHI_CHU { get; set; }
        public bool SU_DUNG { get; set; }
    }
}
