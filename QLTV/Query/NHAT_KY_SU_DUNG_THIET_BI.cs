﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Query
{
    public class NHAT_KY_SU_DUNG_THIET_BI
    {
        public int MA_THIET_BI { get; set; }
        public int ID_NK_SDTB { get; set; }
        public int ID_NK_SD { get; set; }
        public int USER_ID { get; set; }
        public int KIEU_QUET { get; set; }
        public TimeSpan NGAY_TAO { get; set; }
        public DateTime NGAY_TREN_THIET_BI { get; set; }
        public string TEN_THIET_BI { get; set; }
        public int TRANG_THAI_THIET_BI { get; set; }
    }
}
