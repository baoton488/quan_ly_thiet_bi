﻿using QLKhachHang.QLTV;
using QLKhachHang.Query;
using QLTV.QLTV.DanhMuc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLKhachHang
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DevExpress.UserSkins.BonusSkins.Register();
            //DevExpress.UserSkins.OfficeSkins.Register();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            frmDangNhap frm = new frmDangNhap();
            frmMain frmMain = new frmMain();
            
            Application.Run(frm);
            if (frm.IsLoggedIn)
            {
                frmMain.pQ_GROUP.GROUP_TEN = QueryPQ_User.intance().GROUP_TEN;
                frmMain.PhanQuyen();
                Application.Run(frmMain);
            }
                
        }
    }
}
