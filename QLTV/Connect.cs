﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System.Data;
using DevExpress.XtraEditors;
using System.Configuration;
using System.Xml;
using System.Windows.Forms;
namespace QLKhachHang
{
    public class Connect
    {
        public bool start = true;
        public string server;
        public string port;
        public string database;
        public string user_id;
        public string password;

        public static string conString = ConfigurationManager
                        .ConnectionStrings["MyAppConnection"]
                        .ConnectionString;      
        public static String _connection;
        string connstringsave2;
        public MySqlConnection conn = new MySqlConnection(conString);

        public void connn()
        {
           try
           {
               StringBuilder msb = new StringBuilder("Server=");
               msb.Append(server);
               msb.Append(";Port=");
               msb.Append(port);
               msb.Append(";Database=");
               msb.Append(database);
               msb.Append(";User ID=");
               msb.Append(user_id);
               msb.Append(";Password=");
               msb.Append(password);
               msb.Append(";CharSet=utf8");
               conString = msb.ToString();
               save2();
               conn = new MySqlConnection(conString);

               Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
               config.ConnectionStrings.ConnectionStrings["MyAppConnection"].ConnectionString = conString;
               config.ConnectionStrings.ConnectionStrings["Connection"].ConnectionString = connstringsave2;
               ConnectionStringsSection section = config.GetSection("connectionStrings") as ConnectionStringsSection;
               if (!section.SectionInformation.IsProtected)
               {
                   section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
               }
               section.SectionInformation.ForceSave = true;
               config.Save(ConfigurationSaveMode.Modified);
               ConfigurationManager.RefreshSection("connectionStrings");
               conn.Open();
               XtraMessageBox.Show("Kết nối thành công");
           }
           catch(Exception ex)
           {
               XtraMessageBox.Show(ex.Message);
           }
        }
        
        void save2()
        {
            //string connectString = ConfigurationManager.ConnectionStrings["MyAppConnection"].ToString();
            StringBuilder msb = new StringBuilder("XpoProvider=MySql");
            msb.Append(";Server=");
            msb.Append(server);
            msb.Append(";Port=");
            msb.Append(port);
            msb.Append(";Database=");
            msb.Append(database);
            msb.Append(";User ID=");
            msb.Append(user_id);
            msb.Append(";Password=");
            msb.Append(password);
            msb.Append(";persist security info=true");
            msb.Append(";CharSet=utf8");
            msb.Append(";SslMode=none");
            connstringsave2 = msb.ToString();
        }
        public void openconnection()
        {
            try
            {
                if (conn == null)
                    conn = new MySqlConnection(conString);
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
            }
            catch
            {
                return;
                //XtraMessageBox.Show(ex.Message);
            }
        }
        public void closeconnection()
        {
            if (conn != null && conn.State == ConnectionState.Open)
                conn.Close();
        }
    }
}
