﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraSplashScreen;
using QLKhachHang.QLTV;
using QLKhachHang.Default;
using QLKhachHang.QLTV.GiaoDichVang;
using QLKhachHang.QLTV.HeThong;
using QLTV;
using System.Configuration;
using QLTB.Properties;
using System.Diagnostics;
using QLTB.QLTV.DanhMuc;
using QLTV.QLTV.DanhMuc;
using QLTB.QLTV.HeThong;
using QLKhachHang.Query;

namespace QLKhachHang
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        QuyenClass qc = new QuyenClass();
        public int check;
        SplashScreenManager splash = new SplashScreenManager();
        bool statesplash = false;
        private readonly frmDangNhap frm;
        public PQ_GROUP pQ_GROUP = new PQ_GROUP();
        private void StartForm()
        {
            Application.Run(new frmWait());
        }
        public frmMain()
        {
            //frm = frm1;
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                InitializeComponent();

                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }
                
                ////       t.Abort();
                DevExpress.XtraBars.Helpers.SkinHelper.InitSkinGallery(skinRibbonGalleryBarItem1, true, true);
                skinRibbonGalleryBarItem1.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(skinRibbonGalleryBarItem1_GalleryItemClick);
            }
            catch 
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }
                
            }
                //   Control.CheckForIllegalCrossThreadCalls = false;
        }
        public void PhanQuyen()
        {
            if(pQ_GROUP.GROUP_TEN == "User")
            {
                ribbonPage9.Visible = false;
                ribbonPage5.Visible = false;
                ribbonPage6.Visible = false;
            }    
        }

        private void AddTabControl(UserControl usercontrol, string itemTabname)
        {
            bool kq = false;
            foreach (XtraTabPage tabitem in xtraTabControl1.TabPages)
            {
                if (tabitem.Text == itemTabname)
                {
                    kq = true;
                    xtraTabControl1.SelectedTabPage = tabitem;
                }
            }
            if (kq == false)
            {
                Addtab addtab = new Addtab();
                addtab.AddtabControl(xtraTabControl1, itemTabname, usercontrol);
            }
        }
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void xtraTabControl1_ControlAdded(object sender, ControlEventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = xtraTabControl1.TabPages.Count - 1;
        }

        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)
        {
            xtraTabControl1.TabPages.RemoveAt(xtraTabControl1.SelectedTabPageIndex);
            xtraTabControl1.SelectedTabPageIndex = xtraTabControl1.TabPages.Count - 1;
        }

        string FileName = "UserSettings.txt";
        private void SaveSettings(UserSettings us)
        {
            BinaryFormatter binFormat = new BinaryFormatter();
            using (Stream fStream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, us);
                fStream.Close();
            }
        }

        private void skinRibbonGalleryBarItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            UserSettings us = new UserSettings();
            us.SkinName = DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveSkinName;
            SaveSettings(us);
        }
        private void skinRibbonGalleryBarItem1_GalleryItemClick(object sender, GalleryItemClickEventArgs e)
        {
            UserSettings us = new UserSettings();
            us.SkinName = DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveSkinName;
            SaveSettings(us);
        }

        [Serializable]
        public class UserSettings
        {
            public string SkinName;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            barStaticItem3.Caption = QueryPQ_User.intance().ten_dn;
            if(check == 2)
            {
                //ribbonPage1.Visible = false;
            }
            if (File.Exists(FileName))
                DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(LoadSettings(FileName).SkinName);
        }

        private UserSettings LoadSettings(string fileName)
        {
            UserSettings us = null;
            BinaryFormatter binFormat = new BinaryFormatter();
            Stream fStream = new FileStream(fileName, FileMode.Open);
            try { us = binFormat.Deserialize(fStream) as UserSettings; }
            finally { fStream.Close(); }
            return us;
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        //  splashScreenManager.ShowWaitForm();
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    //      Thread t = new Thread(new ThreadStart(StartForm));
            //    //       t.Start();
            //    frmConnect f = new frmConnect();
            //    AddTabControl(f, "Kết nối");
            //    //     t.Abort();
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }

            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }

            //}
        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {
           
        }

        private void barButtonItem10_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem11_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem12_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmBoThietBi f = new frmBoThietBi();
                AddTabControl(f, "Bộ thiết bị");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem15_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem16_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmPQ_GROUP f = new frmPQ_GROUP();
                AddTabControl(f, "Nhóm Người Dùng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }
               
            }
        }

        private void barButtonItem17_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmPQ_User f = new frmPQ_User();
                AddTabControl(f, "Người Dùng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem18_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmThietBi f = new frmThietBi();
                AddTabControl(f, "Tạo thiết bị theo bộ");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }
                
            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem19_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
            //frmDangNhap f = new frmDangNhap();
            //f.Close();
        }

        private void barButtonItem20_ItemClick(object sender, ItemClickEventArgs e)
        {          

        }

        private void barButtonItem21_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmHeThong f = new frmHeThong();
            f.Show();
        }

        private void barButtonItem22_ItemClick(object sender, ItemClickEventArgs e)
        {
             
        }

        private void barButtonItem23_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem24_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem25_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem26_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem27_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem29_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem30_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem31_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem32_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem28_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem33_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem34_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem36_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem37_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem22_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem25_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem38_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem39_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmConnect f = new frmConnect();
            f.Show();
        }

        private void barButtonItem40_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem41_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem42_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem43_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings["IO"].Value = "0";
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

            timer1.Enabled = true;
            ListImage = Split(Resources.bye, 130, 130);
            count = ListImage.Images.Count;
            //label1.Visible = false;
            xtraTabControl1.Visible = false;
            //pictureBox2.Visible = false;
            //this.CenterToScreen();
            pictureBox1.Location = new Point((this.Width / 2) - (pictureBox1.Width / 2),
                      (this.Height / 2) - (pictureBox1.Height / 2));
            pictureBox1.Refresh();
            this.FormBorderStyle = FormBorderStyle.None;
            this.TransparencyKey = SystemColors.Control;
            timer2.Enabled = true;
            e.Cancel = true;
        }

        private void barButtonItem44_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem45_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem46_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem47_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            s++;
            if (s == 3)
            {
                Thread.Sleep(500);
                timer1.Enabled = false;
                Process.GetCurrentProcess().Kill();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            pictureBox1.Image = ListImage.Images[i];
            i++;
            if (i == count) { i = 0; }
        }
        ImageList ListImage;
        int count = 0;
        int i = 0;
        private ImageList Split(Bitmap image, int width, int height)
        {
            ImageList rows = new ImageList();
            rows.ImageSize = new Size(width, height);
            rows.Images.AddStrip(image);
            ImageList cells = new ImageList();
            cells.ImageSize = new Size(width, height);
            foreach (Image row in rows.Images)
            {
                cells.Images.AddStrip(row);
            }
            return cells;
        }
        int s = 0;

        private void barButtonItem49_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem50_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmNhanVienAdd f = new frmNhanVienAdd();
                AddTabControl(f, "Nhân viên");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem1_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmLuuKhoThietBi f = new frmLuuKhoThietBi();
                AddTabControl(f, "Kho thiết bị");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem2_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            frmKiemTraThietBi f = new frmKiemTraThietBi();
            f.Show();
        }

        private void barButtonItem51_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem52_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmNhomThietBi f = new frmNhomThietBi();
                AddTabControl(f, "Nhóm thiết bị");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem15_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmThietBiSuDung f = new frmThietBiSuDung();
                AddTabControl(f, "Sử dụng thiết bị");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem53_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmThietBiChuyenBo f = new frmThietBiChuyenBo();
                AddTabControl(f, "Chuyển thiết bị theo bộ");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem54_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmKiemTraThietBiKoTheoBo f = new frmKiemTraThietBiKoTheoBo();
            f.Show();
    
        }

        private void barButtonItem55_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmNKTB_Xuat f = new frmNKTB_Xuat();
            f.Show();
        }

        private void barButtonItem56_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmKiemTraThietBiDangMuon f = new frmKiemTraThietBiDangMuon();
                AddTabControl(f, "Bộ thiết bị đang mượn");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem57_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmKiemTraSoLanSuDung f = new frmKiemTraSoLanSuDung();
                AddTabControl(f, "Kiểm tra số lần sử dụng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem58_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmGioiHanSoLanSuDung f = new frmGioiHanSoLanSuDung();
                AddTabControl(f, "Giới hạn số lần sử dụng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem59_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmThanhLy f = new frmThanhLy();
                AddTabControl(f, "Thanh lý");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem60_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem61_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmXemThietBiDaQuet f = new frmXemThietBiDaQuet();
            f.Show();
        }

        private void barButtonItem62_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmPQ_User f = new frmPQ_User();
                AddTabControl(f, "Người dùng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem63_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmPQ_GROUP f = new frmPQ_GROUP();
                AddTabControl(f, "Phân quyền group");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem64_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmStatus f = new frmStatus();
            f.Show();
        }

        private void barButtonItem65_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmCapNhatStatusBoThietBi f = new frmCapNhatStatusBoThietBi();
                AddTabControl(f, "Cập nhật trạng thái bộ thiết bị");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem66_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        //  splashScreenManager.ShowWaitForm();
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    //     Thread t = new Thread(new ThreadStart(StartForm));
            //    //    t.Start();
            //    frmPQ_THIETBI f = new frmPQ_THIETBI();
            //    AddTabControl(f, "Phân quyền thiết bị");
            //    //     t.Abort();
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }

            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }

            //}
        }

        private void barButtonItem24_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            frmStatus f = new frmStatus();
            f.Show();
        }

        private void barButtonItem27_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            frmXemThietBiDaQuet f = new frmXemThietBiDaQuet();
            f.Show();
        }

        private void barButtonItem30_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmKiemTraSoLanSuDung f = new frmKiemTraSoLanSuDung();
                AddTabControl(f, "Kiểm tra số lần sử dụng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem37_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            frmKiemTraThietBiKoTheoBo f = new frmKiemTraThietBiKoTheoBo();
            f.Show();
        }
    }
}