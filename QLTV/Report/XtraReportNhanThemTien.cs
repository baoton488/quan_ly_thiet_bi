﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using QLKhachHang;
using MySql.Data.MySqlClient;
using System.Data;
using DevExpress.XtraEditors;

namespace QLTV.Report
{
    public partial class XtraReportNhanThemTien : DevExpress.XtraReports.UI.XtraReport
    {
        Connect cn = new Connect();
        public decimal tienvang;
        public string tentiem,tgd,ly;
        public string diachi;
        public string mahang;
        public string tenvang;
        public string dt;
        public decimal sl;
        public string loai;
        public decimal dongia;
        public decimal cantong, tl_hot,tru_hot,gia_cong,thanh_tien;
        public int maid;
        public string mapx;
        DateTime time;
        decimal tong_tien;
        public decimal tien_them;
        public decimal tien_moi;
        DateTime ngay_them;
        public XtraReportNhanThemTien(decimal tienthem,decimal tienmoi,DateTime ngaythem)
        {
            tien_moi = tienmoi;
            tien_them = tienthem;
            ngay_them = ngaythem;
            //tentiem = tentiem1;
            //diachi = diachi1;
            //dt = dt1;
            //tgd = tgd1;
            //ly = ly1;
            InitializeComponent();           
        }

        private void XtraReport7_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //query2();
            xrLabel8.Text = String.Format("{0:n0}", tien_them);
            xrLabel11.Text = String.Format("{0:n0}", tien_moi);
            string ngay = ngay_them.ToString("dd/MM/yyyy HH:mm");
            xrLabel13.Text = ngay;
            //var s =  String.Format("{0:n0}", tienvang);
            //xrLabel37.Text = s.ToString();
            
            ////xrLabel37.TextFormatString = "{0:n0}";
            //xrLabeltenTiem.Text = tentiem;
            //xrLabeltenTiem2.Text = tentiem;
            //xrLabelDiaChi.Text = diachi;
            //xrLabel9.Text = dt;
            //xrLabel4.Text = time.ToString();
            //xrLabel21.Text = time.ToString();
            //xrLabelTenGiaoDich.Text = tgd.ToString();
            //xrLabelLuuY.Text = ly.ToString();
        }      
        void query2()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT NOW()";
                cmd.Connection = cn.conn;

                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    time = DateTime.Parse(rd[0].ToString());
                }
                rd.Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

    }
}
