﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using QLKhachHang;
using MySql.Data.MySqlClient;
using System.Data;
using DevExpress.XtraEditors;

namespace QLTV.Report
{
    public partial class XtraReport8 : DevExpress.XtraReports.UI.XtraReport
    {
        Connect cn = new Connect();
        decimal tienvang;
        string tentiem;
        string diachi;
        string mahang;
        string tenvang;
        int maid;
        string mapx;
        DateTime time;
        public XtraReport8(string tentiem1,string diachi1,int maid1,string mapx1)
        {
            
            tentiem = tentiem1;
            diachi = diachi1;
            maid = maid1;
            mapx = mapx1;            
            InitializeComponent();           
        }

        private void XtraReport7_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            query2();
           // query();
            //xrLabel2.Text = tenvang;
            var s =  String.Format("{0:n0}", tienvang);
            xrLabel37.Text = s.ToString();
            
            //xrLabel37.TextFormatString = "{0:n0}";
            xrLabel3.Text = tentiem;
            xrLabel5.Text = tentiem;
            
            xrLabel4.Text = time.ToString();
            xrLabel21.Text = time.ToString();
        }

        void query()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select DISTINCT `phx_chi_tiet_phieu_xuat`.`CHI_TIET_PHIEU_XUAT_ID`,"+
		                           " `phx_chi_tiet_phieu_xuat`.`PHIEU_XUAT_ID`, "+
		                           " `phx_chi_tiet_phieu_xuat`.`HANGHOAID`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`HANGHOAMA`, "+
	 	                            "`phx_chi_tiet_phieu_xuat`.`HANG_HOA_TEN`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`SO_LUONG`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`VAT`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`DON_GIA`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`THANH_TIEN`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`SU_DUNG`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`GIA_GOC`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`GIAM_GIA_ID`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`CAN_TONG`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`TL_HOT`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`GIA_CONG`, "+
		                            "`phx_chi_tiet_phieu_xuat`.`NHOMHANGID`,"+
		                            "`phx_chi_tiet_phieu_xuat`.`LOAIVANG`,"+
		                            "`phx_phieu_xuat`.`PHIEU_XUAT_MA`,"+
                                    "`phx_phieu_xuat`.`TIEN_BOT` ,"+
                                    "`phx_khach_hang`.`KH_TEN`"+
                            " from `phx_chi_tiet_phieu_xuat`, `phx_phieu_xuat` , `phx_khach_hang`"+
                            "where `phx_khach_hang`.`KH_ID` = `phx_phieu_xuat`.`KH_ID`"+
                            "and `phx_chi_tiet_phieu_xuat`.`PHIEU_XUAT_ID` = '"+maid+"'"+
	                            "and `phx_phieu_xuat`.`PHIEU_XUAT_MA` = '"+mapx+"'";
                cmd.Connection = cn.conn;

                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    mahang = rd["HANGHOAMA"].ToString();
                    tenvang = rd["HANG_HOA_TEN"].ToString();
                   // diachi = rd["GHI_CHU"].ToString();
                }
                // gcHangHoa.DataSource = datatable;
                //datatable += datatable;
                rd.Close();
                // gcHangHoa.DataSource = datatable;
                // treeList1.DataSource = datatable;

                // gvHangHoa.UpdateCurrentRow();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        void query2()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT NOW()";
                cmd.Connection = cn.conn;

                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    time = DateTime.Parse(rd[0].ToString());
                    // diachi = rd["GHI_CHU"].ToString();
                }
                // gcHangHoa.DataSource = datatable;
                //datatable += datatable;
                rd.Close();
                // gcHangHoa.DataSource = datatable;
                // treeList1.DataSource = datatable;

                // gvHangHoa.UpdateCurrentRow();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void sqlDataSource1_ConfigureDataConnection(object sender, DevExpress.DataAccess.Sql.ConfigureDataConnectionEventArgs e)
        {

        }

    }
}
