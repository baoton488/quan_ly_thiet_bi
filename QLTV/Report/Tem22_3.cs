﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.BarCode;
using System.Drawing.Printing;
using DevExpress.DataAccess.Sql;
using DevExpress.DataAccess.ConnectionParameters;

namespace QLKhachHang.Report
{
    public partial class Tem22_3 : DevExpress.XtraReports.UI.XtraReport
    {
        public string klh1;
        public string klt1;
        public string klv1;
        public string klhg1;
        public string tentiem1;
        public string diachi1;
        public string kyhieu1;
        public string klvg1;

        public Tem22_3()
        {
            InitializeComponent();
            
            //klv1 = klv;
            //a = klh;
            //klhh3 = klhh;
            //klt1 = klt;
            //diachi1 = diachi;
            //tentiem1 = tentiem;
            //lvkh1 = lvkh;
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           // ((XRSubreport)sender).ReportSource.FilterString = "parameter1";
        }

        private void Tem22_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabelTenTiem.Text = tentiem1;
            xrLabelDiaChi.Text = diachi1;
            xrLabelKLT.Text = string.Concat("KLT: ", klt1);
            xrLabelKLH.Text = string.Concat("KLH: ", klh1,"   " ,klhg1);
            //xrLabelKLHG.Text = klhg1;
            xrLabelKLV.Text = string.Concat("KLV: ", klv1);
            xrLabelKLVG.Text = string.Concat("≈ ",klvg1);
            xrLabelKyHieu.Text = kyhieu1;
        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }
    }
}
