﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.BarCode;
using System.Drawing.Printing;
using DevExpress.DataAccess.Sql;
using DevExpress.DataAccess.ConnectionParameters;

namespace QLKhachHang.Report
{
    public partial class Tem22 : DevExpress.XtraReports.UI.XtraReport
    {
        public string cantong;
        public string a;
        public string klhh3;
        public string klt1;
        public string klv1;
        public string tentiem1;
        public string diachi1;
        public string lvkh1;
        //int hanghoaid1;
        //string hanghoama1;
        //int loaiid1;
        //int dvtid1;
        //int nhomhangid1;
        //int nccid1;
        //int giamgiaid1;
        //string hanghoaten1;
        //decimal gia_ban1;
        //int vat1;
        //int thue1;
        //int sudung1;
        //int danhdau1;
        //int sl_in1;
        //string ghichu1;
        //int tao_ma1;
        //decimal gia_ban_si1;
        //decimal can_tong1;
        //decimal tl_hot1;
        //decimal gia_cong1;
        //decimal don_gia_goc1;
        //decimal cong_goc1;
        //decimal tuoi_ban1;
        //decimal tuoi_mua1;
        public Tem22(string klv,string klh,string klhh,string klt,string tentiem,string diachi,string lvkh)
        {
            InitializeComponent();
            klv1 = klv;
            a = klh;
            klhh3 = klhh;
            klt1 = klt;
            diachi1 = diachi;
            tentiem1 = tentiem;
            lvkh1 = lvkh;
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           // ((XRSubreport)sender).ReportSource.FilterString = "parameter1";
        }

        private void Tem22_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //frmHangHoa frm = new frmHangHoa();
            //frmHangHoaSua f = new frmHangHoaSua(frm, hanghoaid1, hanghoama1, loaiid1, dvtid1, nhomhangid1, nccid1, giamgiaid1, hanghoaten1, gia_ban1, vat1, thue1, sudung1, danhdau1, sl_in1, ghichu1, tao_ma1, gia_ban_si1, can_tong1, tl_hot1, gia_cong1, don_gia_goc1, cong_goc1, tuoi_ban1, tuoi_mua1);
            //string a = f.getCanTong;
            //xrLabel1.Text = f.getCanTong;
            //using(frmHangHoaSua f = new frmHangHoaSua(frm, hanghoaid1, hanghoama1, loaiid1, dvtid1, nhomhangid1, nccid1, giamgiaid1, hanghoaten1, gia_ban1, vat1, thue1, sudung1, danhdau1, sl_in1, ghichu1, tao_ma1, gia_ban_si1, can_tong1, tl_hot1, gia_cong1, don_gia_goc1, cong_goc1, tuoi_ban1, tuoi_mua1))
            //{
            //    string a = f.getCanTong;
            //    xrLabel1.Text = f.getCanTong;
            //}

            xrLabel1.Text = klv1;
            xrLabel18.Text = a;
            xrLabel20.Text = klhh3.ToString();
            xrLabel14.Text = klt1;
            xrLabel12.Text = tentiem1;
            xrLabel13.Text = diachi1;
            xrLabel21.Text = lvkh1;
           // ((XtraReport)sender).PrintingSystem.StartPrint += PrintingSystem_StartPrint;
            //DetailReportBand kk = (DetailReportBand)sender;
            ////var kk2 = kk.RootReport.GetCurrentColumnValue("NUMEROETIQUETAS");

            //kk.ReportPrintOptions.DetailCountOnEmptyDataSource = Convert.ToInt32(5);

            //xrBarCode2.Symbology = new Code128Generator();

            //xrBarCode2.Width = 120;
            //xrBarCode2.Height = 15;
            //xrBarCode2.AutoModule = true;

            //((Code39Generator)xrBarCode2.Symbology).CalcCheckSum = false;
            //((Code39Generator)xrBarCode2.Symbology).WideNarrowRatio = 2.5F;

        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }
        //private void PrintingSystem_StartPrint(object sender, DevExpress.XtraPrinting.PrintDocumentEventArgs e)
        //{
        //    e.PrintDocument.PrinterSettings.PrinterName = "Antech_i  i-2200 (1)";
        //    for (int i = 0; i < e.PrintDocument.PrinterSettings.PaperSources.Count; i++)
        //        if (e.PrintDocument.PrinterSettings.PaperSources[i].Kind == PaperSourceKind.TractorFeed)
        //        {
        //            e.PrintDocument.DefaultPageSettings.PaperSource = e.PrintDocument.PrinterSettings.PaperSources[i];
        //            break;
        //        }
        //}
    }
}
