﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using QLKhachHang;
using MySql.Data.MySqlClient;
using System.Data;
using DevExpress.XtraEditors;

namespace QLTV.Report
{
    public partial class XtraReportCamVang : DevExpress.XtraReports.UI.XtraReport
    {
        Connect cn = new Connect();
        public decimal tienvang;
        public string tentiem,tgd,ly;
        public string diachi;
        public string mahang;
        public string tenvang;
        public string dt;
        public decimal sl;
        public string loai;
        public decimal dongia;
        public decimal cantong, tl_hot,tru_hot,gia_cong,thanh_tien;
        public int maid;
        public string mapx;
        DateTime time;
        string tieu_de_phieu_cam;
        //decimal tong_tien;
        public XtraReportCamVang(string tentiem1, string diachi1, string dt1,string tgd1,string ly1,string tieu_de_phieu_cam1)
        {
            tentiem = tentiem1;
            diachi = diachi1;
            //maid = maid1;
            //mapx = mapx1;
            dt = dt1;
            tgd = tgd1;
            ly = ly1;
            tieu_de_phieu_cam = tieu_de_phieu_cam1;
            InitializeComponent();           
        }

        private void XtraReport7_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            query2();
           // query();
            //xrLabel2.Text = tenvang;
            var s =  String.Format("{0:n0}", tienvang);
            xrLabel37.Text = s.ToString();
            
            //xrLabel37.TextFormatString = "{0:n0}";
            xrLabeltenTiem.Text = tentiem;
            xrLabeltenTiem2.Text = tentiem;
            xrLabelDiaChi.Text = diachi;
            xrLabel9.Text = dt;
            xrLabel4.Text = time.ToString("dd/MM/yyyy hh:mm tt");
            string m = "Ngày";
            xrLabel21.Text = time.ToString("dd/MM/yyyy");
            xrLabel22.Text = time.ToString("hh:mm tt");
            xrLabelTenGiaoDich.Text = tgd.ToString();
            xrLabelLuuY.Text = ly.ToString();
            xrLabel34.Text = string.Concat("Nhân viên: ",tieu_de_phieu_cam.ToString());
            xrLabel5.Text = tgd.ToString();
        }      
        void query2()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT NOW()";
                cmd.Connection = cn.conn;

                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    time = DateTime.Parse(rd[0].ToString());
                }
                rd.Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

    }
}
