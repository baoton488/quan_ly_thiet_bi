﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using QLKhachHang.Default;
using System.Threading;
using DevExpress.UserSkins;
using DevExpress.Skins;
using BCrypt.Net;
using QLKhachHang.Query;



namespace QLKhachHang.QLTV
{
    public partial class frmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        Connect cn = new Connect();
        QuyenClass qc = new QuyenClass();
        PQ_USER pQ_USER = new PQ_USER();
        PQ_GROUP pQ_GROUP = new PQ_GROUP();
        public bool IsLoggedIn { get; set; }
        bool flag = true;
        bool checkserver;
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if(pQ_USER.BIKHOA == true)
            {             
                XtraMessageBox.Show("Tài khoản đã bị khóa");
                return;
            }
            else
            {
                try
                {
                    selectpass();
                    bool a = false;
                    a = BCrypt.Net.BCrypt.Verify(textEditPass.Text, pQ_USER.MAT_KHAU);

                    if (a == false)
                    {
                        XtraMessageBox.Show("Bạn đã nhập sai tên đăng nhập hoặc mật khẩu");
                        return;
                    }
                    else
                    {
                        
                        phanquyen();
                    }
                }
                catch
                {
                    XtraMessageBox.Show("Bạn đã nhập sai tên đăng nhập hoặc mật khẩu");
                }
            }
            
            
        }
        void selectpass()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "SELECT * FROM `PQ_GROUP` , PQ_USER WHERE PQ_USER.GROUP_ID = PQ_GROUP.GROUP_ID and USER_TEN = '" + textEditTen.Text + "' and PQ_USER.SU_DUNG = 1";
                
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    pQ_USER.BIKHOA = rd.GetBoolean("BIKHOA");
                    pQ_USER.GROUP_ID = rd.GetInt32("GROUP_ID");
                    pQ_USER.MAT_KHAU = rd.GetString("MAT_KHAU");
                    pQ_GROUP.GROUP_TEN = rd.GetString("GROUP_TEN");
                    QueryPQ_User.intance().GROUP_TEN = rd.GetString("GROUP_TEN");
                    QueryPQ_User.intance().user_id = rd.GetInt32("USER_ID");
                    QueryPQ_User.intance().ten_dn = rd.GetString("USER_TEN");
                    if (String.IsNullOrEmpty(pQ_USER.MAT_KHAU))
                        flag = false;
                    else
                        flag = true;
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                checkserver = true;
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void phanquyen()
        {
            //frmMain f = new frmMain(this);
            //f.pQ_GROUP.GROUP_TEN = pQ_GROUP.GROUP_TEN;
            this.Close();
            IsLoggedIn = true;
            
            //f.PhanQuyen();
            ////f.check = 2;
            //this.Hide();
            //f.ShowDialog();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            frmConnect f = new frmConnect();
            f.Show();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textEditPass.Properties.UseSystemPasswordChar = false;
            }
            else
                textEditPass.Properties.UseSystemPasswordChar = true;
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            selectpass();
            if (checkserver == false)
            {
                simpleButton2.Enabled = false;
                return;
            }
        }

        private void frmDangNhap_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}