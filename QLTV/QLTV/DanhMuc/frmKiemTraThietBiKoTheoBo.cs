﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraReports.UI;
using System.Collections;
using QLTV.Report;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using System.Configuration;
using QLKhachHang.Query;
using Dapper;
//using QLTV.QLTV.GiaoDichVang;

namespace QLKhachHang.QLTV.GiaoDichVang
{
    public partial class frmKiemTraThietBiKoTheoBo : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        int mabothietbi;
        int LayamaXoa;
        //List<int> dsID = new List<int>();
        bool state = true;
        Connect cn = new Connect();
        DataTable datatable = new DataTable();
        Query.QueryTonKho qrtk = new Query.QueryTonKho();

        string maphieuxuat;
        int maid;
        decimal tonkho;
        int hanghoaid3;
        bool statesavephx;
        bool statesavechitietphx;
        bool statesaveupdate;

        public decimal tongtien = 0;

        public GridControl gridcontrol = new GridControl();
        public GridView gridview = null;


        bool check = false;
        
        string checktonkho;

        public Decimal tinhtienvang;
        public string tentiem, diachi,dt,tieu_de_pc,tieu_de_pb;
        int kiem_tra_so_luong;
        int so_luong_thuc_te_trong_bo;

        public frmKiemTraThietBiKoTheoBo()
        {
            InitializeComponent();
            //this.KeyUp += new KeyEventHandler(frmGiaoDich_KeyUp);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }
        object row;
        
        
        private void frmGiaoDich_Load(object sender, EventArgs e)
        {
            
            this.KeyPreview = true;
            //loadlookupkhachhang();
            loadDatatable();
            
        }
        void loadDatatable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("MA_BO");
            datatable.Columns.Add("THIET_BI_MA");
            datatable.Columns.Add("MA_THIET_BI");
            datatable.Columns.Add("TEN_THIET_BI");
            datatable.Columns.Add("TEN_BO");
            datatable.Columns.Add("QUY_CACH");
            datatable.Columns.Add("SO_LUONG");
            datatable.Columns.Add("SL_SU_DUNG");
            gridControl1.DataSource = datatable;
        }

        
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            gridView1.AddNewRow();
        }

        private void gvHangHoa_ShownEditor(object sender, EventArgs e)
        {
            try
            {
                ColumnView view = (ColumnView)sender;
                if (view.FocusedColumn.FieldName == "HANGHOAMA" && view.ActiveEditor is LookUpEdit)
                {
                    //row = (string)view.GetFocusedRowCellValue("HANGHOAMA");
                    row = view.GetRowCellValue(view.FocusedRowHandle, "HANGHOAMA").ToString();
                    //IList<Car> filteredCars = _controller.GetCarsByType(carTypeId);
                    //edit.Properties.DataSource = filteredCars;
                }
            }
            catch
            {
                //Log
            }
        }
        void edit_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit edit = sender as LookUpEdit;
            DataRowView rowView = (DataRowView)edit.GetSelectedDataRow();
            DataRow row = rowView.Row;
        }

        string a;
        public static void CopyValues(DataRow sourceDataRow, DataRow targetDataRow)
        {
            try
            {
                if (sourceDataRow == null || targetDataRow == null)
                    return;
                targetDataRow["HANGHOAID"] = sourceDataRow["HANGHOAID"];
                targetDataRow["HANG_HOA_TEN"] = sourceDataRow["HANG_HOA_TEN"];
                targetDataRow["NHOM_TEN"] = sourceDataRow["NHOM_TEN"];
                targetDataRow["CAN_TONG"] = sourceDataRow["CAN_TONG"];
                targetDataRow["TL_HOT"] = sourceDataRow["TL_HOT"];
                targetDataRow["GIA_CONG"] = sourceDataRow["GIA_CONG"];
                targetDataRow["DON_GIA_BAN"] = sourceDataRow["DON_GIA_BAN"];
                targetDataRow["SO_LUONG"] = 1;
                object tt = sourceDataRow["DON_GIA_BAN"];
                decimal thanhtien = Decimal.Parse(tt.ToString());
                targetDataRow["THANH_TIEN"] = 1 * thanhtien;
                
            }
            catch { }
        }
        long id2;
        string ma2;
        void addatarow()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from danh_muc_hang_hoa where su_dung = 1 and HANGHOAMA= '" + a + "'"; ;
                cmd.Connection = cn.conn;
                //DataTable datatable = new DataTable();
                //datatable.Columns.Add("HANGHOAID");
                //datatable.Columns.Add("HANGHOAMA");
                //datatable.Columns.Add("LOAIID");
                //datatable.Columns.Add("DVTID");
                //datatable.Columns.Add("NHOMHANGID");
                //datatable.Columns.Add("NCCID");
                //datatable.Columns.Add("GIAM_GIA_ID");
                //datatable.Columns.Add("HANG_HOA_TEN");
                //datatable.Columns.Add("GIA_BAN");
                //datatable.Columns.Add("VAT");
                //datatable.Columns.Add("THUE");
                //datatable.Columns.Add("SU_DUNG");
                //datatable.Columns.Add("SL_IN");
                //datatable.Columns.Add("GHI_CHU");
                //datatable.Columns.Add("TAO_MA");
                //datatable.Columns.Add("GIA_BAN_SI");
                //datatable.Columns.Add("CAN_TONG");
                //datatable.Columns.Add("TL_HOT");
                //datatable.Columns.Add("GIA_CONG");
                //datatable.Columns.Add("DON_GIA_GOC");
                //datatable.Columns.Add("CONG_GOC");
                //datatable.Columns.Add("TUOI_BAN");
                //datatable.Columns.Add("TUOI_MUA");
                // datatable.Columns.Add("LOAI_TEN");
                //     datatable.Columns.Add("SU_DUNG");
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    DataRow row = datatable.NewRow();
                    row["HANGHOAID"] = rd["HANGHOAID"];
                    id2 = Int64.Parse(rd["HANGHOAID"].ToString());
                    row["HANGHOAMA"] = rd["HANGHOAMA"];
                    ma2 = rd["HANGHOAMA"].ToString();
                    row["LOAIID"] = rd["LOAIID"];
                    row["DVTID"] = rd["DVTID"];
                    row["NHOMHANGID"] = rd["NHOMHANGID"];
                    row["NCCID"] = rd["NCCID"];
                    row["GIAM_GIA_ID"] = rd["GIAM_GIA_ID"];
                    row["HANG_HOA_TEN"] = rd["HANG_HOA_TEN"];
                    row["GIA_BAN"] = rd["GIA_BAN"];
                    row["VAT"] = rd["VAT"];
                    row["THUE"] = rd["THUE"];
                    row["SU_DUNG"] = rd["SU_DUNG"];
                    row["SL_IN"] = rd["SL_IN"];
                    row["GHI_CHU"] = rd["GHI_CHU"];
                    row["TAO_MA"] = rd["TAO_MA"];
                    row["GIA_BAN_SI"] = rd["GIA_BAN_SI"];
                    row["CAN_TONG"] = rd["CAN_TONG"];
                    row["TL_HOT"] = rd["TL_HOT"];
                    row["GIA_CONG"] = rd["GIA_CONG"];
                    row["DON_GIA_GOC"] = rd["DON_GIA_GOC"];
                    row["CONG_GOC"] = rd["CONG_GOC"];
                    row["TUOI_BAN"] = rd["TUOI_BAN"];
                    row["TUOI_MUA"] = rd["TUOI_MUA"];
                    // row["LOAI_TEN"] = rd["LOAI_TEN"];
                    //     row["SU_DUNG"] = rd["SU_DUNG"];
                   // datatable.Rows.Add(row);
                    datatable.Rows.InsertAt(row, datatable.Rows.Count);
                    
                }
               // gcHangHoa.DataSource = datatable;
                //datatable += datatable;
                rd.Close();
               // gcHangHoa.DataSource = datatable;
                // treeList1.DataSource = datatable;
                
               // gvHangHoa.UpdateCurrentRow();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }     

        private void repositoryItemLookUpEdit2_EditValueChanged(object sender, EventArgs e)
        {            
            
        }

        
        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
                state = true;
               // dsID.Remove(LayamaXoa);
                setvaluegridview();
            }
            catch { }
        }
        ArrayList rows = new ArrayList();
        List<long> dsTam = new List<long>();   
        
        void setvaluegridview()
        {
            gridControl1.ForceInitialize();
            gridView1.UpdateCurrentRow();
        }
        string getNgayThangNam;
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);
            if (gridView1.RowCount < 1)
            {
                gridView1.AddNewRow();
                selectmahanghoa2();
                textEdit1.Text = "";
                textEdit1.Focus();
            }
            else
            {
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    try
                    {
                        int a = Convert.ToInt32(textEdit1.EditValue);
                        int b = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI"));

                        if (b == null)
                        {
                            b = 0;
                        }
                        long b2 = Int64.Parse(b.ToString());
                        if (a == b2)
                        {
                            state = false;
                            int sl = Convert.ToInt32(gridView1.GetRowCellValue(i, "SO_LUONG"));
                            sl = sl + 1;
                            if(kiem_tra_so_luong < sl)
                            {
                                XtraMessageBox.Show("Số lượng đã vượt quá số lượng có sẵn");
                                return;
                            }
                            gridView1.SetRowCellValue(i, "SO_LUONG", sl);
                            gridView1.FocusedRowHandle = i;
                            break;
                        }
                        else
                        {
                            state = true;
                        }
                            
                    }
                    catch {
                        //gridView1.DeleteRow(gridView1.FocusedRowHandle);
                    }
                }
                if(state == true)
                {
                    gridView1.AddNewRow();
                    selectmahanghoa2();
                    state = false;
                }
                textEdit1.Text = "";
                textEdit1.Focus();
            
            }
            gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);
            //if(textEdit1.Text == "")
            //{
            //    return;
            //}
            //gridView1.AddNewRow();
            //selecthh();
            //GiaoDich();           
        }
        void selecthh()
        {
            if (gridView1.RowCount < 1)
            {
                XtraMessageBox.Show("Bạn chưa chọn thêm hàng hóa");
                return;
            }
            checktonkho = textEdit1.Text;
            selectmahangtonkho();
            if (tonkho == 0)
            {
                
                XtraMessageBox.Show("Số lượng tồn không đủ để xuất kho");
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
                textEdit1.Text = "";
                textEdit1.Focus();
                return;
            }
            else
            {
                selectmahanghoa();
                if (check == true)
                {
                    XtraMessageBox.Show("Không có mã này");
                    gridView1.DeleteRow(gridView1.FocusedRowHandle);
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }
                else
                {
                    //for (int i = 0; i < dsID.Count; i++)
                    //{
                    //    int a = Int16.Parse(textEdit1.EditValue.ToString());
                    //    if (a == dsID[i])
                    //    {
                    //        state = false;
                    //        XtraMessageBox.Show("Bạn đã chọn rồi");
                    //    }

                    //}
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        try
                        {
                            long a = Int64.Parse(textEdit1.EditValue.ToString());
                            object b = gridView1.GetRowCellValue(i, "HANGHOAMA");
                            if (b == null)
                            {
                                b = 0;
                            }
                            long b2 = Int64.Parse(b.ToString());
                            if (a == b2)
                            {
                                state = false;
                                XtraMessageBox.Show("Bạn đã chọn rồi");
                                gridView1.DeleteRow(gridView1.FocusedRowHandle);
                                textEdit1.Text = "";
                                textEdit1.Focus();
                            }
                        }
                        catch { }
                    }
                    if (state == false)
                    {
                        return;
                    }
                    else
                    {
                        state = true;

                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "HANGHOAMA", textEdit1.EditValue);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "HANGHOAID", hanghoaid2);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "HANG_HOA_TEN", hanghoaten2);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NHOM_TEN", nhomten2);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "CAN_TONG", cantong2);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TL_HOT", tlhot2);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIA_CONG", giacong2);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DON_GIA_BAN", dongiaban2);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SO_LUONG", 1);
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NHOMHANGID", nhomhangid2);
                        
                       
                        //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TRU_HOT", truhot2);
                        TinhCanTong();
                        
                        gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THANH_TIEN",tinhtienvang );
                        //GiaoDich();
                        //loadtextbox();
                        setvaluegridview();
                        textEdit1.Text = "";
                        textEdit1.Focus();
                       // dsID.Add(Int16.Parse(hanghoaid2.ToString()));
                        
                    }

                }
            }   
        }

        void TinhCanTong()
        {
            //try
            //{
            //    tinhtruhot = cantong2 - tlhot2;
            //    string bien1 = tinhtruhot.ToString();
            //    bien1 = GetUntilOrEmpty(bien1, ".");
            //    if (bien1.Length == 4)
            //    {
            //        tinhtruhot = tinhtruhot / 100;
            //    }
            //    if (bien1.Length == 3)
            //    {
            //        tinhtruhot = tinhtruhot / 100;
            //    }
            //    if (bien1.Length == 2)
            //    {
            //        tinhtruhot = tinhtruhot / 100;
            //    }
            //    if (bien1.Length == 1)
            //    {
            //        tinhtruhot = tinhtruhot / 100;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    XtraMessageBox.Show(ex.Message);
            //}

        }
        public static string GetUntilOrEmpty(string text, string stopAt = ".")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }
        void selectmahanghoa()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = string.Format("SELECT THIET_BI.MA_BO,MA_THIET_BI,TEN_THIET_BI,BO_THIET_BI.TEN_BO as 'TEN_BO',QUY_CACH FROM `THIET_BI`,BO_THIET_BI WHERE THIET_BI.MA_BO = BO_THIET_BI.MA_BO and THIET_BI.SU_DUNG = 1 and BO_THIET_BI.SU_DUNG = 1 and THIET_BI.MA_THIET_BI LIKE N'{0}'", textEdit1.EditValue);
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "HANGHOAMA", textEdit1.EditValue);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "HANGHOAID", hanghoaid2);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "HANG_HOA_TEN", hanghoaten2);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NHOM_TEN", nhomten2);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "CAN_TONG", cantong2);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TL_HOT", tlhot2);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "GIA_CONG", giacong2);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "DON_GIA_BAN", dongiaban2);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SO_LUONG", 1);
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "NHOMHANGID", nhomhangid2);
                }
                else
                {
                    check = true;
                }
                rd.Close();

            }
            catch
            {
               // check = true;
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void selectmahanghoa2()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = string.Format("SELECT THIET_BI.MA_BO,THIET_BI.MA_THIET_BI,TEN_THIET_BI,BO_THIET_BI.TEN_BO as 'TEN_BO',QUY_CACH,TON_KHO.SL_TON as SL_TON, THIET_BI.THIET_BI_MA,TON_KHO.SL_SU_DUNG FROM `THIET_BI`,BO_THIET_BI, TON_KHO WHERE THIET_BI.MA_BO = BO_THIET_BI.MA_BO and THIET_BI.SU_DUNG = 1 and TON_KHO.MA_THIET_BI = THIET_BI.MA_THIET_BI and TON_KHO.MA_THIET_BI LIKE N'{0}'", textEdit1.Text);
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    kiem_tra_so_luong = Convert.ToInt32(rd["SL_TON"]);

                    if(kiem_tra_so_luong == 0)
                    {
                        XtraMessageBox.Show("Thiết bị này chưa có tồn kho");
                        gridView1.DeleteRow(gridView1.FocusedRowHandle);
                        textEdit1.Text = "";
                        textEdit1.Focus();
                        return;
                    }
                    
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_BO", rd.GetInt32(0));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_THIET_BI", rd.GetInt32(1));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN_THIET_BI", rd.GetString(2));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN_BO", rd.GetString(3));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "QUY_CACH", rd.GetString(4));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SO_LUONG", 1);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THIET_BI_MA", rd.GetString(6));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SL_SU_DUNG", rd.GetInt32(7));
                }
                else
                {
                    check = true;
                    gridView1.DeleteRow(gridView1.FocusedRowHandle);
                    XtraMessageBox.Show("Mã thiết bị " + textEdit1.Text + " không có trong bộ thiết bị " + lookUpEditBoThietBi.Text + "");
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }
                rd.Close();

            }
            catch
            {
                
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void selectmahangtonkho()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "select sl_ton from ton_kho where KHOID = 1 and HANGHOAID = " + checktonkho + "";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    tonkho = rd.GetDecimal(0);
                }
                rd.Close();

            }
            catch
            {
                // check = true;
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        bool ck2 = false;
        void checktonkhokhiban()
        {
            try
            {
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    checktonkho = gridView1.GetRowCellValue(i, "HANGHOAMA").ToString();
                    cn.openconnection();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandType = CommandType.Text;
                    string sql = "select sl_ton from ton_kho where KHOID = 1 and HANGHOAID = " + checktonkho + "";
                    cmd.CommandText = sql;
                    cmd.Connection = cn.conn;
                    MySqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        tonkho = rd.GetDecimal(0);
                    }
                    rd.Close();
                    if (tonkho == 0)
                    {
                        ck2 = true;
                        XtraMessageBox.Show("Mã " + checktonkho + " đã xuất kho rồi");
                        return;
                    }
                }


            }
            catch
            {
                // check = true;
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void updatetonkho2()
        {
            StringBuilder sCommand = new StringBuilder("update ton_kho set sl_xuat = 1,sl_ton= 0 where KHOID = 1 and HANGHOAID in ");
            using (MySqlConnection mConnection = cn.conn)
            {
                List<string> Rows = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    //DataRow r = (DataRow)rows[i];
                    object hht = gridView1.GetRowCellValue(i, "HANG_HOA_TEN");
                    object tt = gridView1.GetRowCellValue(i, "THANH_TIEN");
                    object hanghoaid = gridView1.GetRowCellValue(i, "HANGHOAID");
                    hanghoaid3 = Int32.Parse(hanghoaid.ToString());
                    object hanghoama = gridView1.GetRowCellValue(i, "HANGHOAMA");
                    object sl = gridView1.GetRowCellValue(i, "SO_LUONG");
                    object dgb = gridView1.GetRowCellValue(i, "DON_GIA_BAN");
                    object ct = gridView1.GetRowCellValue(i, "CAN_TONG");
                    object tlh = gridView1.GetRowCellValue(i, "TL_HOT");
                    object gc = gridView1.GetRowCellValue(i, "GIA_CONG");
                    object nhid = gridView1.GetRowCellValue(i, "NHOMHANGID");
                    object lv = gridView1.GetRowCellValue(i, "NHOM_TEN");
                    decimal tong = Decimal.Parse(tt.ToString());
                    tongtien = tongtien + tong;
                    rows.Add(gridView1.GetDataRow(i));
                    dsTam.Add(Int32.Parse(hanghoaid.ToString()));
                    setvaluegridview();
                    Rows.Add(string.Format("{0}",hanghoaid));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", Rows));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    myCmd.ExecuteNonQuery();
                    statesaveupdate = true;
                    //if(myCmd.ExecuteNonQuery() == gridView1.RowCount)
                    //{
                    //    statesaveupdate = true;
                    //}
                }
            }
        }
        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridView1.Focus();
            gridView1.RefreshData();
            gridView1.UpdateSummary();

        }

        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            gridView1.Focus();
            gridView1.RefreshData();
            gridView1.UpdateSummary();
        }

        private void gridView1_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            gridView1.Focus();
            gridView1.RefreshData();
            gridView1.UpdateSummary();
        }

        

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object s = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HANGHOAMA");
                    LayamaXoa = Int32.Parse(s.ToString());
                }
            }
            catch { }
            
        }
        void checkPhieu()
        {
            XtraReport6 rpt = new XtraReport6();
            rpt.Parameters["parameter1"].Value = 24;
            rpt.Parameters["parameter1"].Value = "PX180909008";
            rpt.CreateDocument();
            rpt.ShowPreviewDialog();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            XtraReportBanVang rpt = new XtraReportBanVang(tentiem, diachi, maid, maphieuxuat,dt,tieu_de_pb,tieu_de_pc);
            rpt.Parameters["parameter1"].Value = maid;
            rpt.Parameters["parameter2"].Value = maphieuxuat;
            rpt.Name = "In phiếu xuất";
            rpt.CreateDocument();
            rpt.Print();           
        }      

        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                gridView1.PostEditor();
                gridView1.UpdateCurrentRow();
                gridView1.FocusedRowHandle = GridControl.NewItemRowHandle;
            }
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
        
        }

        private void searchLookUpEditKhachHang_Click(object sender, EventArgs e)
        {
            
        }
        
        private void frmGiaoDich_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void frmGiaoDich_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode.Equals(Keys.F5))
            //{
            //    simpleButton4_Click(sender, e);
            //}
            //if (e.KeyCode.Equals(Keys.F1))
            //{

            //}
            //if (e.KeyCode == Keys.Escape)
            //{
            //    //this.Close();
            //}
        }
        void taosophieuxuat()
        {
            try
            {

                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "TaoSoPhieuXuat";
                //string sql = "set @THANGNAM = (select date_format(now(), '%y%m') );" +
                //             " set @b = (SELECT COUNT(PHIEU_MA) FROM ( select DISTINCT PHIEU_MA from cam_phieu_cam_vang ) a WHERE PHIEU_MA LIKE CONCAT('PC.',@THANGNAM,'%'))+1;"+
                //             " SET @b = CONVERT(@THANGNAM,UNSIGNED)*10000 +@b;"+
                //             " set @so_phieu2 = concat('PC.',convert(@b,UNSIGNED));"+
                //             " select @b,@THANGNAM,@so_phieu2;";
                //cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    maid = rd.GetInt32(0);
                    maphieuxuat = rd.GetString(4);
                }
                else
                    rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }

        }

        private void lookUpEditBoThietBi_EditValueChanged(object sender, EventArgs e)
        {
            mabothietbi = Convert.ToInt32(lookUpEditBoThietBi.EditValue);
        }
        string thongbao;
        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            //kiemtrathietbi();
            //XtraMessageBox.Show("Số lượng trong bộ "+lookUpEditBoThietBi.Text+" là "+so_luong_thuc_te_trong_bo+"");
            //thongbao = "";
            //List<string> list3 = new List<string>();
            //kiemtrathietbi();
            //kiemtrathucte();
            //IEnumerable<int> differenceQuery = list1.Except(list2);
            //foreach (int s in differenceQuery)
            //{
            //    list3.Add(s.ToString());
            //}
            //thongbao = string.Concat("Các thiết bị có mã sau còn thiếu: ", String.Join(" ", list3));
            //thongbao = thongbao.Trim();
            //string b = string.Empty;
            //string c = string.Empty;
            //for (int i = 0; i < thongbao.Length; i++)
            //{
            //    if (Char.IsDigit(thongbao[i]))
            //    {
            //        b += string.Concat(thongbao[i], ",");
            //        //c = string.Concat(b, ",");
            //    }

            //}
            //c = b;
            //if (c == "")
            //{

            //}
            //else
            //{
            //    c = c.Remove(c.Length - 1);
            //}

            //XtraMessageBoxArgs args = new XtraMessageBoxArgs();

            //args.Caption = "Message";
            //args.Text = thongbao;
            //args.Buttons = new DialogResult[] { DialogResult.OK, DialogResult.Cancel, DialogResult.Retry };
            //args.Showing += args_Showing;
            ////XtraMessageBox.Show(args).ToString();
            //DialogResult dr = XtraMessageBox.Show(args);
            //if (dr == DialogResult.OK)
            //{
            //    if (c == "")
            //    {
            //        insertlichsukiemtrahoantat();
            //        updatesudungtonkho();
            //        return;
            //    }
            //    else
            //    {
            //        insertlichsukiemtra();
            //        updatesudungtonkho();
            //        return;
            //    }

            //}
            //if (dr == DialogResult.Cancel)
            //{
            //    return;
            //}
            //if (dr == DialogResult.Retry)
            //{
            //    frmThietBiAdd f = new frmThietBiAdd();
            //    f.ShowDialog();
            //    return;
            //}
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            gridView1.AddNewRow();
        }

        //void kiemtrathucte()
        //{
        //    list2.Clear();
        //    for (int i = 0; i < gridView1.RowCount; i++)
        //    {
        //        int a = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI"));
        //        list2.Add(a);
        //    }
        //}
        void args_Showing(object sender, XtraMessageShowingArgs e)
        {
            e.Buttons[DialogResult.OK].Text = "Lưu";
            e.Buttons[DialogResult.Cancel].Text = "Tiếp tục";
            e.Buttons[DialogResult.Retry].Text = "Thay thế";
        }

        List<int> list1 = new List<int>();
        void kiemtrathietbi()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "SELECT `MA_THIET_BI` from THIET_BI WHERE THIET_BI.MA_BO = "+lookUpEditBoThietBi.EditValue+"";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    so_luong_thuc_te_trong_bo = rd.GetInt32(0);
                    int a = rd.GetInt32(0);
                    list1.Add(a);
                }

                rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void frmKiemTraThietBiKoTheoBo_KeyUp(object sender, KeyEventArgs e)
        {
            //simpleButton5_Click(sender, e);
            //textEdit1.Focus();
            //textEdit1.Text = "";
        }
    }
}
