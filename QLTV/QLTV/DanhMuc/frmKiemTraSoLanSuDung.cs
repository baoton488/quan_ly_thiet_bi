﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
//using DapperExtensions;
using System.Configuration;

namespace QLKhachHang
{
    public partial class frmKiemTraSoLanSuDung : DevExpress.XtraEditors.XtraUserControl
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryTHIET_BI qr = new Query.QueryTHIET_BI();
        int maid;
        public frmKiemTraSoLanSuDung()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
            gridControl1.DataSource = getDataTable(); 
        }
        public DataTable getDataTable()
        {
            cn.openconnection();
            MySqlCommand cmd = new MySqlCommand("SELECT count(NHAT_KY_SU_DUNG_THIET_BI.MA_THIET_BI) as SLSU_DUNG, NHAT_KY_SU_DUNG_THIET_BI.MA_THIET_BI, THIET_BI.TEN_THIET_BI,BO_THIET_BI.TEN_BO "+
                                                " from NHAT_KY_SU_DUNG_THIET_BI, THIET_BI, BO_THIET_BI "+
                                                " WHERE NHAT_KY_SU_DUNG_THIET_BI.MA_THIET_BI = THIET_BI.MA_THIET_BI "+
                                                " and THIET_BI.MA_BO = BO_THIET_BI.MA_BO "+
                                                " GROUP by NHAT_KY_SU_DUNG_THIET_BI.MA_THIET_BI", cn.conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);
            }
            catch { }
            return dt;
        }


        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            updatetonkho();
        }        

        void update()
        {
            
        }

        
        void load_thiet_bi(int id)
        {
            string s = "SELECT TON_KHO.MA_BO,TON_KHO.MA_THIET_BI,TEN_THIET_BI,BO_THIET_BI.TEN_BO as 'TEN_BO',QUY_CACH ,TON_KHO.SL_TON, THIET_BI.THIET_BI_MA, TON_KHO.SL_SU_DUNG "+
                        " FROM `THIET_BI`,BO_THIET_BI, TON_KHO "+
                        " WHERE TON_KHO.MA_BO = BO_THIET_BI.MA_BO "+
                        " and TON_KHO.MA_THIET_BI = THIET_BI.MA_THIET_BI "+
                        " and THIET_BI.SU_DUNG = 1 "+
                        " and TON_KHO.MA_BO = "+id+"";
            gridControl1.DataSource = connection.Query(s).ToList();
        }
        

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                
            }
        }
        void updatetonkho()
        {
            string sql = "UPDATE `TON_KHO` SET `SL_TON`=@SL_TON WHERE MA_THIET_BI = @MA_THIET_BI and MA_BO = @MA_BO";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<TON_KHO>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    list.Add(new TON_KHO() { MA_BO = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")), MA_THIET_BI = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI")),SL_TON = Convert.ToInt32(gridView1.GetRowCellValue(i, "SL_TON"))});
                }
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã cập nhật số lượng thiết bị");
                }
            }


            //StringBuilder sCommand = new StringBuilder("update ton_kho set sl_xuat = 1,sl_ton= 0 where KHOID = 1 and HANGHOAID in ");
            //using (MySqlConnection mConnection = cn.conn)
            //{
            //    List<string> Rows = new List<string>();
            //    for (int i = 0; i < gridView1.RowCount; i++)
            //    {
            //        //DataRow r = (DataRow)rows[i];
            //        object hht = gridView1.GetRowCellValue(i, "HANG_HOA_TEN");
            //        object tt = gridView1.GetRowCellValue(i, "THANH_TIEN");
            //        object hanghoaid = gridView1.GetRowCellValue(i, "HANGHOAID");
                    
                    
            //        //setvaluegridview();
            //        Rows.Add(string.Format("{0}", hanghoaid));
            //    }
            //    sCommand.Append("(");
            //    sCommand.Append(string.Join(",", Rows));
            //    sCommand.Append(")");
            //    sCommand.Append(";");
            //    cn.openconnection();
            //    using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
            //    {
            //        myCmd.CommandType = CommandType.Text;
            //        myCmd.ExecuteNonQuery();

            //        if (myCmd.ExecuteNonQuery() == gridView1.RowCount)
            //        {
            //            //statesaveupdate = true;
            //        }
            //    }
            //}
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            
        }

        private void frmThietBi_Load(object sender, EventArgs e)
        {
            
        }

        private void lookUpEditBoThietBi_EditValueChanged(object sender, EventArgs e)
        {
            
        }
        
        
    }
}