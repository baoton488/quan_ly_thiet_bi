﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
//using DapperExtensions;
using System.Configuration;

namespace QLKhachHang
{
    public partial class frmThietBiSuDung : DevExpress.XtraEditors.XtraUserControl
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryTHIET_BI qr = new Query.QueryTHIET_BI();
        public XRPictureBox picture = new XRPictureBox();
        int maid;
        public frmThietBiSuDung()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            updatetonkho();
        }        

        void update()
        {
            
        }

        
        void load_thiet_bi(int id)
        {
            string s = "select THIET_BI.MA_THIET_BI, THIET_BI.TEN_THIET_BI, THIET_BI.GHI_CHU, if(THIET_BI.SO_LAN_SU_DUNG is null , 0 , THIET_BI.SO_LAN_SU_DUNG) as SO_LAN_SU_DUNG" +
                        " from BO_THIET_BI, THIET_BI "+
                        " WHERE BO_THIET_BI.MA_BO = THIET_BI.MA_BO "+
                        " and BO_THIET_BI.MA_BO = "+id+"";
            gridControl1.DataSource = connection.Query(s).ToList();
        }
        

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked == true)
            {
                gridControl1.Enabled = true;
            }
            else
            {
                gridControl1.Enabled = false;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                
            }
        }
        void updatetonkho()
        {
            string sql = "UPDATE `THIET_BI` SET `SO_LAN_SU_DUNG`=@SO_LAN_SU_DUNG, GHI_CHU = @GHI_CHU WHERE MA_THIET_BI = @MA_THIET_BI";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<THIET_BI>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    list.Add(new THIET_BI() { SO_LAN_SU_DUNG = Convert.ToInt32(gridView1.GetRowCellValue(i, "SO_LAN_SU_DUNG")) + 1, MA_THIET_BI = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI")), GHI_CHU = gridView1.GetRowCellValue(i, "GHI_CHU").ToString() });
                }
                var affectedRows = connection.Execute(sql,
                    list
                );
            }


            //StringBuilder sCommand = new StringBuilder("update ton_kho set sl_xuat = 1,sl_ton= 0 where KHOID = 1 and HANGHOAID in ");
            //using (MySqlConnection mConnection = cn.conn)
            //{
            //    List<string> Rows = new List<string>();
            //    for (int i = 0; i < gridView1.RowCount; i++)
            //    {
            //        //DataRow r = (DataRow)rows[i];
            //        object hht = gridView1.GetRowCellValue(i, "HANG_HOA_TEN");
            //        object tt = gridView1.GetRowCellValue(i, "THANH_TIEN");
            //        object hanghoaid = gridView1.GetRowCellValue(i, "HANGHOAID");
                    
                    
            //        //setvaluegridview();
            //        Rows.Add(string.Format("{0}", hanghoaid));
            //    }
            //    sCommand.Append("(");
            //    sCommand.Append(string.Join(",", Rows));
            //    sCommand.Append(")");
            //    sCommand.Append(";");
            //    cn.openconnection();
            //    using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
            //    {
            //        myCmd.CommandType = CommandType.Text;
            //        myCmd.ExecuteNonQuery();

            //        if (myCmd.ExecuteNonQuery() == gridView1.RowCount)
            //        {
            //            //statesaveupdate = true;
            //        }
            //    }
            //}
        }

        void insertsudung()
        {
            //string sql = "INSERT INTO `SU_DUNG`(`MA_BO`, `NGAY`, `TEN_BAC_SI`, `PHONG`, `GHI_CHU`) VALUES (@MA_BO,now,@TEN_BAC_SI,@PHONG,@GHI_CHU)";
            //using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            //{
            //    connection.Open();
            //    var list = new List<SubBand)>();
            //    for (int i = 0; i < gridView1.RowCount; i++)
            //    {
            //        list.Add(new THIET_BI() { SO_LAN_SU_DUNG = Convert.ToInt32(gridView1.GetRowCellValue(i, "SO_LAN_SU_DUNG")), MA_THIET_BI = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI")), GHI_CHU = gridView1.GetRowCellValue(i, "GHI_CHU").ToString() });
            //    }
            //    var affectedRows = connection.Execute(sql,
            //        list
            //    );
            //}
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;
                    object ten;
                    object ghichu;
                    object mabo;
                    object quycach;

                    id = view.GetRowCellValue(view.FocusedRowHandle, "MA_THIET_BI");
                    mabo = view.GetRowCellValue(view.FocusedRowHandle, "MA_BO");
                    ten = view.GetRowCellValue(view.FocusedRowHandle, "TEN_THIET_BI").ToString();
                    quycach = view.GetRowCellValue(view.FocusedRowHandle, "QUY_CACH") == null ? "" : view.GetRowCellValue(view.FocusedRowHandle, "QUY_CACH");
                    ghichu = view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU") == null? "" :view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU");

                    maid = Convert.ToInt32(id);
                    lookUpEditBoThietBi.EditValue = mabo;

                }
            }
            catch { }
        }

        private void frmThietBi_Load(object sender, EventArgs e)
        {
            lookUpEditBoThietBi.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditBoThietBi.Properties.ValueMember = "MA_BO";
            lookUpEditBoThietBi.Properties.DisplayMember = "TEN_BO";
            gridControl1.Enabled = false;
            //gridControl1.DataSource = connection.Query<THIET_BI>("", commandType: CommandType.Text).ToList();
        }

        private void lookUpEditBoThietBi_EditValueChanged(object sender, EventArgs e)
        {
            load_thiet_bi(Convert.ToInt32(lookUpEditBoThietBi.EditValue));
        }
        
        
    }
}