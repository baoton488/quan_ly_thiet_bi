﻿using QLTB;
namespace QLTV.QLTV.DanhMuc
{
    partial class frmStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTênbộ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTôngsốbộ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCóthểsửdụng = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colĐangmổ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChờrửa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChờhấp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHư = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colĐangsữachữa = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(566, 331);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTênbộ,
            this.colTôngsốbộ,
            this.colCóthểsửdụng,
            this.colĐangmổ,
            this.colChờrửa,
            this.colChờhấp,
            this.colHư,
            this.colĐangsữachữa});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colTênbộ
            // 
            this.colTênbộ.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colTênbộ.AppearanceCell.Options.UseFont = true;
            this.colTênbộ.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colTênbộ.AppearanceHeader.Options.UseFont = true;
            this.colTênbộ.FieldName = "Tên bộ";
            this.colTênbộ.Name = "colTênbộ";
            this.colTênbộ.Visible = true;
            this.colTênbộ.VisibleIndex = 0;
            // 
            // colTôngsốbộ
            // 
            this.colTôngsốbộ.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colTôngsốbộ.AppearanceCell.Options.UseFont = true;
            this.colTôngsốbộ.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colTôngsốbộ.AppearanceHeader.Options.UseFont = true;
            this.colTôngsốbộ.FieldName = "Tông số bộ";
            this.colTôngsốbộ.Name = "colTôngsốbộ";
            this.colTôngsốbộ.Visible = true;
            this.colTôngsốbộ.VisibleIndex = 1;
            // 
            // colCóthểsửdụng
            // 
            this.colCóthểsửdụng.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colCóthểsửdụng.AppearanceCell.Options.UseFont = true;
            this.colCóthểsửdụng.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colCóthểsửdụng.AppearanceHeader.Options.UseFont = true;
            this.colCóthểsửdụng.FieldName = "Có thể sử dụng";
            this.colCóthểsửdụng.Name = "colCóthểsửdụng";
            this.colCóthểsửdụng.Visible = true;
            this.colCóthểsửdụng.VisibleIndex = 2;
            // 
            // colĐangmổ
            // 
            this.colĐangmổ.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colĐangmổ.AppearanceCell.Options.UseFont = true;
            this.colĐangmổ.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colĐangmổ.AppearanceHeader.Options.UseFont = true;
            this.colĐangmổ.FieldName = "Đang mổ";
            this.colĐangmổ.Name = "colĐangmổ";
            this.colĐangmổ.Visible = true;
            this.colĐangmổ.VisibleIndex = 3;
            // 
            // colChờrửa
            // 
            this.colChờrửa.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colChờrửa.AppearanceCell.Options.UseFont = true;
            this.colChờrửa.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colChờrửa.AppearanceHeader.Options.UseFont = true;
            this.colChờrửa.FieldName = "Chờ rửa";
            this.colChờrửa.Name = "colChờrửa";
            this.colChờrửa.Visible = true;
            this.colChờrửa.VisibleIndex = 4;
            // 
            // colChờhấp
            // 
            this.colChờhấp.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colChờhấp.AppearanceCell.Options.UseFont = true;
            this.colChờhấp.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colChờhấp.AppearanceHeader.Options.UseFont = true;
            this.colChờhấp.FieldName = "Chờ hấp";
            this.colChờhấp.Name = "colChờhấp";
            this.colChờhấp.Visible = true;
            this.colChờhấp.VisibleIndex = 5;
            // 
            // colHư
            // 
            this.colHư.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colHư.AppearanceCell.Options.UseFont = true;
            this.colHư.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colHư.AppearanceHeader.Options.UseFont = true;
            this.colHư.FieldName = "Hư";
            this.colHư.Name = "colHư";
            this.colHư.Visible = true;
            this.colHư.VisibleIndex = 6;
            // 
            // colĐangsữachữa
            // 
            this.colĐangsữachữa.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colĐangsữachữa.AppearanceCell.Options.UseFont = true;
            this.colĐangsữachữa.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 13F);
            this.colĐangsữachữa.AppearanceHeader.Options.UseFont = true;
            this.colĐangsữachữa.FieldName = "Đang sữa chữa";
            this.colĐangsữachữa.Name = "colĐangsữachữa";
            this.colĐangsữachữa.Visible = true;
            this.colĐangsữachữa.VisibleIndex = 7;
            // 
            // frmStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 331);
            this.Controls.Add(this.gridControl1);
            this.Name = "frmStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.XtraForm1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colTênbộ;
        private DevExpress.XtraGrid.Columns.GridColumn colTôngsốbộ;
        private DevExpress.XtraGrid.Columns.GridColumn colCóthểsửdụng;
        private DevExpress.XtraGrid.Columns.GridColumn colĐangmổ;
        private DevExpress.XtraGrid.Columns.GridColumn colChờrửa;
        private DevExpress.XtraGrid.Columns.GridColumn colChờhấp;
        private DevExpress.XtraGrid.Columns.GridColumn colHư;
        private DevExpress.XtraGrid.Columns.GridColumn colĐangsữachữa;
    }
}