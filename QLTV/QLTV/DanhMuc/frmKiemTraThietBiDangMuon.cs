﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
//using DapperExtensions;
using System.Configuration;
using Newtonsoft.Json;

namespace QLKhachHang
{
    public partial class frmKiemTraThietBiDangMuon : DevExpress.XtraEditors.XtraUserControl
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryTHIET_BI qr = new Query.QueryTHIET_BI();
        int maid;
        public frmKiemTraThietBiDangMuon()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            search();
        }        

        void update()
        {
            
        }

        
        void load_thiet_bi(int id)
        {
            try
            {
                gridControl1.DataSource = null;
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT SU_DUNG.CAC_BO_THIET_BI FROM NHOM_BO JOIN SU_DUNG ON NHOM_BO.NHOM_BO_ID = SU_DUNG.NHOM_BO_ID WHERE NHOM_BO.NHOM_BO_ID = "+id+" and SU_DUNG.IS_SU_DUNG = 1";
                cmd.Connection = cn.conn;
                DataTable dt = new DataTable();
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    string s = rd["CAC_BO_THIET_BI"].ToString();
                    dt = JsonConvert.DeserializeObject<DataTable>(s);
                    //chartControl1.Series["s1"].Points.AddPoint(rd["NHOM_TEN"].ToString(), rd.GetDouble("CAN_TONG"));
                    //chartControl1.Series["s2"].Points.AddPoint(rd["NHOM_TEN"].ToString(), rd.GetDouble("GIA_CONG"));

                }

                rd.Close();
                gridControl1.DataSource = dt;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }


            
                        
            //gridControl1.DataSource = dt;
        }
        

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                
            }
        }
        void search()
        {
            string sql = "UPDATE `TON_KHO` SET `SL_TON`=@SL_TON WHERE MA_THIET_BI = @MA_THIET_BI and MA_BO = @MA_BO";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<TON_KHO>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    list.Add(new TON_KHO() { MA_BO = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")), MA_THIET_BI = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI")),SL_TON = Convert.ToInt32(gridView1.GetRowCellValue(i, "SL_TON"))});
                }
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã cập nhật số lượng thiết bị");
                }
            }


            //StringBuilder sCommand = new StringBuilder("update ton_kho set sl_xuat = 1,sl_ton= 0 where KHOID = 1 and HANGHOAID in ");
            //using (MySqlConnection mConnection = cn.conn)
            //{
            //    List<string> Rows = new List<string>();
            //    for (int i = 0; i < gridView1.RowCount; i++)
            //    {
            //        //DataRow r = (DataRow)rows[i];
            //        object hht = gridView1.GetRowCellValue(i, "HANG_HOA_TEN");
            //        object tt = gridView1.GetRowCellValue(i, "THANH_TIEN");
            //        object hanghoaid = gridView1.GetRowCellValue(i, "HANGHOAID");
                    
                    
            //        //setvaluegridview();
            //        Rows.Add(string.Format("{0}", hanghoaid));
            //    }
            //    sCommand.Append("(");
            //    sCommand.Append(string.Join(",", Rows));
            //    sCommand.Append(")");
            //    sCommand.Append(";");
            //    cn.openconnection();
            //    using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
            //    {
            //        myCmd.CommandType = CommandType.Text;
            //        myCmd.ExecuteNonQuery();

            //        if (myCmd.ExecuteNonQuery() == gridView1.RowCount)
            //        {
            //            //statesaveupdate = true;
            //        }
            //    }
            //}
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;
                    object ten;
                    object ghichu;
                    object mabo;
                    object quycach;

                    id = view.GetRowCellValue(view.FocusedRowHandle, "MA_THIET_BI");
                    mabo = view.GetRowCellValue(view.FocusedRowHandle, "MA_BO");
                    ten = view.GetRowCellValue(view.FocusedRowHandle, "TEN_THIET_BI").ToString();
                    quycach = view.GetRowCellValue(view.FocusedRowHandle, "QUY_CACH") == null ? "" : view.GetRowCellValue(view.FocusedRowHandle, "QUY_CACH");
                    ghichu = view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU") == null? "" :view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU");

                    maid = Convert.ToInt32(id);
                    lookUpEditNhomBo.EditValue = mabo;

                }
            }
            catch { }
        }

        private void frmThietBi_Load(object sender, EventArgs e)
        {
            lookUpEditNhomBo.Properties.DataSource = connection.Query<NHOM_BO>("select * from NHOM_BO where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhomBo.Properties.ValueMember = "NHOM_BO_ID";
            lookUpEditNhomBo.Properties.DisplayMember = "NHOM_BO_TEN";
            //gridControl1.Enabled = false;
            //gridControl1.DataSource = connection.Query<THIET_BI>("", commandType: CommandType.Text).ToList();
        }

        private void lookUpEditBoThietBi_EditValueChanged(object sender, EventArgs e)
        {
            load_thiet_bi(Convert.ToInt32(lookUpEditNhomBo.EditValue));
        }
        
        
    }
}