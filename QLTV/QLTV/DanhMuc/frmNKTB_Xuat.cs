﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
using System.Configuration;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Dynamic;
using System.Collections;

namespace QLKhachHang
{
    public partial class frmNKTB_Xuat : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryBO_THIET_BI qr = new Query.QueryBO_THIET_BI();
        public XRPictureBox picture = new XRPictureBox();
        int maid;
        int idinssertnk_chitiet;
        string listgrouppq;
        string NK_SD_MA;
        public frmNKTB_Xuat()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            //checksudung();
            try
            {
                if (lookUpEditNguoiNhan.Text == "" || lookUpEditPhong.Text == "" || gridView1.RowCount < 1)
                {
                    XtraMessageBox.Show("Bạn chưa điền đủ thông tin");
                    return;
                }
                if (c == 1)
                {
                    c = 0;
                    return;
                }
                else
                {
                    RunTransaction(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
                    //gridControl1.DataSource = connection.Query<NHAT_KY_SU_DUNG>("select * from SU_DUNG order by ID desc", commandType: CommandType.Text);
                    //lookUpEditBoThietBi1.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
                    //lookUpEditBoThietBi1.Properties.ValueMember = "MA_BO";
                    //lookUpEditBoThietBi1.Properties.DisplayMember = "TEN_BO";
                }
            }
            catch (Exception ex) { XtraMessageBox.Show(ex.Message); }
        }

        void luu()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.queryaddbothietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                //cmd.Parameters.Add(qr.TEN_BO, MySqlDbType.VarChar).Value = txtNguoiNhan.Text;
                //cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtGhiChu.Text;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã lưu");
                    //txtNguoiNhan.Text = "";
                    //txtGhiChu.Text = "";
                    gridControl1.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void update()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.queryupdatebothietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                //cmd.Parameters.Add(qr.TEN_BO, MySqlDbType.VarChar).Value = txtNguoiNhan.Text;
                //cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtGhiChu.Text;
                cmd.Parameters.Add(qr.MA_BO, MySqlDbType.Int32).Value = maid;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã lưu");
                    lookUpEditPhong.Text = "";
                    //txtGhiChu.Text = "";
                    gridControl1.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void delete()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.querydeletebothietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.SU_DUNG, MySqlDbType.Int32).Value = 0;
                cmd.Parameters.Add(qr.MA_BO, MySqlDbType.Int32).Value = maid;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã xóa");
                    gridControl1.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void load_bo()
        {
            lookUpEditNhomBo1.Properties.DataSource = connection.Query<NHOM_BO>("select * from NHOM_BO where SU_DUNG = @SU_DUNG and NHOM_BO_ID  in ("+listgrouppq+") ", new { SU_DUNG = 1}, commandType: CommandType.Text);
            lookUpEditNhomBo1.Properties.ValueMember = "NHOM_BO_ID";
            lookUpEditNhomBo1.Properties.DisplayMember = "NHOM_BO_TEN";

            lookUpEditPhong.Properties.DataSource = connection.Query<PHONG>("select * from PHONG where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditPhong.Properties.ValueMember = "ID";
            lookUpEditPhong.Properties.DisplayMember = "TEN_PHONG";

            lookUpEditNguoiNhan.Properties.DataSource = connection.Query<NHAN_VIEN>("select * from NHAN_VIEN where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNguoiNhan.Properties.ValueMember = "MA_NHAN_VIEN";
            lookUpEditNguoiNhan.Properties.DisplayMember = "TEN_NHAN_VIEN";

        }

        private void frmBoThietBiAdd_Load(object sender, EventArgs e)
        {
            selectpqgroup();
            load_bo();

            DataTable datatable = new DataTable();
            datatable.Columns.Add("MA_BO");
            datatable.Columns.Add("TEN_BO");
            datatable.Columns.Add("IS_DAT");
            datatable.Columns.Add("TRANG_THAI");

            gridControl1.DataSource = datatable;
            //gridControl1.DataSource = connection.Query<NHAT_KY_SU_DUNG>("select * from NHAT_KY_SU_DUNG order by ID_NK_SD desc", commandType: CommandType.Text);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn trả bộ thiết bị không? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                trathietbi();
                gridControl1.DataSource = connection.Query<NHAT_KY_SU_DUNG>("select * from SU_DUNG order by ID desc", commandType: CommandType.Text);
            }
            
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;


                    id = view.GetRowCellValue(view.FocusedRowHandle, "ID");

                    maid = Convert.ToInt32(id);
                }
            }
            catch { }
        }

        void xuatthietbi()
        {
            taomanhatky();
            string sql = "INSERT INTO `NHAT_KY_SU_DUNG`(`TEN_CA_MO`, `NGAY`, `PHONG`, `GHI_CHU`, `NGUOI_NHAN`, `IS_SU_DUNG`,NK_SD_MA,USER_ID) VALUES " +
                        " (@TEN_CA_MO,now(),@PHONG,@GHI_CHU,@NGUOI_NHAN,@IS_SU_DUNG,@NK_SD_MA,@USER_ID) ";
            json = string.Concat("[", json, "]");
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAT_KY_SU_DUNG>();
                list.Add(new NHAT_KY_SU_DUNG()
                        {
                            TEN_CA_MO = txtCaMo.Text
                            ,
                            NGUOI_NHAN = lookUpEditNguoiNhan.Text
                            ,
                            IS_SU_DUNG = true
                            ,
                            GHI_CHU = memoEdit1.Text
                            ,
                            PHONG = lookUpEditPhong.Text
                            ,
                            NK_SD_MA = NK_SD_MA
                            ,
                            USER_ID = Query.QueryPQ_User.intance().user_id
                        });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {

                    //XtraMessageBox.Show("Đã lưu");
                    //json = "";
                    //memoEdit2.Text = "";
                    //ckinsertnk = true;
                }
            }
        }
        void nhatkychitiet()
        {
            gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);
            string sql = "INSERT INTO `NHAT_KY_SU_DUNG_CHI_TIET`(ID_NK_SD,`MA_BO`, `TEN_BO`, `IS_DAT`,TRANG_THAI) VALUES " +
                        " (@ID_NK_SD,@MA_BO,@TEN_BO,@IS_DAT,@TRANG_THAI) ";
            json = string.Concat("[", json, "]");
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAT_KY_SU_DUNG_CHI_TIET>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    //DataRow r = (DataRow)rows[i];
                    NHAT_KY_SU_DUNG_CHI_TIET chitiet = new NHAT_KY_SU_DUNG_CHI_TIET();
                    chitiet.ID_NK_SD = idinssertnk_chitiet;
                    chitiet.MA_BO = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO"));
                    chitiet.TEN_BO = gridView1.GetRowCellValue(i, "TEN_BO").ToString();
                    chitiet.IS_DAT = 1;
                    chitiet.TRANG_THAI = (int)BO_THIET_BI.STATUS.CO_THE_SU_DUNG;

                    list.Add(chitiet);
                }
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    //XtraMessageBox.Show("Đã lưu");
                    //json = "";
                    //memoEdit2.Text = "";
                }
            }
        }

        ArrayList rows = new ArrayList();
        
        void UPDATESTATUSBOTHIETBI()
        {
            string sql = "UPDATE `BO_THIET_BI` SET `MUC_DO_SU_DUNG`= " + BO_THIET_BI.STATUS.DANG_MO + " WHERE MA_BO IN";
            StringBuilder sCommand = new StringBuilder("UPDATE `BO_THIET_BI` SET `MUC_DO_SU_DUNG`= 1 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                //List<int> Rows = new List<int>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    //object mabo = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO"));
                    //rows.Add(gridView1.GetDataRow(i));
                    dsTam.Add(Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")));
                    //object hht = gridView1.GetRowCellValue(i, "MA_BO");
                    //rows.Add(gridView1.GetDataRow(i));
                    //dsTam.Add(Int32.Parse(hanghoaid.ToString()));
                    //Rows.Add(string.Format("{0}", hanghoaid));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if(a > 0)
                    {
                        //XtraMessageBox.Show("Đã lưu");
                    }
                }
                //var affectedRows = connection.Execute(sql,
                //    list
                //);
                //if (affectedRows > 0)
                //{
                //    XtraMessageBox.Show("Đã lưu");
                //    json = "";
                //    memoEdit2.Text = "";
                //}
            }
        }
        void UPDATESTATUSTHIETBI()
        {
            StringBuilder sCommand = new StringBuilder("UPDATE `THIET_BI` SET `IS_SU_DUNG`= 1 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                //List<int> Rows = new List<int>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    //object mabo = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO"));
                    //rows.Add(gridView1.GetDataRow(i));
                    dsTam.Add(Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")));
                    //object hht = gridView1.GetRowCellValue(i, "MA_BO");
                    //rows.Add(gridView1.GetDataRow(i));
                    //dsTam.Add(Int32.Parse(hanghoaid.ToString()));
                    //Rows.Add(string.Format("{0}", hanghoaid));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if (a > 0)
                    {
                        //ckupdatebtb = true;
                    }
                }
            }
        }

        void selectid()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "select max(ID_NK_SD) from NHAT_KY_SU_DUNG";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    idinssertnk_chitiet = rd.GetInt32(0);
                }
                else
                    idinssertnk_chitiet = 1;
                rd.Close();
            }
            catch
            {
                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        public void RunTransaction(string myConnString)
        {
            MySqlConnection myConnection = new MySqlConnection(myConnString);
            myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
            MySqlTransaction myTrans;

            // Start a local transaction
            myTrans = myConnection.BeginTransaction();
            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            try
            {
                xuatthietbi();
                selectid();
                nhatkychitiet();
                UPDATESTATUSBOTHIETBI();
                UPDATESTATUSTHIETBI();
                myTrans.Commit();
                gridControl1.DataSource = null;

                txtCaMo.Text = "";
                memoEdit1.Text = "";
                DataTable datatable = new DataTable();
                datatable.Columns.Add("MA_BO");
                datatable.Columns.Add("TEN_BO");
                datatable.Columns.Add("IS_DAT");
                datatable.Columns.Add("TRANG_THAI");

                gridControl1.DataSource = datatable;
                lookUpEditBoThietBi1.Properties.DataSource = null;
                lookUpEditBoThietBi1.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG and NHOM_BO_ID = @NHOM_BO_ID and MUC_DO_SU_DUNG = @MUC_DO_SU_DUNG", new { SU_DUNG = 1, NHOM_BO_ID = lookUpEditNhomBo1.EditValue, MUC_DO_SU_DUNG = 0 }, commandType: CommandType.Text);
                lookUpEditBoThietBi1.Properties.ValueMember = "MA_BO";
                lookUpEditBoThietBi1.Properties.DisplayMember = "TEN_BO";
                XtraMessageBox.Show("Đã tạo phiếu xuất thiết bị "+NK_SD_MA+"");
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (MySqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");
                    }
                }

                Console.WriteLine("An exception of type " + e.GetType() +
                " was encountered while inserting the data.");
                Console.WriteLine("Neither record was written to database.");
            }
            finally
            {
                myConnection.Close();
            }
        }

        void trathietbi()
        {
            string sql = "UPDATE `SU_DUNG` SET IS_SU_DUNG= 0, NGAY_TRA = now() WHERE ID = @ID";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAT_KY_SU_DUNG>();
                list.Add(new NHAT_KY_SU_DUNG()
                {
                    ID_NK_SD = maid
                });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã lưu");
                }
            }
        }
        string json,json1;
        private void lookUpEditBoThietBi_EditValueChanged(object sender, EventArgs e)
        {
            addbo();
            //lookUpEditThietBi.Properties.DataSource = connection.Query<THIET_BI>("select * from THIET_BI where SU_DUNG = @SU_DUNG and MA_BO = @MA_BO", new { SU_DUNG = 1, MA_BO = lookUpEditBoThietBi1.EditValue }, commandType: CommandType.Text);
            //lookUpEditThietBi.Properties.ValueMember = "MA_THIET_BI";
            //lookUpEditThietBi.Properties.DisplayMember = "TEN_THIET_BI";

            //dynamic flexible = new ExpandoObject();
            //flexible.Mabo = Convert.ToInt32(lookUpEditBoThietBi1.EditValue);
            //flexible.Tenbo = lookUpEditBoThietBi1.Text;
            //flexible.Trangthai = 1;

            //string j = Newtonsoft.Json.JsonConvert.SerializeObject(flexible);
            //j = string.Concat(j,",");
            //json += j;
            //memoEdit2.Text = json;
        }
        List<int> ds = new List<int>();
        void addbo()
        {
            
            if (gridView1.RowCount < 1)
            {
                gridView1.AddNewRow();
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_BO", lookUpEditBoThietBi1.EditValue);
                gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN_BO", lookUpEditBoThietBi1.Text);
                ds.Add((int)lookUpEditBoThietBi1.EditValue);
            }
            else
            {
                int i = (int)lookUpEditBoThietBi1.EditValue;
                bool ck = ds.Contains(i);
                if(ck == false)
                {
                    gridView1.AddNewRow();
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_BO", lookUpEditBoThietBi1.EditValue);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN_BO", lookUpEditBoThietBi1.Text);
                    ds.Add((int)lookUpEditBoThietBi1.EditValue);
                }
                else
                {
                    XtraMessageBox.Show("Mã thiết bị "+i+" đã có trong danh sách");
                    return;
                }
                
            }
            
        }
        int c = 0;
        void checksudung()
        {
            try
            {

                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "SELECT IS_SU_DUNG from SU_DUNG where MA_BO = "+lookUpEditBoThietBi1.EditValue+"";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    c = rd.GetInt16(0);
                    if(c == 1)
                    {
                        XtraMessageBox.Show("Bộ thiết bị " + lookUpEditBoThietBi1.Text + " đang được sử dụng");
                        return;
                    }
                }
                else
                    rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            json = "";
            memoEdit2.Text = "";
        }

        private void lookUpEditNguoiNhan_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                frmPhongAdd f = new frmPhongAdd(this);
                f.ShowDialog();
            }
        }

        public void loadPhong()
        {
            lookUpEditPhong.Properties.DataSource = connection.Query<PHONG>("select * from PHONG where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditPhong.Properties.ValueMember = "ID";
            lookUpEditPhong.Properties.DisplayMember = "TEN_PHONG";
        }

        private void lookUpEditNguoiNhan_ButtonClick_1(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                frmNhanVien f = new frmNhanVien(this);
                f.ShowDialog();
            }
        }

        public void loadNhanvien()
        {
            lookUpEditNguoiNhan.Properties.DataSource = connection.Query<NHAN_VIEN>("select * from NHAN_VIEN where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNguoiNhan.Properties.ValueMember = "MA_NHAN_VIEN";
            lookUpEditNguoiNhan.Properties.DisplayMember = "TEN_NHAN_VIEN";
        }

        private void lookUpEditThietBi_EditValueChanged(object sender, EventArgs e)
        {
            dynamic flexible = new ExpandoObject();
            flexible.Mathietbi = Convert.ToInt32(lookUpEditThietBi.EditValue);
            flexible.Tenthietbi = lookUpEditThietBi.Text;

            string j = Newtonsoft.Json.JsonConvert.SerializeObject(flexible);
            j = string.Concat(j, ",");
            json1 += j;
            memoEdit3.Text = json1;
        }

        private void frmSuDung_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void lookUpEditNhomBo1_EditValueChanged(object sender, EventArgs e)
        {
            lookUpEditBoThietBi1.Properties.DataSource = connection.Query<BO_THIET_BI>("select MA_BO,TEN_BO from BO_THIET_BI where SU_DUNG = @SU_DUNG and NHOM_BO_ID = @NHOM_BO_ID and MUC_DO_SU_DUNG = @MUC_DO_SU_DUNG", new { SU_DUNG = 1, NHOM_BO_ID = lookUpEditNhomBo1.EditValue, MUC_DO_SU_DUNG = 0 }, commandType: CommandType.Text);
            lookUpEditBoThietBi1.Properties.ValueMember = "MA_BO";
            lookUpEditBoThietBi1.Properties.DisplayMember = "TEN_BO";
        }
        void taomanhatky()
        {
            try
            {

                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "TaoMaNhatKy";
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    NK_SD_MA = rd.GetString(2);
                }
                else
                    rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }

        }

        void selectpqgroup()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "select OPTION1 from PQ_GROUP where GROUP_ID = "+QueryPQ_User.intance().user_id+" ";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    listgrouppq = rd.GetString(0);
                    //List<string> a = listgrouppq.Split(',').ToList();
                    //List<int> myStringList = a.Select(s => int.Parse(s)).ToList();
                }
                else
                    return;
                rd.Close();
            }
            catch
            {
                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            ds.Clear();
            lookUpEditNhomBo1.Properties.DataSource = null;
            lookUpEditBoThietBi1.Properties.DataSource = null;
            
            frmBoThietBiAdd_Load(sender, e);
            lookUpEditNhomBo1.EditValue = 0;
        }
        
    }
}