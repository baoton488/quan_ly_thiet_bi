﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Configuration;
using QLKhachHang.Query;
using Dapper;
using QLKhachHang;

namespace QLTB.QLTV.DanhMuc
{
    public partial class frmNKTB_Tra : DevExpress.XtraEditors.XtraUserControl
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        public frmNKTB_Tra()
        {
            InitializeComponent();
        }

        private void frmTraThietBi_Load(object sender, EventArgs e)
        {
            loadthietbi();
        }

        void loadthietbi()
        {
            lookUpEdit1.Properties.DataSource = connection.Query<NHAT_KY_SU_DUNG>("select * from NHAT_KY_SU_DUNG where IS_SU_DUNG = @IS_SU_DUNG order by ID_NK_SD desc", new { IS_SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEdit1.Properties.ValueMember = "ID_NK_SD";
            lookUpEdit1.Properties.DisplayMember = "NK_SD_MA";
        }
        

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridControl1.DataSource = connection.Query<NHAT_KY_SU_DUNG_CHI_TIET>("select * from NHAT_KY_SU_DUNG_CHI_TIET where ID_NK_SD = @ID_NK_SD", new { ID_NK_SD = lookUpEdit1.EditValue }, commandType: CommandType.Text);           
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);
            RunTransaction(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        }
        public void RunTransaction(string myConnString)
        {
            MySqlConnection myConnection = new MySqlConnection(myConnString);
            myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
            MySqlTransaction myTrans;

            // Start a local transaction
            myTrans = myConnection.BeginTransaction();
            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            try
            {
                trathietbi();
                UPDATESTATUSBOTHIETBI();
                UPDATESTATUSTHIETBI();
                //UPDATESL_SDBOTHIETBI();
                //UPDATESL_SDTHIETBI();
                myTrans.Commit();
                gridControl1.DataSource = null;
                XtraMessageBox.Show("Đã trả thiết bị");
                loadthietbi();
                gridControl1.DataSource = null;
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (MySqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");
                    }
                }

                Console.WriteLine("An exception of type " + e.GetType() +
                " was encountered while inserting the data.");
                Console.WriteLine("Neither record was written to database.");
            }
            finally
            {
                myConnection.Close();
            }
        }
        void trathietbi()
        {
            string sql = "UPDATE `NHAT_KY_SU_DUNG` SET IS_SU_DUNG= 0, NGAY_TRA = now() WHERE ID_NK_SD = @ID_NK_SD";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAT_KY_SU_DUNG>();
                list.Add(new NHAT_KY_SU_DUNG()
                {
                    ID_NK_SD = Convert.ToInt32(lookUpEdit1.EditValue)
                });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    
                }
            }
        }
        void UPDATESTATUSBOTHIETBI()
        {
            StringBuilder sCommand = new StringBuilder("UPDATE `BO_THIET_BI` SET `MUC_DO_SU_DUNG`= 0 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                for (int i = 0; i < gridView1.RowCount; i++)
                {                   
                    dsTam.Add(Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")));

                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if (a > 0)
                    {
                        
                    }
                }

            }
        }

        void UPDATESTATUSTHIETBI()
        {
            StringBuilder sCommand = new StringBuilder("UPDATE `THIET_BI` SET `IS_SU_DUNG`= 0 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                //List<int> Rows = new List<int>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    dsTam.Add(Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if (a > 0)
                    {
                        //ckupdatebtb = true;
                    }
                }
            }
        }
        void UPDATESL_SDTHIETBI()
        {
            StringBuilder sCommand = new StringBuilder("UPDATE `THIET_BI` SET `SO_LAN_SU_DUNG`= SO_LAN_SU_DUNG + 1 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                //List<int> Rows = new List<int>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    dsTam.Add(Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if (a > 0)
                    {
                        //ckupdatebtb = true;
                    }
                }
            }
        }

        void UPDATESL_SDBOTHIETBI()
        {
            StringBuilder sCommand = new StringBuilder("UPDATE `BO_THIET_BI` SET `SO_LAN_SU_DUNG`= SO_LAN_SU_DUNG + 1 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                //List<int> Rows = new List<int>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    dsTam.Add(Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_BO")));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if (a > 0)
                    {
                        //ckupdatebtb = true;
                    }
                }
            }
        }


        private void simpleButton2_Click(object sender, EventArgs e)
        {
            loadthietbi();
        }
    }
}