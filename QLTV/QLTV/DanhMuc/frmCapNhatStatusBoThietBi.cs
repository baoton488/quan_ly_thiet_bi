﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QLKhachHang.Query;
using MySql.Data.MySqlClient;
using System.Configuration;
using Dapper;

namespace QLTB.QLTV.DanhMuc
{
    public partial class frmCapNhatStatusBoThietBi : DevExpress.XtraEditors.XtraUserControl
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        public frmCapNhatStatusBoThietBi()
        {
            InitializeComponent();
            gridView1.RowCellClick += gridView_RowCellClick;
        }

        private void gridView_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            XtraMessageBox.Show("aaa");
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridControl1.DataSource = connection.Query<BO_THIET_BI1>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG and NHOM_BO_ID = @NHOM_BO_ID and MUC_DO_SU_DUNG >= 1", new { SU_DUNG = 1, NHOM_BO_ID = lookUpEdit1.EditValue }, commandType: CommandType.Text);
            gridView1.RowCellClick += gridView_RowCellClick;
        }

        private void frmCapNhatStatusBoThietBi_Load(object sender, EventArgs e)
        {
            lookUpEdit1.Properties.DataSource = connection.Query<NHOM_BO>("select * from NHOM_BO where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEdit1.Properties.ValueMember = "NHOM_BO_ID";
            lookUpEdit1.Properties.DisplayMember = "NHOM_BO_TEN";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //update();
        }
        int a;
        void update()
        {
            string sql = "UPDATE `BO_THIET_BI` SET MUC_DO_SU_DUNG = @MUC_DO_SU_DUNG , USER_ID = @USER_ID , NGAY_UPDATE_TRANG_THAI = now() WHERE MA_BO = @MA_BO";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<BO_THIET_BI>();
                list.Add(new BO_THIET_BI()
                {
                    MUC_DO_SU_DUNG = (BO_THIET_BI.STATUS)a,
                    MA_BO = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle,"MA_BO")),
                    USER_ID = QueryPQ_User.intance().user_id
                });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {

                }
            }

            string sql1 = "UPDATE `THIET_BI` SET IS_SU_DUNG= @IS_SU_DUNG WHERE MA_BO = @MA_BO";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<THIET_BI>();
                list.Add(new THIET_BI()
                {
                    IS_SU_DUNG = Convert.ToInt16((BO_THIET_BI.STATUS)a),
                    MA_BO = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "MA_BO")),
                });
                var affectedRows = connection.Execute(sql1,
                    list
                );
                if (affectedRows > 0)
                {

                }
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {

        }

        private void gridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);
            int i = (int)(BO_THIET_BI.STATUS)Enum.Parse(typeof(BO_THIET_BI.STATUS), e.Value.ToString());
            a = i;
            update();
        }

        
    }
}