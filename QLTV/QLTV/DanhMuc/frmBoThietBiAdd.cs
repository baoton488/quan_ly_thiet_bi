﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
using System.Configuration;

namespace QLKhachHang
{
    public partial class frmBoThietBiAdd : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryBO_THIET_BI qr = new Query.QueryBO_THIET_BI();
        public XRPictureBox picture = new XRPictureBox();
        int maid;
        private readonly frmThietBi frm1;
        public frmBoThietBiAdd()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        public frmBoThietBiAdd(frmThietBi frm)
        {
            InitializeComponent();
            frm1 = frm;
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            if (checkEdit1.Checked == true)
            {
                update();
            }
            else
            {
                luu();
            }
        }

        void luu()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.queryaddbothietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.TEN_BO, MySqlDbType.VarChar).Value = txtTenBo.Text;
                cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtGhiChu.Text;
                cmd.Parameters.Add(qr.NHOM_BO_ID, MySqlDbType.VarChar).Value = lookUpEditNhomBo.EditValue;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã lưu");
                    txtTenBo.Text = "";
                    txtGhiChu.Text = "";
                    gridControl1.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
                    frm1.loadbothietbi();
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void update()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.queryupdatebothietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.TEN_BO, MySqlDbType.VarChar).Value = txtTenBo.Text;
                cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtGhiChu.Text;
                cmd.Parameters.Add(qr.MA_BO, MySqlDbType.Int32).Value = maid;
                cmd.Parameters.Add(qr.NHOM_BO_ID, MySqlDbType.VarChar).Value = lookUpEditNhomBo.EditValue;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã lưu");
                    txtTenBo.Text = "";
                    txtGhiChu.Text = "";
                    gridControl1.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void delete()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.querydeletebothietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.SU_DUNG, MySqlDbType.Int32).Value = 0;
                cmd.Parameters.Add(qr.MA_BO, MySqlDbType.Int32).Value = maid;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã xóa");
                    gridControl1.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void frmBoThietBiAdd_Load(object sender, EventArgs e)
        {
            gridControl1.Enabled = false;
            gridControl1.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhomBo.Properties.DataSource = connection.Query<NHOM_BO>("select * from NHOM_BO where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhomBo.Properties.ValueMember = "NHOM_BO_ID";
            lookUpEditNhomBo.Properties.DisplayMember = "NHOM_BO_TEN";
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked == true)
            {
                gridControl1.Enabled = true;
            }
            else
            {
                gridControl1.Enabled = false;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                delete();
            }
            
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;
                    object ten;
                    object ghichu;


                    id = view.GetRowCellValue(view.FocusedRowHandle, "MA_BO");
                    ten = view.GetRowCellValue(view.FocusedRowHandle, "TEN_BO");
                    ghichu = view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU");

                    maid = Convert.ToInt32(id);
                    txtTenBo.Text = ten.ToString();
                    txtGhiChu.Text = ghichu.ToString();
                }
            }
            catch { }
        }
    }
}