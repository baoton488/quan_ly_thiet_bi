﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraReports.UI;
using System.Collections;
using QLTV.Report;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using System.Configuration;
using QLKhachHang.Query;
using Dapper;
using QLTV.QLTV.DanhMuc;
using System.Text.RegularExpressions;
using System.IO;
//using QLTV.QLTV.GiaoDichVang;

namespace QLKhachHang.QLTV.GiaoDichVang
{
    public partial class frmKiemTraThietBi : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        int mabothietbi;
        int LayamaXoa;
        //List<int> dsID = new List<int>();
        bool state = true;
        Connect cn = new Connect();
        DataTable datatable = new DataTable();
        Query.QueryTonKho qrtk = new Query.QueryTonKho();
        DateTime ngayxuat;
        string maphieuxuat;
        int maid;
        decimal tonkho;
        int hanghoaid3;
        bool statesavephx;
        bool statesavechitietphx;
        bool statesaveupdate;
        string getCanTong;
        decimal setCanTong;
        decimal tinhtruhot;

        public decimal tongtien = 0;

        public GridControl gridcontrol = new GridControl();
        public GridView gridview = null;
        object summaryThanhtien;
        object summarySoLuong;
        object summaryCanTong;
        object summaryTL_Hot;
        object summaryGiaCong;

        bool check = false;
        string checktonkho;

        public Decimal tinhtienvang;
        public string tentiem, diachi,dt,tieu_de_pc,tieu_de_pb;
        int kiem_tra_so_luong;
        int so_luong_thuc_te_trong_bo;

        public int id_lichsukiemtra;
        public string ghichu_lichsukiemtra;
        public frmKiemTraThietBi()
        {
            InitializeComponent();
            //this.KeyUp += new KeyEventHandler(frmGiaoDich_KeyUp);
        }
        public void test()
        {
            XtraMessageBox.Show(ghichu_lichsukiemtra);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }
        object row;
        
        
        private void frmGiaoDich_Load(object sender, EventArgs e)
        {
            lookUpEditNhomBo.Properties.DataSource = connection.Query<NHOM_BO>("select * from NHOM_BO where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhomBo.Properties.ValueMember = "NHOM_BO_ID";
            lookUpEditNhomBo.Properties.DisplayMember = "NHOM_BO_TEN";

            lookUpEditBoThietBi.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditBoThietBi.Properties.ValueMember = "MA_BO";
            lookUpEditBoThietBi.Properties.DisplayMember = "TEN_BO";
            this.KeyPreview = true;
            //loadlookupkhachhang();
            loadDatatable();
            
        }
        void loadDatatable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("MA_BO");
            datatable.Columns.Add("MA_THIET_BI");
            datatable.Columns.Add("TEN_THIET_BI");
            datatable.Columns.Add("TEN_BO");
            datatable.Columns.Add("QUY_CACH");
            datatable.Columns.Add("SO_LUONG");
            datatable.Columns.Add("THIET_BI_MA");
            datatable.Columns.Add("SL_SU_DUNG");
            gridControl1.DataSource = datatable;
        }

        
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            gridView1.AddNewRow();
        }

        private void gvHangHoa_ShownEditor(object sender, EventArgs e)
        {
            try
            {
                ColumnView view = (ColumnView)sender;
                if (view.FocusedColumn.FieldName == "HANGHOAMA" && view.ActiveEditor is LookUpEdit)
                {
                    //row = (string)view.GetFocusedRowCellValue("HANGHOAMA");
                    row = view.GetRowCellValue(view.FocusedRowHandle, "HANGHOAMA").ToString();
                    //IList<Car> filteredCars = _controller.GetCarsByType(carTypeId);
                    //edit.Properties.DataSource = filteredCars;
                }
            }
            catch
            {
                //Log
            }
        }
        void edit_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit edit = sender as LookUpEdit;
            DataRowView rowView = (DataRowView)edit.GetSelectedDataRow();
            DataRow row = rowView.Row;
        }

        string a;
        public static void CopyValues(DataRow sourceDataRow, DataRow targetDataRow)
        {
            try
            {
                if (sourceDataRow == null || targetDataRow == null)
                    return;
                targetDataRow["HANGHOAID"] = sourceDataRow["HANGHOAID"];
                targetDataRow["HANG_HOA_TEN"] = sourceDataRow["HANG_HOA_TEN"];
                targetDataRow["NHOM_TEN"] = sourceDataRow["NHOM_TEN"];
                targetDataRow["CAN_TONG"] = sourceDataRow["CAN_TONG"];
                targetDataRow["TL_HOT"] = sourceDataRow["TL_HOT"];
                targetDataRow["GIA_CONG"] = sourceDataRow["GIA_CONG"];
                targetDataRow["DON_GIA_BAN"] = sourceDataRow["DON_GIA_BAN"];
                targetDataRow["SO_LUONG"] = 1;
                object tt = sourceDataRow["DON_GIA_BAN"];
                decimal thanhtien = Decimal.Parse(tt.ToString());
                targetDataRow["THANH_TIEN"] = 1 * thanhtien;
                
            }
            catch { }
        }
        long id2;
        string ma2;
        void addatarow()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from danh_muc_hang_hoa where su_dung = 1 and HANGHOAMA= '" + a + "'"; ;
                cmd.Connection = cn.conn;
                //DataTable datatable = new DataTable();
                //datatable.Columns.Add("HANGHOAID");
                //datatable.Columns.Add("HANGHOAMA");
                //datatable.Columns.Add("LOAIID");
                //datatable.Columns.Add("DVTID");
                //datatable.Columns.Add("NHOMHANGID");
                //datatable.Columns.Add("NCCID");
                //datatable.Columns.Add("GIAM_GIA_ID");
                //datatable.Columns.Add("HANG_HOA_TEN");
                //datatable.Columns.Add("GIA_BAN");
                //datatable.Columns.Add("VAT");
                //datatable.Columns.Add("THUE");
                //datatable.Columns.Add("SU_DUNG");
                //datatable.Columns.Add("SL_IN");
                //datatable.Columns.Add("GHI_CHU");
                //datatable.Columns.Add("TAO_MA");
                //datatable.Columns.Add("GIA_BAN_SI");
                //datatable.Columns.Add("CAN_TONG");
                //datatable.Columns.Add("TL_HOT");
                //datatable.Columns.Add("GIA_CONG");
                //datatable.Columns.Add("DON_GIA_GOC");
                //datatable.Columns.Add("CONG_GOC");
                //datatable.Columns.Add("TUOI_BAN");
                //datatable.Columns.Add("TUOI_MUA");
                // datatable.Columns.Add("LOAI_TEN");
                //     datatable.Columns.Add("SU_DUNG");
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    DataRow row = datatable.NewRow();
                    row["HANGHOAID"] = rd["HANGHOAID"];
                    id2 = Int64.Parse(rd["HANGHOAID"].ToString());
                    row["HANGHOAMA"] = rd["HANGHOAMA"];
                    ma2 = rd["HANGHOAMA"].ToString();
                    row["LOAIID"] = rd["LOAIID"];
                    row["DVTID"] = rd["DVTID"];
                    row["NHOMHANGID"] = rd["NHOMHANGID"];
                    row["NCCID"] = rd["NCCID"];
                    row["GIAM_GIA_ID"] = rd["GIAM_GIA_ID"];
                    row["HANG_HOA_TEN"] = rd["HANG_HOA_TEN"];
                    row["GIA_BAN"] = rd["GIA_BAN"];
                    row["VAT"] = rd["VAT"];
                    row["THUE"] = rd["THUE"];
                    row["SU_DUNG"] = rd["SU_DUNG"];
                    row["SL_IN"] = rd["SL_IN"];
                    row["GHI_CHU"] = rd["GHI_CHU"];
                    row["TAO_MA"] = rd["TAO_MA"];
                    row["GIA_BAN_SI"] = rd["GIA_BAN_SI"];
                    row["CAN_TONG"] = rd["CAN_TONG"];
                    row["TL_HOT"] = rd["TL_HOT"];
                    row["GIA_CONG"] = rd["GIA_CONG"];
                    row["DON_GIA_GOC"] = rd["DON_GIA_GOC"];
                    row["CONG_GOC"] = rd["CONG_GOC"];
                    row["TUOI_BAN"] = rd["TUOI_BAN"];
                    row["TUOI_MUA"] = rd["TUOI_MUA"];
                    // row["LOAI_TEN"] = rd["LOAI_TEN"];
                    //     row["SU_DUNG"] = rd["SU_DUNG"];
                   // datatable.Rows.Add(row);
                    datatable.Rows.InsertAt(row, datatable.Rows.Count);
                    
                }
               // gcHangHoa.DataSource = datatable;
                //datatable += datatable;
                rd.Close();
               // gcHangHoa.DataSource = datatable;
                // treeList1.DataSource = datatable;
                
               // gvHangHoa.UpdateCurrentRow();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }     

        private void repositoryItemLookUpEdit2_EditValueChanged(object sender, EventArgs e)
        {            
            
        }

        
        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                gridView1.DeleteRow(gridView1.FocusedRowHandle);
                state = true;
               // dsID.Remove(LayamaXoa);
                setvaluegridview();
            }
            catch { }
        }
        ArrayList rows = new ArrayList();
        List<long> dsTam = new List<long>();   

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.RowCount < 1)
                {
                    XtraMessageBox.Show("Bạn chưa chọn thêm hàng hóa");
                    return;
                }
                checktonkhokhiban();
                if (ck2 == true)
                {
                    ck2 = false;
                    //XtraMessageBox.Show("Mã " + checktonkho + " đã xuất kho rồi");
                    return;
                }
                //if (searchLookUpEditKhachHang.Text == "")
                //{
                //    XtraMessageBox.Show("Bạn vui lòng kiểm tra lại dữ liệu");
                //    return;
                //}
                else
                {
                    string In = System.Configuration.ConfigurationManager.AppSettings["In"];
                    if (In == "")
                    {
                        XtraMessageBox.Show("Bạn vui lòng thay đổi thiết lập hệ thống");
                        return;
                    }

                    if (statesavechitietphx == true && statesavephx == true && statesaveupdate == true)
                    {     
                        if (In == "1")
                        {
                            //if (DialogResult.Yes == XtraMessageBox.Show("Giao dịch thành công, Bạn có muốn in phiếu " + maphieuxuat + " không? ", "Thông báo", MessageBoxButtons.YesNo))
                            
                            XtraReportBanVang rpt = new XtraReportBanVang(tentiem, diachi, maid, maphieuxuat, dt,tieu_de_pb,tieu_de_pc);
                            rpt.Parameters["parameter1"].Value = maid + 1;
                            rpt.Parameters["parameter2"].Value = maphieuxuat;
                            rpt.Name = "In phiếu xuất";
                            rpt.CreateDocument(true);
                            rpt.Print();
                        }
                        if (In == "0")
                        {
                            
                        }
                        textEdit1.Focus();

                        frmGiaoDich_Load(sender, e);
                        //checkPhieu();
                    }
                    else
                    {
                        XtraMessageBox.Show("Có lỗi xảy ra trong quá trình giao dịch, Bạn vui lòng liên hệ với nhà phát triển");
                    }
                }
            }
            catch { }
            
        }

        
        void setvaluegridview()
        {
            gridControl1.ForceInitialize();
            gridView1.UpdateCurrentRow();
            summaryThanhtien = gridView1.Columns["THANH_TIEN"].SummaryItem.SummaryValue;
            summarySoLuong = gridView1.Columns["SO_LUONG"].SummaryItem.SummaryValue;
            summaryCanTong = gridView1.Columns["CAN_TONG"].SummaryItem.SummaryValue;
            summaryTL_Hot = gridView1.Columns["TL_HOT"].SummaryItem.SummaryValue;
            summaryGiaCong = gridView1.Columns["GIA_CONG"].SummaryItem.SummaryValue;
        }
        string getNgayThangNam;
        //void getDatimeServer()
        //{
        //    try
        //    {
        //        cn.openconnection();
        //        MySqlCommand cmd = new MySqlCommand();
        //        cmd.CommandType = CommandType.Text;
        //        string sql = "SELECT CURRENT_TIMESTAMP";
        //        cmd.CommandText = sql;
        //        cmd.Connection = cn.conn;
        //        MySqlDataReader rd = cmd.ExecuteReader();
        //        if (rd.Read())
        //        {
        //            DateTime date = rd.GetDateTime(0);
        //            //date = date.AddHours(1);
        //            ngayxuat = date;
        //            string px = "PX";
        //            string thang = date.Month.ToString();
        //            if(thang.Length < 2)
        //            {
        //                thang = string.Concat("0", thang);
        //            }
        //            string nam = date.Year.ToString();
        //            nam = Right(nam, 2);
        //            string ngay = date.Day.ToString();
        //            if(ngay.Length < 2)
        //            {
        //                ngay = string.Concat("0", ngay);
        //            }
        //            getNgayThangNam = string.Concat(px, nam, thang, ngay);
        //            //maphieuxuat = string.Concat(px, nam, thang, ngay, maid);
        //        }

        //        rd.Close();

        //    }
        //    catch(Exception ex)
        //    {
        //          XtraMessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        cn.closeconnection();
        //        cn.conn.Dispose();
        //    }
        //}

        //void SelectNgayThangNamTonTai()
        //{
        //    try
        //    {
        //        cn.openconnection();
        //        MySqlCommand cmd = new MySqlCommand();
        //        cmd.CommandType = CommandType.Text;
        //        string sql = string.Format("SELECT `PHIEU_XUAT_MA` FROM phx_phieu_xuat WHERE PHIEU_XUAT_MA like '%{0}%' ORDER by PHIEU_XUAT_MA DESC limit 1",getNgayThangNam);
        //        cmd.CommandText = sql;
        //        cmd.Connection = cn.conn;
        //        MySqlDataReader rd = cmd.ExecuteReader();
        //        if (rd.Read())
        //        {
        //            string LayPhieuMa = rd.GetString(0);
        //            string get3kytucuoi = Right(LayPhieuMa, 3);
        //            long tangmacuoi = Int64.Parse(get3kytucuoi) + 1;
        //            string taomacuoi = tangmacuoi.ToString();
        //            if(taomacuoi.Length == 1)
        //            {
        //                taomacuoi = string.Concat("00", taomacuoi);
        //            }
        //            if(taomacuoi.Length == 2)
        //            {
        //                taomacuoi = string.Concat("0",taomacuoi);
        //            }
        //            maphieuxuat = string.Concat(getNgayThangNam,taomacuoi);
        //        }
        //        else
        //        {
        //            maphieuxuat = string.Concat(getNgayThangNam, "001");
        //        }
        //        rd.Close();

        //    }
        //    catch
        //    {
        //        maphieuxuat = string.Concat(getNgayThangNam,"001");
        //        //XtraMessageBox.Show(ex.Message);
        //    }
        //    finally
        //    {
        //        cn.closeconnection();
        //        cn.conn.Dispose();
        //    }
        //}
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);
            if (gridView1.RowCount < 1)
            {
                gridView1.AddNewRow();
                selectmahanghoa2();
                textEdit1.Text = "";
                textEdit1.Focus();
            }
            else
            {
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    try
                    {
                        int a = Convert.ToInt32(textEdit1.EditValue);
                        int b = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI"));

                        if (b == null)
                        {
                            b = 0;
                        }
                        long b2 = Int64.Parse(b.ToString());
                        if (a == b2)
                        {
                            state = false;
                            int sl = Convert.ToInt32(gridView1.GetRowCellValue(i, "SO_LUONG"));
                            sl = sl + 1;
                            if(kiem_tra_so_luong < sl)
                            {
                                XtraMessageBox.Show("Số lượng đã vượt quá số lượng có sẵn");
                                return;
                            }
                            gridView1.SetRowCellValue(i, "SO_LUONG", sl);
                            gridView1.FocusedRowHandle = i;
                            break;
                        }
                        else
                        {
                            state = true;
                        }
                            
                    }
                    catch {
                        //gridView1.DeleteRow(gridView1.FocusedRowHandle);
                    }
                }
                if(state == true)
                {
                    gridView1.AddNewRow();
                    selectmahanghoa2();
                    state = false;
                }
                textEdit1.Text = "";
                textEdit1.Focus();
            
            }
            gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);
          
        }

        public static string GetUntilOrEmpty(string text, string stopAt = ".")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }


        void selectmahanghoa2()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = string.Format("SELECT THIET_BI.MA_BO,THIET_BI.MA_THIET_BI,TEN_THIET_BI,BO_THIET_BI.TEN_BO as 'TEN_BO',QUY_CACH,TON_KHO.SL_TON as SL_TON, THIET_BI.THIET_BI_MA,TON_KHO.SL_SU_DUNG FROM `THIET_BI`,BO_THIET_BI, TON_KHO WHERE THIET_BI.MA_BO = BO_THIET_BI.MA_BO and THIET_BI.SU_DUNG = 1 and TON_KHO.MA_THIET_BI = THIET_BI.MA_THIET_BI and TON_KHO.MA_BO = " + mabothietbi + " and TON_KHO.MA_THIET_BI LIKE N'{0}'", textEdit1.Text);
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    kiem_tra_so_luong = Convert.ToInt32(rd["SL_TON"]);

                    if(kiem_tra_so_luong == 0)
                    {
                        XtraMessageBox.Show("Thiết bị này chưa có tồn kho");
                        gridView1.DeleteRow(gridView1.FocusedRowHandle);
                        return;
                    }
                    
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_BO", rd.GetInt32(0));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "MA_THIET_BI", rd.GetInt32(1));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN_THIET_BI", rd.GetString(2));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "TEN_BO", rd.GetString(3));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "QUY_CACH", rd.GetString(4));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SO_LUONG", 1);
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "THIET_BI_MA", rd.GetString(6));
                    gridView1.SetRowCellValue(gridView1.FocusedRowHandle, "SL_SU_DUNG", rd.GetInt32(7));
                }
                else
                {
                    check = true;
                    gridView1.DeleteRow(gridView1.FocusedRowHandle);
                    XtraMessageBox.Show("Mã thiết bị " + textEdit1.Text + " không có trong bộ thiết bị " + lookUpEditBoThietBi.Text + "");
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }
                rd.Close();

            }
            catch
            {
                // check = true;
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void selectmahangtonkho()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "select sl_ton from ton_kho where KHOID = 1 and HANGHOAID = " + checktonkho + "";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    tonkho = rd.GetDecimal(0);
                }
                rd.Close();

            }
            catch
            {
                // check = true;
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        bool ck2 = false;
        void checktonkhokhiban()
        {
            try
            {
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    checktonkho = gridView1.GetRowCellValue(i, "HANGHOAMA").ToString();
                    cn.openconnection();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandType = CommandType.Text;
                    string sql = "select sl_ton from ton_kho where KHOID = 1 and HANGHOAID = " + checktonkho + "";
                    cmd.CommandText = sql;
                    cmd.Connection = cn.conn;
                    MySqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        tonkho = rd.GetDecimal(0);
                    }
                    rd.Close();
                    if (tonkho == 0)
                    {
                        ck2 = true;
                        XtraMessageBox.Show("Mã " + checktonkho + " đã xuất kho rồi");
                        return;
                    }
                }


            }
            catch
            {
                // check = true;
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void updatetonkho2()
        {
            StringBuilder sCommand = new StringBuilder("update ton_kho set sl_xuat = 1,sl_ton= 0 where KHOID = 1 and HANGHOAID in ");
            using (MySqlConnection mConnection = cn.conn)
            {
                List<string> Rows = new List<string>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    //DataRow r = (DataRow)rows[i];
                    object hht = gridView1.GetRowCellValue(i, "HANG_HOA_TEN");
                    object tt = gridView1.GetRowCellValue(i, "THANH_TIEN");
                    object hanghoaid = gridView1.GetRowCellValue(i, "HANGHOAID");
                    hanghoaid3 = Int32.Parse(hanghoaid.ToString());
                    object hanghoama = gridView1.GetRowCellValue(i, "HANGHOAMA");
                    object sl = gridView1.GetRowCellValue(i, "SO_LUONG");
                    object dgb = gridView1.GetRowCellValue(i, "DON_GIA_BAN");
                    object ct = gridView1.GetRowCellValue(i, "CAN_TONG");
                    object tlh = gridView1.GetRowCellValue(i, "TL_HOT");
                    object gc = gridView1.GetRowCellValue(i, "GIA_CONG");
                    object nhid = gridView1.GetRowCellValue(i, "NHOMHANGID");
                    object lv = gridView1.GetRowCellValue(i, "NHOM_TEN");
                    decimal tong = Decimal.Parse(tt.ToString());
                    tongtien = tongtien + tong;
                    rows.Add(gridView1.GetDataRow(i));
                    dsTam.Add(Int32.Parse(hanghoaid.ToString()));
                    setvaluegridview();
                    Rows.Add(string.Format("{0}",hanghoaid));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", Rows));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    myCmd.ExecuteNonQuery();
                    statesaveupdate = true;
                    //if(myCmd.ExecuteNonQuery() == gridView1.RowCount)
                    //{
                    //    statesaveupdate = true;
                    //}
                }
            }
        }
        private void gridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            gridView1.Focus();
            gridView1.RefreshData();
            gridView1.UpdateSummary();

        }

        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            gridView1.Focus();
            gridView1.RefreshData();
            gridView1.UpdateSummary();
        }

        private void gridView1_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            gridView1.Focus();
            gridView1.RefreshData();
            gridView1.UpdateSummary();
        }

        

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object s = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HANGHOAMA");
                    LayamaXoa = Int32.Parse(s.ToString());
                }
            }
            catch { }
            
        }
        void checkPhieu()
        {
            XtraReport6 rpt = new XtraReport6();
            rpt.Parameters["parameter1"].Value = 24;
            rpt.Parameters["parameter1"].Value = "PX180909008";
            rpt.CreateDocument();
            rpt.ShowPreviewDialog();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            XtraReportBanVang rpt = new XtraReportBanVang(tentiem, diachi, maid, maphieuxuat,dt,tieu_de_pb,tieu_de_pc);
            rpt.Parameters["parameter1"].Value = maid;
            rpt.Parameters["parameter2"].Value = maphieuxuat;
            rpt.Name = "In phiếu xuất";
            rpt.CreateDocument();
            rpt.Print();           
        }      

        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                gridView1.PostEditor();
                gridView1.UpdateCurrentRow();
                gridView1.FocusedRowHandle = GridControl.NewItemRowHandle;
            }
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
        
        }

        private void searchLookUpEditKhachHang_Click(object sender, EventArgs e)
        {
            
        }
        
        private void frmGiaoDich_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void frmGiaoDich_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            {
                simpleButton4_Click(sender, e);
            }
            if (e.KeyCode.Equals(Keys.F1))
            {

            }
            if (e.KeyCode == Keys.Escape)
            {
                //this.Close();
            }
        }
        void taosophieuxuat()
        {
            try
            {

                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "TaoSoPhieuXuat";
                //string sql = "set @THANGNAM = (select date_format(now(), '%y%m') );" +
                //             " set @b = (SELECT COUNT(PHIEU_MA) FROM ( select DISTINCT PHIEU_MA from cam_phieu_cam_vang ) a WHERE PHIEU_MA LIKE CONCAT('PC.',@THANGNAM,'%'))+1;"+
                //             " SET @b = CONVERT(@THANGNAM,UNSIGNED)*10000 +@b;"+
                //             " set @so_phieu2 = concat('PC.',convert(@b,UNSIGNED));"+
                //             " select @b,@THANGNAM,@so_phieu2;";
                //cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    maid = rd.GetInt32(0);
                    maphieuxuat = rd.GetString(4);
                }
                else
                    rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }

        }
        string thongbao;
        private void lookUpEditBoThietBi_EditValueChanged(object sender, EventArgs e)
        {
            mabothietbi = Convert.ToInt32(lookUpEditBoThietBi.EditValue);
        }
        string b1 = string.Empty;
        string c1 = string.Empty;
        public void xulykhicokiemtra()
        {
            string[] numbers = Regex.Split(ghichu_lichsukiemtra, @"\D+");
            foreach (string value in numbers)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    int i = int.Parse(value);
                    b1 += string.Concat(i, ",");
                    //Console.WriteLine("Number: {0}", i);
                }
            }
            c1 = b1;
            c1 = c1.Remove(c1.Length - 1);
            Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            config.AppSettings.Settings["Port"].Value = c1.ToString();
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            //string s = "SELECT THIET_BI.MA_BO,THIET_BI.MA_THIET_BI,TEN_THIET_BI,BO_THIET_BI.TEN_BO as 'TEN_BO',QUY_CACH,TON_KHO.SL_TON as SL_TON, THIET_BI.THIET_BI_MA FROM `THIET_BI`,BO_THIET_BI, TON_KHO WHERE THIET_BI.MA_BO = BO_THIET_BI.MA_BO and THIET_BI.SU_DUNG = 1 and TON_KHO.MA_THIET_BI = THIET_BI.MA_THIET_BI and TON_KHO.MA_BO = " + 1 + " and TON_KHO.MA_THIET_BI not in ("+c+")";
            //gridControl1.DataSource = connection.Query(s);
            
        }

        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            thongbao ="";
            List<string> list3 = new List<string>();
            kiemtrathietbi();
            kiemtrathucte();
            IEnumerable<int> differenceQuery = list1.Except(list2);
            foreach (int s in differenceQuery)
            {
                list3.Add(s.ToString());
            }
            thongbao = string.Concat("Các thiết bị có mã sau còn thiếu: ", String.Join(" ", list3));
            thongbao = thongbao.Trim();
            string b = string.Empty;
            string c = string.Empty;
            for (int i = 0; i < thongbao.Length; i++)
            {
                if (Char.IsDigit(thongbao[i]))
                {
                    b += string.Concat(thongbao[i], ",");
                    //c = string.Concat(b, ",");
                }
                    
            }
            c = b;
            if(c == "")
            {

            }
            else
            {
                c = c.Remove(c.Length - 1);
            }
            
            XtraMessageBoxArgs args = new XtraMessageBoxArgs();
            
            args.Caption = "Message";
            args.Text = thongbao;
            args.Buttons = new DialogResult[] { DialogResult.OK , DialogResult.Cancel,DialogResult.Retry  };
            args.Showing += args_Showing;
            //XtraMessageBox.Show(args).ToString();
            DialogResult dr = XtraMessageBox.Show(args);
            if (dr == DialogResult.OK)
            {
                if(c == "")
                {
                    insertlichsukiemtrahoantat();
                    updatesudungtonkho();
                    return;
                }
                else
                {
                    insertlichsukiemtra();
                    updatesudungtonkho();
                    return;
                }
                
            }
            if (dr == DialogResult.Cancel)
            {
                return;
            }
            if (dr == DialogResult.Retry)
            {
                frmThietBiAdd f = new frmThietBiAdd();
                f.ShowDialog();
                return;
            }

            //if(args.Button)
        }
        void args_Showing(object sender, XtraMessageShowingArgs e)
        {
            e.Buttons[DialogResult.OK].Text = "Lưu";
            e.Buttons[DialogResult.Cancel].Text = "Tiếp tục";
            e.Buttons[DialogResult.Retry].Text = "Thay thế";
        }  
        
        List<int> list1 = new List<int>();
        void kiemtrathietbi()
        {
            try
            {
                list1.Clear();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "SELECT `MA_THIET_BI` from TON_KHO WHERE TON_KHO.MA_BO = " + lookUpEditBoThietBi.EditValue + "";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    so_luong_thuc_te_trong_bo = rd.GetInt32(0);
                    int a = rd.GetInt32(0);
                    list1.Add(a);
                }

                rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        List<int> list2 = new List<int>();
        void kiemtrathucte()
        {
            list2.Clear();
            for(int i =0;i<gridView1.RowCount;i++)
            {
                int a = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI"));
                list2.Add(a);
            }
        }

        void insertlichsukiemtra()
        {

            string sql = "INSERT INTO `LICH_SU_KIEM_TRA_THIET_BI`(`MA_BO_THIET_BI`, `TEN_BO_THIET_BI`, `NGAY`, `GHI_CHU`) VALUES (@MA_BO_THIET_BI,@TEN_BO_THIET_BI,now(),@GHI_CHU)";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<LICH_SU_KIEM_TRA_THIET_BI>();
                list.Add(new LICH_SU_KIEM_TRA_THIET_BI() { MA_BO_THIET_BI = Convert.ToInt32(lookUpEditBoThietBi.EditValue)
                                                        , TEN_BO_THIET_BI = lookUpEditBoThietBi.Text
                                                         ,  GHI_CHU = thongbao
                });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    
                }
            }       
        }
        void updatesudungtonkho()
        {

            string sql = "UPDATE `TON_KHO` SET `SL_SU_DUNG`= SL_SU_DUNG+1 WHERE `MA_THIET_BI`=@MA_THIET_BI and `MA_BO`=@MA_BO";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<TON_KHO>();
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    list.Add(new TON_KHO() { 
                        MA_BO = Convert.ToInt32(lookUpEditBoThietBi.EditValue), 
                        MA_THIET_BI = Convert.ToInt32(gridView1.GetRowCellValue(i, "MA_THIET_BI")) });
                }
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {

                }
            }
        }

        void insertlichsukiemtrahoantat()
        {

            string sql = "INSERT INTO `LICH_SU_KIEM_TRA_THIET_BI`(`MA_BO_THIET_BI`, `TEN_BO_THIET_BI`, `NGAY`, `GHI_CHU`,IS_HOAN_TAT) VALUES (@MA_BO_THIET_BI,@TEN_BO_THIET_BI,now(),@GHI_CHU,@IS_HOAN_TAT)";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<LICH_SU_KIEM_TRA_THIET_BI>();
                list.Add(new LICH_SU_KIEM_TRA_THIET_BI()
                {
                    MA_BO_THIET_BI = Convert.ToInt32(lookUpEditBoThietBi.EditValue)
                    ,
                    TEN_BO_THIET_BI = lookUpEditBoThietBi.Text
                    ,
                    GHI_CHU = thongbao
                    ,
                    IS_HOAN_TAT = 1
                });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {

                }
            }
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            using(frmLichSuKiemTraTheoBo f = new frmLichSuKiemTraTheoBo())
            {
                f.id = Convert.ToInt32(lookUpEditBoThietBi.EditValue);
                f.ShowDialog();
            }
        }
        string p = string.Empty;
        private void simpleButton6_Click_1(object sender, EventArgs e)
        {
            p = System.Configuration.ConfigurationManager.AppSettings["Port"];
            int i = Convert.ToInt32(lookUpEditBoThietBi.EditValue);
            string s = "SELECT THIET_BI.MA_BO,THIET_BI.MA_THIET_BI,TEN_THIET_BI,BO_THIET_BI.TEN_BO as 'TEN_BO',QUY_CACH,TON_KHO.SL_TON as SL_TON, THIET_BI.THIET_BI_MA FROM `THIET_BI`,BO_THIET_BI, TON_KHO WHERE THIET_BI.MA_BO = BO_THIET_BI.MA_BO and THIET_BI.SU_DUNG = 1 and TON_KHO.MA_THIET_BI = THIET_BI.MA_THIET_BI and TON_KHO.MA_BO = " + i + " and TON_KHO.MA_THIET_BI not in ("+p+")";
            //gridControl1.DataSource = connection.Query(s);
        
            try
            {                
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = s;
                cmd.Connection = cn.conn;
                DataTable dt = new DataTable();
                dt.Columns.Add("MA_BO");
                dt.Columns.Add("MA_THIET_BI");
                dt.Columns.Add("TEN_THIET_BI");
                dt.Columns.Add("TEN_BO");
                dt.Columns.Add("QUY_CACH");
                dt.Columns.Add("SL_TON");
                dt.Columns.Add("THIET_BI_MA");
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    DataRow row = dt.NewRow();
                    row["MA_BO"] = rd["MA_BO"];
                    row["MA_THIET_BI"] = rd["MA_THIET_BI"];
                    row["TEN_THIET_BI"] = rd["TEN_THIET_BI"];
                    row["TEN_BO"] = rd["TEN_BO"];
                    row["QUY_CACH"] = rd["QUY_CACH"];
                    row["SL_TON"] = rd["SL_TON"];
                    row["THIET_BI_MA"] = rd["THIET_BI_MA"];
                    dt.Rows.Add(row);
                }
                rd.Close();
                gridControl1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
                Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
                config.AppSettings.Settings["Port"].Value = "";
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
        
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveDialog = new SaveFileDialog()) // cách 2
            {
                saveDialog.Filter = "Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html";
                if (saveDialog.ShowDialog() != DialogResult.Cancel)
                {
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;

                    switch (fileExtenstion)
                    {
                        case ".xls":
                            gridControl1.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            gridControl1.ExportToXlsx(exportFilePath);
                            break;
                        case ".rtf":
                            gridControl1.ExportToRtf(exportFilePath);
                            break;
                        case ".pdf":
                            gridControl1.ExportToPdf(exportFilePath);
                            break;
                        case ".html":
                            gridControl1.ExportToHtml(exportFilePath);
                            break;
                        case ".mht":
                            gridControl1.ExportToMht(exportFilePath);
                            break;
                        default:
                            break;
                    }

                    if (File.Exists(exportFilePath))
                    {
                        try
                        {
                            //Try to open the file and let windows decide how to open it.
                            System.Diagnostics.Process.Start(exportFilePath);
                        }
                        catch
                        {
                            String msg = "The file could not be opened." + Environment.NewLine + Environment.NewLine + "Path: " + exportFilePath;
                            MessageBox.Show(msg, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        String msg = "The file could not be saved." + Environment.NewLine + Environment.NewLine + "Path: " + exportFilePath;
                        MessageBox.Show(msg, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void frmKiemTraThietBi_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode.Equals(Keys.Enter))
            //{
            //    simpleButton5_Click(sender, e);
            //    textEdit1.Focus();
            //} 
        }

        private void listBoxControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lookUpEditNhomBo_EditValueChanged(object sender, EventArgs e)
        {
            lookUpEditBoThietBi.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG and NHOM_BO_ID = @NHOM_BO_ID", new { SU_DUNG = 1, NHOM_BO_ID = lookUpEditNhomBo.EditValue }, commandType: CommandType.Text);
            lookUpEditBoThietBi.Properties.ValueMember = "MA_BO";
            lookUpEditBoThietBi.Properties.DisplayMember = "TEN_BO";
        }
    }
}
