﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
using DapperExtensions;
using System.Configuration;
using System.Collections;
using System.Windows.Documents;

namespace QLKhachHang
{
    public partial class frmThietBi : DevExpress.XtraEditors.XtraUserControl
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryTHIET_BI qr = new Query.QueryTHIET_BI();
        Query.QueryTonKho qrtk = new Query.QueryTonKho();
        public XRPictureBox picture = new XRPictureBox();
        int maid;
        int mathietbiinsert;
        int id;
        bool insert = false;
        public frmThietBi()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            luu();
            //if (insert == true)
            //{
            //    luutonkho();
            //}
        }

        void luu()
        {
            try
            {
                selectid();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.queryaddthietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.MA_BO, MySqlDbType.Int32).Value = lookUpEditBoThietBi.EditValue;
                cmd.Parameters.Add(qr.MA_NTB, MySqlDbType.Int32).Value = lookUpEditNhomThietBi.EditValue;
                //cmd.Parameters.Add(qr.NHOM_BO_ID, MySqlDbType.Int32).Value = lookUpEditNhomBo.EditValue;
                cmd.Parameters.Add(qr.TEN_THIET_BI, MySqlDbType.VarChar).Value = txtTenThietBi.Text;
                cmd.Parameters.Add(qr.QUY_CACH, MySqlDbType.VarChar).Value = txtQuyCach.Text;
                cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtGhiChu.Text;
                cmd.Parameters.Add(qr.THIET_BI_MA, MySqlDbType.VarChar).Value = txtMaThietBi.Text;
                cmd.Parameters.Add(qr.VI_TRI_KHO, MySqlDbType.VarChar).Value = txtViTriKho.Text;
                //cmd.Parameters.Add(qr.ID, MySqlDbType.Int32).Value = id;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã lưu");
                    load_thiet_bi();
                    insert = true;
                }
                else
                {
                    insert = false;
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                insert = false;
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        void luutonkho()
        {
            try
            {
                selectid();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qrtk.queryaddtonkhothietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qrtk.MA_BO, MySqlDbType.Int32).Value = lookUpEditBoThietBi.EditValue;
                cmd.Parameters.Add(qrtk.MA_THIET_BI, MySqlDbType.Int32).Value = mathietbiinsert;               
                cmd.Parameters.Add(qrtk.THIET_BI_MA, MySqlDbType.VarChar).Value = txtMaThietBi.Text;
                cmd.Parameters.Add(qrtk.SL_SU_DUNG, MySqlDbType.Int32).Value = 0;
                //cmd.Parameters.Add(qrtk.ID, MySqlDbType.VarChar).Value = id;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã lưu");
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }       
        void update()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.queryupdatethietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.MA_BO, MySqlDbType.Int32).Value = lookUpEditBoThietBi.EditValue;
                cmd.Parameters.Add(qr.MA_NTB, MySqlDbType.Int32).Value = lookUpEditNhomThietBi.EditValue;
                cmd.Parameters.Add(qr.TEN_THIET_BI, MySqlDbType.VarChar).Value = txtTenThietBi.Text;
                cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtGhiChu.Text;
                cmd.Parameters.Add(qr.QUY_CACH, MySqlDbType.VarChar).Value = txtQuyCach.Text;
                cmd.Parameters.Add(qr.MA_THIET_BI, MySqlDbType.Int32).Value = maid;
                cmd.Parameters.Add(qr.THIET_BI_MA, MySqlDbType.VarChar).Value = txtMaThietBi.Text;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã cập nhật");
                    load_thiet_bi();
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        void delete()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = qr.querydeletethietbi();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.MA_THIET_BI, MySqlDbType.Int32).Value = maid;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    XtraMessageBox.Show("Đã xóa");
                    load_thiet_bi();
                }
                else
                {
                    XtraMessageBox.Show("Lỗi");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        void selectid()
        {
            try
            {
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "select max(`MA_THIET_BI`) from THIET_BI";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    mathietbiinsert = rd.GetInt32(0);
                    id =mathietbiinsert+1;
                }

                rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        void load_thiet_bi()
        {
            try
            {
                string s = "SELECT THIET_BI.MA_BO,MA_THIET_BI,TEN_THIET_BI,BO_THIET_BI.TEN_BO as 'TEN_BO',QUY_CACH, NHOM_THIET_BI.MA_NTB,TEN_NTB,THIET_BI.THIET_BI_MA,THIET_BI.VI_TRI_KHO,NHOM_BO.NHOM_BO_ID,NHOM_BO.NHOM_BO_TEN FROM `THIET_BI`,BO_THIET_BI, NHOM_THIET_BI, NHOM_BO " +
                        " WHERE THIET_BI.MA_BO = BO_THIET_BI.MA_BO " +
                        " and THIET_BI.MA_NTB = NHOM_THIET_BI.MA_NTB" +
                        " and NHOM_BO.NHOM_BO_ID = BO_THIET_BI.NHOM_BO_ID" +
                        " and THIET_BI.SU_DUNG = 1 " +
                        " and BO_THIET_BI.SU_DUNG = 1" +
                        " and NHOM_THIET_BI.SU_DUNG = 1"+
                        " order by THIET_BI.MA_THIET_BI desc";
                gridControl1.DataSource = connection.Query(s).ToList();
            }
            catch { }
        }
        

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked == true)
            {
                gridControl1.Enabled = true;
            }
            else
            {
                gridControl1.Enabled = false;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                delete();
            }
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;
                    object ten;
                    object ghichu;
                    object mabo;
                    object quycach;
                    object manhom;
                    object nhomboid;
                    object mansx;

                    id = view.GetRowCellValue(view.FocusedRowHandle, "MA_THIET_BI");
                    mabo = view.GetRowCellValue(view.FocusedRowHandle, "MA_BO");
                    manhom = view.GetRowCellValue(view.FocusedRowHandle, "MA_NTB");
                    nhomboid = view.GetRowCellValue(view.FocusedRowHandle, "NHOM_BO_ID");
                    ten = view.GetRowCellValue(view.FocusedRowHandle, "TEN_THIET_BI").ToString();
                    quycach = view.GetRowCellValue(view.FocusedRowHandle, "QUY_CACH") == null ? "" : view.GetRowCellValue(view.FocusedRowHandle, "QUY_CACH");
                    ghichu = view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU") == null? "" :view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU");
                    mansx = view.GetRowCellValue(view.FocusedRowHandle, "THIET_BI_MA") == null ? "" : view.GetRowCellValue(view.FocusedRowHandle, "THIET_BI_MA");

                    maid = Convert.ToInt32(id);
                    lookUpEditBoThietBi.EditValue = mabo;
                    lookUpEditNhomThietBi.EditValue = manhom;
                    lookUpEditNhomBo.EditValue = nhomboid;
                    txtQuyCach.Text = (string)quycach;
                    txtTenThietBi.Text = ten.ToString();
                    txtGhiChu.Text = ghichu.ToString();
                    txtMaThietBi.Text = mansx.ToString();
                }
            }
            catch { }
        }

        public void loadnhomthietbi()
        {
            lookUpEditNhomThietBi.Properties.DataSource = connection.Query<NHOM_THIET_BI>("select * from NHOM_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhomThietBi.Properties.ValueMember = "MA_NTB";
            lookUpEditNhomThietBi.Properties.DisplayMember = "TEN_NTB";
        }
        public void loadbothietbi()
        {
            //lookUpEditBoThietBi.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            //lookUpEditBoThietBi.Properties.ValueMember = "MA_BO";
            //lookUpEditBoThietBi.Properties.DisplayMember = "TEN_BO";
        }

        public void loadnhombo()
        {
            lookUpEditNhomBo.Properties.DataSource = connection.Query<NHOM_BO>("select * from NHOM_BO where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhomBo.Properties.ValueMember = "NHOM_BO_ID";
            lookUpEditNhomBo.Properties.DisplayMember = "NHOM_BO_TEN";
        }
        private void frmThietBi_Load(object sender, EventArgs e)
        {
            loadbothietbi();
            loadnhomthietbi();
            loadnhombo();
            load_thiet_bi();
            //gridControl1.DataSource = connection.Query<THIET_BI>("", commandType: CommandType.Text).ToList();
        }
        void insertnhieuthietbi()
        {
            string sql = "INSERT INTO `THIET_BI`(`MA_BO`, MA_NTB,`TEN_THIET_BI`, `QUY_CACH`, `NGAY`, `GHI_CHU`, `SU_DUNG`, VI_TRI_KHO,THIET_BI_MA) VALUES (@MA_BO,@MA_NTB,@TEN_THIET_BI,@QUY_CACH,now(),@GHI_CHU,1,@VI_TRI_KHO,@THIET_BI_MA)";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                ArrayList rows = new ArrayList();
                var list = new List<THIET_BI>();

                int[] selectedRows = gridView1.GetSelectedRows();  
                foreach (int rowHandle in selectedRows)  
                {
                    if (rowHandle >= 0)
                    {
                        object quy_cach = gridView1.GetRowCellValue(rowHandle, "QUY_CACH") == null ? "" : gridView1.GetRowCellValue(rowHandle, "QUY_CACH");
                        object ghi_chu = gridView1.GetRowCellValue(rowHandle, "GHI_CHU") == null ? "" : gridView1.GetRowCellValue(rowHandle, "GHI_CHU");

                        list.Add(new THIET_BI() {MA_BO = Convert.ToInt32(lookUpEditBoThietBi.EditValue)
                                                , MA_NTB = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "MA_NTB"))
                                                
                                                , TEN_THIET_BI = gridView1.GetRowCellValue(rowHandle, "TEN_THIET_BI").ToString()
                                                , QUY_CACH = quy_cach.ToString()
                                                , GHI_CHU = ghi_chu.ToString()
                                                , VI_TRI_KHO = gridView1.GetRowCellValue(rowHandle, "VI_TRI_KHO").ToString()
                                                , THIET_BI_MA = gridView1.GetRowCellValue(rowHandle, "THIET_BI_MA").ToString()
                                                , SU_DUNG = 1});
                        
                    }
                }
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã lưu");
                }
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            insertnhieuthietbi();
            //getlistidmoitao();
            //updatetonkhonhieuthietbi();
            load_thiet_bi();
        }
        ArrayList g = new ArrayList();
        void getlistidmoitao()
        {
            try
            {
                
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "SELECT MA_THIET_BI FROM `THIET_BI` WHERE `MA_BO` = "+lookUpEditBoThietBi.EditValue+" and NGAY in (SELECT max(NGAY) FROM THIET_BI)";
                string sql = "SELECT MA_THIET_BI FROM `THIET_BI` WHERE `MA_BO` = "+lookUpEditBoThietBi.EditValue+"";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    int r = rd.GetInt32(0);
                    g.Add(r);
                }

                rd.Close();

            }
            catch
            {

            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
            

            //ArrayList b = new ArrayList();
            //var a = connection.Query<THIET_BI>("select MA_THIET_BI from THIET_BI where MA_BO = @MA_BO", new { MA_BO = 2 }, commandType: CommandType.Text);
            //b.Add(a);
            //object c = b;
        }
        private void updatetonkhonhieuthietbi()
        {
            int k = 0;
            string sql = "INSERT INTO `TON_KHO`(`MA_THIET_BI`, `MA_BO`, `SL_TON`,THIET_BI_MA) VALUES (@MA_THIET_BI,@MA_BO,@SL_TON,@THIET_BI_MA)";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                ArrayList rows = new ArrayList();
                var list = new List<TON_KHO>();

                int[] selectedRows = gridView1.GetSelectedRows();
                foreach (int rowHandle in selectedRows)
                {
                    if (rowHandle >= 0)
                    {
                        for (int i = k; i <= g.Count;i++ )
                        {
                            list.Add(new TON_KHO()
                            {
                                MA_BO = Convert.ToInt32(lookUpEditBoThietBi.EditValue)
                            ,
                                MA_THIET_BI = (int)g[i]
                            ,
                                SL_TON = 1
                            ,
                                THIET_BI_MA = gridView1.GetRowCellValue(rowHandle, "THIET_BI_MA").ToString()
                            });
                            k++;
                            break;
                        }
                            
                    }
                }
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    k = 0;
                    g.Clear();
                }
            }
        }

        private void lookUpEditBoThietBi_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                frmBoThietBiAdd f = new frmBoThietBiAdd(this);
                f.ShowDialog();
            }
        }

        private void lookUpEditNhomThietBi_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                frmNhomThietBiAdd f = new frmNhomThietBiAdd(this);
                f.ShowDialog();
            }
        }

        private void lookUpEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                frmNhomBoAdd f = new frmNhomBoAdd(this);
                f.ShowDialog();
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            update();
        }

        private void lookUpEditNhomBo_EditValueChanged(object sender, EventArgs e)
        {
            lookUpEditBoThietBi.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG and NHOM_BO_ID = @NHOM_BO_ID", new { SU_DUNG = 1, NHOM_BO_ID = lookUpEditNhomBo.EditValue }, commandType: CommandType.Text);
            lookUpEditBoThietBi.Properties.ValueMember = "MA_BO";
            lookUpEditBoThietBi.Properties.DisplayMember = "TEN_BO";
        }
    }
}