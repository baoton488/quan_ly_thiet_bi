﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Windows.Threading;
using QLKhachHang;
using MySql.Data.MySqlClient;

namespace QLTV.QLTV.DanhMuc
{
    public partial class frmStatus : DevExpress.XtraEditors.XtraForm
    {
        Connect cn = new Connect();
        public frmStatus()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(5000);
            timer.Tick += Tick;
            timer.Start();
        }

        private void XtraForm1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qltb_demo7DataSet.TRANGTHAIBTB' table. You can move, or remove it, as needed.
            //this.tRANGTHAIBTBTableAdapter.Fill(this.qltb_demo7DataSet1.TRANGTHAIBTB);
            gridControl1.DataSource = getDataTable();
        }
        private void Tick(object sender, EventArgs e)
        {
            //this.tRANGTHAIBTBTableAdapter.Fill(this.qltb_demo7DataSet1.TRANGTHAIBTB);
            gridControl1.DataSource = getDataTable();
        }

        public DataTable getDataTable()
        {
            cn.openconnection();
            MySqlCommand cmd = new MySqlCommand("SELECT `Tên bộ`, `Tông số bộ`, `Có thể sử dụng`, `Đang mổ`, `Chờ rửa`, `Chờ hấp`, Hư, `Đang sữa chữa` FROM TRANGTHAIBTB", cn.conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                da.Fill(dt);

            }
            catch { }
            return dt;
        }
    }
}