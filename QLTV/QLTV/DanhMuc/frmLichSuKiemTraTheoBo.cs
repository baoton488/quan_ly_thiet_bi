﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Configuration;
using Dapper;
using QLKhachHang.Query;
using DevExpress.XtraGrid.Views.Grid;
using QLKhachHang.QLTV.GiaoDichVang;

namespace QLTV.QLTV.DanhMuc
{
    public partial class frmLichSuKiemTraTheoBo : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        public int id;
        public frmLichSuKiemTraTheoBo()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        private void frmLichSuKiemTraTheoBo_Load(object sender, EventArgs e)
        {
            lookUpEdit1.Properties.DataSource = connection.Query<BO_THIET_BI>("select * from BO_THIET_BI where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEdit1.Properties.ValueMember = "MA_BO";
            lookUpEdit1.Properties.DisplayMember = "TEN_BO";
            lookUpEdit1.EditValue = id;
            gridControl1.DataSource = connection.Query<LICH_SU_KIEM_TRA_THIET_BI>("select * from LICH_SU_KIEM_TRA_THIET_BI where MA_BO_THIET_BI = @MA_BO_THIET_BI", new { MA_BO_THIET_BI = id }, commandType: CommandType.Text);
        }
        void load_thiet_bi(int id)
        {
            //string s = "SELECT * FROM `LICH_SU_KIEM_TRA_THIET_BI` WHERE MA_BO_THIET_BI = " + id + "";
            var a = connection.Query<LICH_SU_KIEM_TRA_THIET_BI>("select * from LICH_SU_KIEM_TRA_THIET_BI where MA_BO_THIET_BI = @MA_BO_THIET_BI", new { MA_BO_THIET_BI = 1 }, commandType: CommandType.Text);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            gridControl1.DataSource = connection.Query<LICH_SU_KIEM_TRA_THIET_BI>("select * from LICH_SU_KIEM_TRA_THIET_BI where MA_BO_THIET_BI = @MA_BO_THIET_BI", new { MA_BO_THIET_BI = 1 }, commandType: CommandType.Text);
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            
        }
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;
                    object ghichu;

                    id = view.GetRowCellValue(view.FocusedRowHandle, "ID");
                    ghichu = view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU");

                    frmKiemTraThietBi f = new frmKiemTraThietBi();
                    f.id_lichsukiemtra = Convert.ToInt32(id);
                    f.ghichu_lichsukiemtra = ghichu.ToString();
                    f.xulykhicokiemtra();
                    
                    this.Close();
                    //f.test();
                }
            }
            catch { }
        }
    }
}