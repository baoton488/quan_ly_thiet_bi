﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
using System.Configuration;

namespace QLKhachHang
{
    public partial class frmPhongAdd : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryBO_THIET_BI qr = new Query.QueryBO_THIET_BI();
        public XRPictureBox picture = new XRPictureBox();
        int maid;
        private readonly frmNKTB_Xuat frm1;
        public frmPhongAdd()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        public frmPhongAdd(frmNKTB_Xuat frm)
        {
            InitializeComponent();
            frm1 = frm;
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            luu();
        }

        void luu()
        {
            string sql = "INSERT INTO `PHONG`(`TEN_PHONG`, `GHI_CHU`, `SU_DUNG`) VALUES (@TEN_PHONG,@GHI_CHU,@SU_DUNG)";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<PHONG>();

                list.Add(new PHONG()
                        {
                            TEN_PHONG = txtTenPhong.Text
                        ,
                            GHI_CHU = txtGhiChu.Text
                            ,
                            SU_DUNG = true
                        });
                
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã lưu");
                    load();
                    frm1.loadPhong();
                }
            }
        }

        void update()
        {
            string sql = "UPDATE `PHONG` SET `TEN_PHONG`=@TEN_PHONG,`GHI_CHU`=@GHI_CHU WHERE `ID`=@ID";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<PHONG>();

                list.Add(new PHONG()
                {
                    ID = maid
                    ,
                    TEN_PHONG = txtTenPhong.Text
                    ,
                    GHI_CHU = txtGhiChu.Text
                    ,                 
                });

                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã lưu");
                    load();
                    frm1.loadPhong();
                }
            }
        }

        void delete()
        {
            string sql = "UPDATE `PHONG` SET `SU_DUNG`= 0 WHERE `ID`=@ID";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<PHONG>();

                list.Add(new PHONG()
                {
                    ID = maid
                });

                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã lưu");
                    load();
                    frm1.loadPhong();
                }
            }
        }

        void load()
        {
            gridControl1.DataSource = connection.Query<PHONG>("select * from PHONG where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
        }
        private void frmBoThietBiAdd_Load(object sender, EventArgs e)
        {
            load();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            //if (checkEdit1.Checked == true)
            //{
            //    gridControl1.Enabled = true;
            //}
            //else
            //{
            //    gridControl1.Enabled = false;
            //}
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                delete();
            }
            
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;
                    object ten;
                    object ghichu;


                    id = view.GetRowCellValue(view.FocusedRowHandle, "ID");
                    ten = view.GetRowCellValue(view.FocusedRowHandle, "TEN_PHONG");
                    ghichu = view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU");

                    maid = Convert.ToInt32(id);
                    txtTenPhong.Text = ten.ToString();
                    txtGhiChu.Text = ghichu.ToString();
                }
            }
            catch { }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            update();
        }
    }
}