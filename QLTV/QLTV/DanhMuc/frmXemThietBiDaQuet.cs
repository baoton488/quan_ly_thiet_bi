﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows.Threading;
using QLKhachHang.Query;
using Dapper;
using QLKhachHang;
using Quobject.SocketIoClientDotNet.Client;

namespace QLTB.QLTV.DanhMuc
{
    public delegate void UpdateTextBoxMethod(string text);
    public partial class frmXemThietBiDaQuet : DevExpress.XtraEditors.XtraForm
    {
        DispatcherTimer timer = new DispatcherTimer();
        Connect cn = new Connect();
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        public frmXemThietBiDaQuet()
        {
            InitializeComponent();

            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += Tick;
            timer.Start();
        }

        private void frmXemThietBiDaQuet_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            lookUpEdit1.Properties.DataSource = connection.Query<NHAT_KY_SU_DUNG>("select * from NHAT_KY_SU_DUNG where IS_SU_DUNG = @IS_SU_DUNG order by ID_NK_SD desc", new { IS_SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEdit1.Properties.ValueMember = "ID_NK_SD";
            lookUpEdit1.Properties.DisplayMember = "NK_SD_MA";

            socketIoManager();
        }
        private void Tick(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string connectionString = ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString;
            string query = "SELECT BO_THIET_BI.MA_BO, BO_THIET_BI.TEN_BO, THIET_BI.MA_THIET_BI, THIET_BI.TEN_THIET_BI, NHAT_KY_SU_DUNG_THIET_BI.TRANG_THAI_THIET_BI,NHAT_KY_SU_DUNG_THIET_BI.ID_NK_SD FROM THIET_BI left JOIN NHAT_KY_SU_DUNG_THIET_BI on NHAT_KY_SU_DUNG_THIET_BI.MA_THIET_BI = THIET_BI.MA_THIET_BI AND NHAT_KY_SU_DUNG_THIET_BI.ID_NK_SD= @NK_SD LEFT JOIN BO_THIET_BI on BO_THIET_BI.MA_BO = THIET_BI.MA_BO WHERE THIET_BI.MA_BO in (SELECT NHAT_KY_SU_DUNG_CHI_TIET.MA_BO from NHAT_KY_SU_DUNG_CHI_TIET WHERE NHAT_KY_SU_DUNG_CHI_TIET.ID_NK_SD = @NK_SD)";
            //DataSet ds = new DataSet();
            using (MySqlCommand cmd = new MySqlCommand(query, new MySqlConnection(connectionString)))
            {
                cmd.Parameters.Add("@NK_SD", MySqlDbType.Int32).Value = lookUpEdit1.EditValue;
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(cmd))
                {
                    adapter.Fill(dt);
                    gridControl1.DataSource = dt;
                }
            }
        }

        private void lookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridControl2.DataSource = connection.Query<NHAT_KY_SU_DUNG_CHI_TIET>("select * from NHAT_KY_SU_DUNG_CHI_TIET where ID_NK_SD = @ID_NK_SD", new { ID_NK_SD = lookUpEdit1.EditValue }, commandType: CommandType.Text);
            DataTable dt = new DataTable();
            string connectionString = ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString;
            string query = "SELECT BO_THIET_BI.MA_BO, BO_THIET_BI.TEN_BO, THIET_BI.MA_THIET_BI, THIET_BI.TEN_THIET_BI, NHAT_KY_SU_DUNG_THIET_BI.TRANG_THAI_THIET_BI,NHAT_KY_SU_DUNG_THIET_BI.ID_NK_SD FROM THIET_BI left JOIN NHAT_KY_SU_DUNG_THIET_BI on NHAT_KY_SU_DUNG_THIET_BI.MA_THIET_BI = THIET_BI.MA_THIET_BI AND NHAT_KY_SU_DUNG_THIET_BI.ID_NK_SD= @NK_SD LEFT JOIN BO_THIET_BI on BO_THIET_BI.MA_BO = THIET_BI.MA_BO WHERE THIET_BI.MA_BO in (SELECT NHAT_KY_SU_DUNG_CHI_TIET.MA_BO from NHAT_KY_SU_DUNG_CHI_TIET WHERE NHAT_KY_SU_DUNG_CHI_TIET.ID_NK_SD = @NK_SD)";
            //DataSet ds = new DataSet();
            using (MySqlCommand cmd = new MySqlCommand(query, new MySqlConnection(connectionString)))
            {
                cmd.Parameters.Add("@NK_SD", MySqlDbType.Int32).Value = lookUpEdit1.EditValue;
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(cmd))
                {
                    adapter.Fill(dt);
                    gridControl1.DataSource = dt;
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            timer.Stop();
        }

        void insert()
        {
            try
            {
                string sql = "INSERT INTO `NHAT_KY_SU_DUNG_THIET_BI`(`MA_THIET_BI`, `ID_NK_SD`, `USER_ID`, `KIEU_QUET`, `NGAY_TREN_THIET_BI`,TEN_THIET_BI,TRANG_THAI_THIET_BI) VALUES " +
                            " (@MA_THIET_BI,@ID_NK_SD,@USER_ID,@KIEU_QUET,@NGAY_TREN_THIET_BI,@TEN_THIET_BI,@TRANG_THAI_THIET_BI) ";
                getten();
                using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
                {
                    connection.Open();
                    var list = new List<NHAT_KY_SU_DUNG_THIET_BI>();
                    list.Add(new NHAT_KY_SU_DUNG_THIET_BI()
                            {
                                MA_THIET_BI = Convert.ToInt32(textEdit1.Text)
                                ,
                                ID_NK_SD = Convert.ToInt32(lookUpEdit1.EditValue)
                                ,
                                USER_ID = QueryPQ_User.intance().user_id
                                ,
                                KIEU_QUET = 1
                                ,
                                NGAY_TREN_THIET_BI = DateTime.Now
                                ,
                                TEN_THIET_BI = ten
                                ,
                                TRANG_THAI_THIET_BI = 1
                            });
                    var affectedRows = connection.Execute(sql,
                        list
                    );
                    if (affectedRows > 0)
                    {
                        ten = "";
                    }
                    else
                    {
                        ten = "";
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

       
        string ten;
        bool cktontai;
        void getten()
        {
            try
            {
                
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "select TEN_THIET_BI from THIET_BI where MA_THIET_BI = "+Convert.ToInt32(textEdit1.Text)+"";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    ten = rd.GetString(0);
                }
                rd.Close();
            }
            catch
            {
                ten = "";
                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void frmXemThietBiDaQuet_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                checktontai();
                if (cktontai == true)
                {
                    XtraMessageBox.Show("Lỗi thiết bị đã quét rồi!");
                    return;
                }
                else
                    insert();
                    textEdit1.Text = "";
            }    
        }

        void checktontai()
        {
            try
            {

                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                string sql = "select MA_THIET_BI, ID_NK_SD from NHAT_KY_SU_DUNG_THIET_BI where MA_THIET_BI = " + Convert.ToInt32(textEdit1.Text) + " and ID_NK_SD = "+Convert.ToInt32(lookUpEdit1.EditValue)+"";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    cktontai = true;
                }
                else
                {
                    cktontai = false;
                }
                rd.Close();
            }
            catch
            {
                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void gridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            object quantity;
            int a;
            if (!Convert.IsDBNull(gridView1.GetRowCellValue(e.RowHandle, "TRANG_THAI_THIET_BI")))
            {
                quantity = Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "TRANG_THAI_THIET_BI"));
                a = (int)quantity;
                if (a == 1)
                {
                    e.Appearance.BackColor = Color.LightGreen;
                }
                else
                {
                    e.Appearance.BackColor = Color.White;
                }
            } 
            

            //Override any other formatting  
            e.HighPriority = true;  
        }
        private void socketIoManager()
        {
            //Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            //config.AppSettings.Settings["IO"].Value = "1";
            //config.Save(ConfigurationSaveMode.Modified);
            //ConfigurationManager.RefreshSection("appSettings");
            // Instantiate the socket.io connection
            //var socket = io.connect('https://localhost', {secure: true});
            try
            {
                var options = new IO.Options();
                //options.Secure = true;
                options.IgnoreServerCertificateValidation = true;
                string sk = "wss://isuzuml.ddns.net:";
                sk = string.Concat(sk, "5000");
                var socket = IO.Socket(sk, options);

                // Upon a connection event, update our status
                socket.On(Socket.EVENT_DISCONNECT, () =>
                {
                    UpdateStatus("Disconnected");
                });
                socket.On(Socket.EVENT_CONNECT, () =>
                {
                    UpdateStatus("Connected");
                });
                socket.On("log_success", (data) =>
                {
                    var temperature = new { temperature = "" };
                    //var tempValue = JsonConvert.DeserializeAnonymousType((string)data, temperature);
                    var tempValue = (string)data;
                    UpdateTemp((string)tempValue);
                });
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
        private void UpdateTemp(string text)
        {
            if (this.textEdit1.InvokeRequired)
            {
                UpdateTextBoxMethod del = new UpdateTextBoxMethod(UpdateTemp);
                this.Invoke(del, new object[] { text });
                
            }
            else
            {
                this.textEdit1.Text = text;
            }
        }
        private void UpdateStatus(string text)
        {
            //if (this.textEdit2.InvokeRequired)
            //{
            //    UpdateTextBoxMethod del = new UpdateTextBoxMethod(UpdateStatus);
            //    this.Invoke(del, new object[] { "Connected" });
            //}
            //else
            //{
            //    this.textEdit2.Text = text;
            //}
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            RunTransaction(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        }
        public void RunTransaction(string myConnString)
        {
            MySqlConnection myConnection = new MySqlConnection(myConnString);
            myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
            MySqlTransaction myTrans;

            // Start a local transaction
            myTrans = myConnection.BeginTransaction();
            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            try
            {
                trathietbi();
                UPDATESTATUSBOTHIETBI();
                UPDATESTATUSTHIETBI();
                //UPDATESL_SDBOTHIETBI();
                //UPDATESL_SDTHIETBI();
                myTrans.Commit();
                gridControl1.DataSource = null;
                XtraMessageBox.Show("Đã trả thiết bị");
                //loadthietbi();
                gridControl1.DataSource = null;
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (MySqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");
                    }
                }

                Console.WriteLine("An exception of type " + e.GetType() +
                " was encountered while inserting the data.");
                Console.WriteLine("Neither record was written to database.");
            }
            finally
            {
                myConnection.Close();
            }
        }
        void trathietbi()
        {
            string sql = "UPDATE `NHAT_KY_SU_DUNG` SET IS_SU_DUNG= 0, NGAY_TRA = now() WHERE ID_NK_SD = @ID_NK_SD";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAT_KY_SU_DUNG>();
                list.Add(new NHAT_KY_SU_DUNG()
                {
                    ID_NK_SD = Convert.ToInt32(lookUpEdit1.EditValue)
                });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {

                }
            }
        }
        void UPDATESTATUSBOTHIETBI()
        {
            StringBuilder sCommand = new StringBuilder("UPDATE `BO_THIET_BI` SET `MUC_DO_SU_DUNG`= 0 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                for (int i = 0; i < gridView2.RowCount; i++)
                {
                    dsTam.Add(Convert.ToInt32(gridView2.GetRowCellValue(i, "MA_BO")));

                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if (a > 0)
                    {

                    }
                }

            }
        }

        void UPDATESTATUSTHIETBI()
        {
            StringBuilder sCommand = new StringBuilder("UPDATE `THIET_BI` SET `IS_SU_DUNG`= 0 WHERE MA_BO IN");
            List<long> dsTam = new List<long>();
            using (MySqlConnection mConnection = cn.conn)
            {
                //List<int> Rows = new List<int>();
                for (int i = 0; i < gridView2.RowCount; i++)
                {
                    dsTam.Add(Convert.ToInt32(gridView2.GetRowCellValue(i, "MA_BO")));
                }
                sCommand.Append("(");
                sCommand.Append(string.Join(",", dsTam));
                sCommand.Append(")");
                sCommand.Append(";");
                cn.openconnection();
                using (MySqlCommand myCmd = new MySqlCommand(sCommand.ToString(), mConnection))
                {
                    myCmd.CommandType = CommandType.Text;
                    //myCmd.ExecuteNonQuery();
                    int a = myCmd.ExecuteNonQuery();
                    if (a > 0)
                    {
                        //ckupdatebtb = true;
                    }
                }
            }
        }
    }
}