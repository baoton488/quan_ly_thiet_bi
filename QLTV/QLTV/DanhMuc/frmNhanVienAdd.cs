﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using QLKhachHang.Query;
using Dapper;
using System.Configuration;

namespace QLKhachHang
{
    public partial class frmNhanVienAdd : DevExpress.XtraEditors.XtraUserControl
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryBO_THIET_BI qr = new Query.QueryBO_THIET_BI();
        public XRPictureBox picture = new XRPictureBox();
        int maid;
        private readonly frmNKTB_Xuat frm1;
        public frmNhanVienAdd()
        {
            InitializeComponent();
            DapperExtensions.DapperExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();

            //Asynchronous
            DapperExtensions.DapperAsyncExtensions.SqlDialect = new DapperExtensions.Sql.MySqlDialect();
        }

        public frmNhanVienAdd(frmNKTB_Xuat frm)
        {
            InitializeComponent();
            frm1 = frm;
        }

        

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                luu();
            }
            catch { }
        }

        void luu()
        {
            string sql = "INSERT INTO `NHAN_VIEN`(`TEN_NHAN_VIEN`, `GIOI_TINH`, `NGAY_SINH`, `GHI_CHU`, `SU_DUNG`) VALUES (@TEN_NHAN_VIEN,@GIOI_TINH,@NGAY_SINH,@GHI_CHU,@SU_DUNG)";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAN_VIEN>();

                list.Add(new NHAN_VIEN()
                {
                    TEN_NHAN_VIEN = txtTenNhanVien.Text
                    ,
                    GIOI_TINH = (ckGioiTinh.Checked == true) ? true : false
                    ,
                    NGAY_SINH = Convert.ToDateTime(dtNgaySinh.EditValue)
                    ,
                    GHI_CHU = txtGhiChu.Text
                    ,
                    SU_DUNG = 1
                });

                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã lưu");
                    load();
                }
            }
        }

        void update()
        {
            string sql = "UPDATE `NHAN_VIEN` SET `TEN_NHAN_VIEN`=@TEN_NHAN_VIEN,`GIOI_TINH`=@GIOI_TINH,`NGAY_SINH`=@NGAY_SINH,`GHI_CHU`=@GHI_CHU WHERE `MA_NHAN_VIEN`= @MA_NHAN_VIEN";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAN_VIEN>();

                list.Add(new NHAN_VIEN()
                {
                    MA_NHAN_VIEN = maid
                    ,
                    TEN_NHAN_VIEN = txtTenNhanVien.Text
                    ,
                    GIOI_TINH = (ckGioiTinh.Checked == true) ? true : false
                    ,
                    NGAY_SINH = Convert.ToDateTime(dtNgaySinh.EditValue)
                    ,
                    GHI_CHU = txtGhiChu.Text
                    ,
                    SU_DUNG = 1
                });

                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã lưu");
                    load();
                }
            }
        }

        void delete()
        {
            string sql = "UPDATE `NHAN_VIEN` SET `SU_DUNG`=0 WHERE `MA_NHAN_VIEN`= @MA_NHAN_VIEN";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<NHAN_VIEN>();

                list.Add(new NHAN_VIEN()
                {
                    MA_NHAN_VIEN = maid
                });

                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {
                    XtraMessageBox.Show("Đã xóa");
                    load();
                }
            }
        }

        private void frmBoThietBiAdd_Load(object sender, EventArgs e)
        {
            load();
        }

        void load()
        {
            gridControl1.DataSource = connection.Query<NHAN_VIEN>("select * from NHAN_VIEN where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
        }
        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
            {
                delete();
            }
            
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == -1)
                {
                    return;
                }
                else
                {
                    object id;
                    object ten;
                    object gioitinh,ngaysinh,ghichu;


                    id = view.GetRowCellValue(view.FocusedRowHandle, "MA_NHAN_VIEN");
                    ten = view.GetRowCellValue(view.FocusedRowHandle, "TEN_NHAN_VIEN");
                    gioitinh = view.GetRowCellValue(view.FocusedRowHandle, "GIOI_TINH");
                    ngaysinh = view.GetRowCellValue(view.FocusedRowHandle, "NGAY_SINH");
                    ghichu = view.GetRowCellValue(view.FocusedRowHandle, "GHI_CHU");
                    maid = Convert.ToInt32(id);
                    txtTenNhanVien.Text = ten.ToString();
                    dtNgaySinh.EditValue = ngaysinh;
                    txtGhiChu.Text = ghichu.ToString();
                    if ((int)gioitinh == 1)
                        ckGioiTinh.Checked = true;
                    else
                        ckGioiTinh.Checked = false;

                    
                }
            }
            catch { }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            try
            {
                update();
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
    }
}