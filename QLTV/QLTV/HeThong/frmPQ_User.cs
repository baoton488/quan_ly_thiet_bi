﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using DevExpress.XtraGrid.Views.Grid;
using System.Security.Cryptography;

namespace QLKhachHang
{
    public partial class frmPQ_User : DevExpress.XtraEditors.XtraUserControl
    {
        Connect cn = new Connect();
        Query.QueryPQ_User qr = new Query.QueryPQ_User();
        string hash = "B@oKho@";
        int kid;
        string kt;
        int sd;
        int vt = -1;
        int userid1;
        int groupid1;
        string userma1;
        string userten1;
        string matkhau1;
        bool bikhoa1;
        string lydokhoa1;
        bool sudung1;
        DateTime ngaytao1;
        public frmPQ_User()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frmPQ_UserAdd f = new frmPQ_UserAdd(this);
            f.ShowDialog();
        }

        void loadDefault()
        {
            Default.DefaultButton.SetDefaultButton(btnThem,btnSua,btnXoa, layoutControlItem2);
            Default.DefaultGridView.SetDefaultGridView(gvPQ_User);
        }
        void loadLoaiVang()
        {
            try
            {                
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = qr.queryloadpq_user();
                cmd.Connection = cn.conn;
                DataTable dt = new DataTable();
                dt.Columns.Add("USER_ID");
                dt.Columns.Add("USER_MA");
                dt.Columns.Add("USER_TEN");
                dt.Columns.Add("MAT_KHAU");
                dt.Columns.Add("BIKHOA", typeof(bool));
                dt.Columns.Add("LY_DO_KHOA");
                dt.Columns.Add("NGAY_TAO");
                dt.Columns.Add("GROUP_ID");
               // dt.Columns.Add("NV_ID");
                dt.Columns.Add("SU_DUNG");
                
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    DataRow row = dt.NewRow();
                    row["USER_ID"] = rd["USER_ID"];
                    row["USER_MA"] = rd["USER_MA"];
                    row["USER_TEN"] = rd["USER_TEN"];
                    row["MAT_KHAU"] = rd["MAT_KHAU"];
                    row["BIKHOA"] = rd["BIKHOA"];
                    row["LY_DO_KHOA"] = rd["LY_DO_KHOA"];
                    row["NGAY_TAO"] = rd["NGAY_TAO"];
                    row["GROUP_ID"] = rd["GROUP_ID"];
                //    row["NV_ID"] = rd["NV_ID"];
                    row["SU_DUNG"] = rd["SU_DUNG"];
                    dt.Rows.Add(row);
                }
                rd.Close();
                gcPQ_User.DataSource = dt;
               // treeList1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            frmPQ_UserSua f = new frmPQ_UserSua(this, userid1, groupid1, userma1, userten1, matkhau1, bikhoa1, lydokhoa1, ngaytao1, sudung1);
            f.ShowDialog();
        }

        private void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            //try
            //{
            //    object nhid1;
            //    object dgv1;
            //    object dgm1;
            //    object dgb1;
            //    object dgc1;
            //    object nt1;
            //    nhid1 = e.Node.GetValue(treeList1.Columns["NHOMHANGID"]);
            //    nt1 = e.Node.GetValue(treeList1.Columns["NHOM_TEN"]);
            //    dgv1 = e.Node.GetValue(treeList1.Columns["DON_GIA_VON"]);
            //    dgm1 = e.Node.GetValue(treeList1.Columns["DON_GIA_MUA"]);
            //    dgb1 = e.Node.GetValue(treeList1.Columns["DON_GIA_BAN"]);
            //    dgc1 = e.Node.GetValue(treeList1.Columns["DON_GIA_CAM"]);
            //    nhid = Int32.Parse(nhid1.ToString());
            //    nt = nt1.ToString();
            //    dgv = Decimal.Parse(dgv1.ToString());
            //    dgm = Decimal.Parse(dgm1.ToString());
            //    dgb = Decimal.Parse(dgb1.ToString());
            //    dgc = Decimal.Parse(dgc1.ToString());
            //}
            //catch { }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
                {
                    cn.openconnection();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandType = CommandType.Text;
                    string sql = qr.querydeletepq_user();

                    cmd.CommandText = sql;
                    cmd.Connection = cn.conn;
                    cmd.Parameters.Add(qr.USER_ID, MySqlDbType.Int32).Value = userid1;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        frmKho_Load(sender, e);
                        XtraMessageBox.Show("Đã xóa");              
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại");
                    }
                }
            }
            catch { }
        }
        bool c = false;

        public void frmKho_Load(object sender, EventArgs e)
        {
            loadLoaiVang();
            loadDefault();
        }

        private void gvPQUser_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == vt)
                {
                    return;
                }
                else
                {
                    c = true;

                    object userid;
                    object groupid;
                    object userma;
                    object userten;
                    object matkhau;
                    object bikhoa;
                    object lydokhoa;
                    object sudung;
                    object ngaytao;
                    userid = view.GetRowCellValue(view.FocusedRowHandle, "USER_ID");
                    groupid = view.GetRowCellValue(view.FocusedRowHandle, "GROUP_ID");
                    userma = view.GetRowCellValue(view.FocusedRowHandle, "USER_MA");
                    userten = view.GetRowCellValue(view.FocusedRowHandle, "USER_TEN");
                    matkhau = view.GetRowCellValue(view.FocusedRowHandle, "MAT_KHAU");
                    bikhoa = view.GetRowCellValue(view.FocusedRowHandle, "BIKHOA");
                    lydokhoa = view.GetRowCellValue(view.FocusedRowHandle, "LY_DO_KHOA");
                    sudung = view.GetRowCellValue(view.FocusedRowHandle, "SU_DUNG");
                    ngaytao = view.GetRowCellValue(view.FocusedRowHandle, "NGAY_TAO");

                    userid1 = Int32.Parse(userid.ToString());
                    groupid1 = Int32.Parse(groupid.ToString());
                    userma1 = userma.ToString();
                    userten1 = userten.ToString();
                    matkhau1 = matkhau.ToString();
                    bikhoa1 = Boolean.Parse(bikhoa.ToString());
                    lydokhoa1 = lydokhoa.ToString();
                    sudung1 = Boolean.Parse(sudung.ToString());
                    ngaytao1 = DateTime.Parse(ngaytao.ToString());
                    Decrypt();
                }
            }
            catch { }
        }
        void Decrypt()
        {
            try
            {
                byte[] data = Convert.FromBase64String(matkhau1);
                using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
                {
                    byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                    using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = key, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                    {
                        ICryptoTransform tranform = tripDes.CreateDecryptor();
                        byte[] result = tranform.TransformFinalBlock(data, 0, data.Length);
                        matkhau1 = UTF8Encoding.UTF8.GetString(result);
                    }
                }
            }
            catch
            {
                //flag = false;
            }
        }

        void Encrypt()
        {
            //byte[] data = UTF8Encoding.UTF8.GetBytes(textEditPass.Text);
            //using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            //{
            //    byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
            //    using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = key, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
            //    {
            //        ICryptoTransform tranform = tripDes.CreateEncryptor();
            //        byte[] result = tranform.TransformFinalBlock(data, 0, data.Length);
            //        encryptpasss = Convert.ToBase64String(result, 0, result.Length);
            //    }
            //}
        }
    }
}
