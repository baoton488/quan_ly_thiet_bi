﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using DevExpress.XtraGrid.Views.Grid;
using QLTB.QLTV.HeThong;

namespace QLKhachHang
{
    public partial class frmPQ_GROUP : DevExpress.XtraEditors.XtraUserControl
    {
        Connect cn = new Connect();
        Query.QueryPQ_GROUP qr = new Query.QueryPQ_GROUP();
        //int kid;
        //string kt;
        //int sd;
        int vt = -1;
        int groupid1;
        string groupma1;
        string groupten1;
        bool bikhoa1;
        string lydokhoa1;
        bool sudung1;
        DateTime ngaytao1;
        public frmPQ_GROUP()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frmPQ_GROUPAdd f = new frmPQ_GROUPAdd(this);
            f.ShowDialog();
        }

        void loadDefault()
        {
            Default.DefaultButton.SetDefaultButton(btnThem,btnSua,btnXoa, layoutControlItem2);
            Default.DefaultGridView.SetDefaultGridView(gvPQ_GROUP);
        }
        void loadLoaiVang()
        {
            try
            {                
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = qr.queryloadpq_group();
                cmd.Connection = cn.conn;
                DataTable dt = new DataTable();
                dt.Columns.Add("GROUP_ID");
                dt.Columns.Add("GROUP_MA");
                dt.Columns.Add("GROUP_TEN");
                dt.Columns.Add("BIKHOA", typeof(bool));
                dt.Columns.Add("LY_DO_KHOA");
                dt.Columns.Add("SU_DUNG");
                dt.Columns.Add("NGAY_TAO");
                MySqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    DataRow row = dt.NewRow();
                    row["GROUP_ID"] = rd["GROUP_ID"];
                    row["GROUP_MA"] = rd["GROUP_MA"];
                    row["GROUP_TEN"] = rd["GROUP_TEN"];
                    row["BIKHOA"] = rd["BIKHOA"];
                    row["LY_DO_KHOA"] = rd["LY_DO_KHOA"];
                    row["SU_DUNG"] = rd["SU_DUNG"];
                    row["NGAY_TAO"] = rd["NGAY_TAO"];
                    dt.Rows.Add(row);
                }
                rd.Close();
                gcPQ_GROUP.DataSource = dt;
               // treeList1.DataSource = dt;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            frmPQ_GROUPSua f = new frmPQ_GROUPSua(this, groupid1,groupma1,groupten1, bikhoa1, lydokhoa1,sudung1,ngaytao1);
            f.ShowDialog();
        }

        private void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            //try
            //{
            //    object nhid1;
            //    object dgv1;
            //    object dgm1;
            //    object dgb1;
            //    object dgc1;
            //    object nt1;
            //    nhid1 = e.Node.GetValue(treeList1.Columns["NHOMHANGID"]);
            //    nt1 = e.Node.GetValue(treeList1.Columns["NHOM_TEN"]);
            //    dgv1 = e.Node.GetValue(treeList1.Columns["DON_GIA_VON"]);
            //    dgm1 = e.Node.GetValue(treeList1.Columns["DON_GIA_MUA"]);
            //    dgb1 = e.Node.GetValue(treeList1.Columns["DON_GIA_BAN"]);
            //    dgc1 = e.Node.GetValue(treeList1.Columns["DON_GIA_CAM"]);
            //    nhid = Int32.Parse(nhid1.ToString());
            //    nt = nt1.ToString();
            //    dgv = Decimal.Parse(dgv1.ToString());
            //    dgm = Decimal.Parse(dgm1.ToString());
            //    dgb = Decimal.Parse(dgb1.ToString());
            //    dgc = Decimal.Parse(dgc1.ToString());
            //}
            //catch { }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn xóa? ", "Thông báo", MessageBoxButtons.YesNo))
                {
                    cn.openconnection();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandType = CommandType.Text;
                    string sql = qr.querydeletepq_group();

                    cmd.CommandText = sql;
                    cmd.Connection = cn.conn;
                    cmd.Parameters.Add(qr.GROUP_ID, MySqlDbType.Int32).Value = groupid1;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        frmKho_Load(sender, e);
                        XtraMessageBox.Show("Đã xóa");              
                    }
                    else
                    {
                        XtraMessageBox.Show("Xóa thất bại");
                    }
                }
            }
            catch { }
        }
        bool c = false;
        private void gvNhomVang_RowClick(object sender, RowClickEventArgs e)
        {
            try
            {
                GridView view = (GridView)sender;
                if (view.FocusedRowHandle == vt)
                {
                    return;
                }
                else
                {
                    c = true;
                    object groupid;
                    object groupma;
                    object groupten;
                    object bikhoa;
                    object lydokhoa;
                    object sudung;
                    object ngaytao;
                    groupid = view.GetRowCellValue(view.FocusedRowHandle, "GROUP_ID");
                    groupma = view.GetRowCellValue(view.FocusedRowHandle, "GROUP_MA");
                    groupten = view.GetRowCellValue(view.FocusedRowHandle, "GROUP_TEN");
                    bikhoa = view.GetRowCellValue(view.FocusedRowHandle, "BIKHOA");
                    lydokhoa = view.GetRowCellValue(view.FocusedRowHandle, "LY_DO_KHOA");
                    sudung = view.GetRowCellValue(view.FocusedRowHandle, "SU_DUNG");
                    ngaytao = view.GetRowCellValue(view.FocusedRowHandle, "NGAY_TAO");
                    
                    groupid1 = Int32.Parse(groupid.ToString());
                    groupma1 = groupma.ToString();
                    groupten1 = groupten.ToString();
                    bikhoa1 = Boolean.Parse(bikhoa.ToString());
                    lydokhoa1 = lydokhoa.ToString();
                    sudung1 = Boolean.Parse(sudung.ToString());
                    ngaytao1 = DateTime.Parse(ngaytao.ToString());
                }
            }
            catch { }
        }

        public void frmKho_Load(object sender, EventArgs e)
        {
            loadLoaiVang();
            loadDefault();
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            frmPQ_THIETBI f = new frmPQ_THIETBI(groupid1);
            f.ShowDialog();
        }
    }
}
