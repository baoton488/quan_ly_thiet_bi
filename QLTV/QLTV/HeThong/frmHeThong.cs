﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Management;
using System.Drawing.Printing;
using DevExpress.XtraReports.UI;

namespace QLKhachHang.QLTV.HeThong
{
    public partial class frmHeThong : DevExpress.XtraEditors.XtraForm
    {
        public frmHeThong()
        {
            InitializeComponent();
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                //comboBoxEdit1.Items.Add(printer);
                comboBoxEdit1.Properties.Items.Add(printer);
            }
        }

        private void frmHeThong_Load(object sender, EventArgs e)
        {
            //var query = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            //var printers = query.Get();
            //foreach (ManagementObject printer in printers)
            //{
            //    if (printer["name"].ToString() == comboBox1.SelectedItem.ToString())
            //    {
            //        printer.InvokeMethod("SetDefaultPrinter", new object[] { comboBox1.SelectedItem.ToString() });
            //    }
            //}

            var printerQuery = new ManagementObjectSearcher("SELECT * from Win32_Printer");
            var a = printerQuery.Get();
            foreach (var printer in printerQuery.Get())
            {
                var name = printer.GetPropertyValue("Name");
                var status = printer.GetPropertyValue("Status");
                var isDefault = printer.GetPropertyValue("Default");
                var isNetworkPrinter = printer.GetPropertyValue("Network");
                var select = printer.GetPropertyValue("Selection");
                if (Boolean.Parse(isDefault.ToString()) == true)
                {
                    comboBox1.SelectedItem = name;
                }
            }
            
        }
        private PrintDocument printDoc = new PrintDocument();
        private void PopulateInstalledPrintersCombo()
        {
            int i;
            string pkInstalledPrinters;

            for (i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                comboBox1.Items.Add(pkInstalledPrinters);
                if (printDoc.PrinterSettings.IsDefaultPrinter)
                {
                    comboBox1.Text = printDoc.PrinterSettings.PrinterName;
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var query = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            var printers = query.Get();
            foreach (ManagementObject printer in printers)
            {
                //frmHangHoaSua f = new frmHangHoaSua(this,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
                //f.xr.PrinterName = comboBox1.SelectedItem.ToString();
                if (printer["name"].ToString() == comboBox1.SelectedItem.ToString())
                {
                    printer.InvokeMethod("SetDefaultPrinter", new object[] { comboBox1.SelectedItem.ToString() });
                }
            }
        }
    }
}