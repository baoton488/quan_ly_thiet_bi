﻿namespace QLKhachHang
{
    partial class frmPQ_GROUPAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtGroupTen = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ckBiKhoa = new DevExpress.XtraEditors.CheckEdit();
            this.mmLyDoKhoa = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckBiKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmLyDoKhoa.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên nhóm";
            // 
            // txtGroupTen
            // 
            this.txtGroupTen.Location = new System.Drawing.Point(97, 23);
            this.txtGroupTen.Name = "txtGroupTen";
            this.txtGroupTen.Size = new System.Drawing.Size(243, 20);
            this.txtGroupTen.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.ckBiKhoa);
            this.groupControl1.Controls.Add(this.mmLyDoKhoa);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.txtGroupTen);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(391, 189);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Thông tin Người dùng";
            // 
            // ckBiKhoa
            // 
            this.ckBiKhoa.Location = new System.Drawing.Point(97, 129);
            this.ckBiKhoa.Name = "ckBiKhoa";
            this.ckBiKhoa.Properties.Caption = "";
            this.ckBiKhoa.Size = new System.Drawing.Size(75, 19);
            this.ckBiKhoa.TabIndex = 2;
            // 
            // mmLyDoKhoa
            // 
            this.mmLyDoKhoa.Location = new System.Drawing.Point(97, 56);
            this.mmLyDoKhoa.Name = "mmLyDoKhoa";
            this.mmLyDoKhoa.Size = new System.Drawing.Size(243, 67);
            this.mmLyDoKhoa.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(304, 154);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "Lưu";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Bị khóa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lý do khóa";
            // 
            // frmPQ_GROUPAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 189);
            this.Controls.Add(this.groupControl1);
            this.MaximizeBox = false;
            this.Name = "frmPQ_GROUPAdd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Group";
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ckBiKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmLyDoKhoa.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtGroupTen;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit ckBiKhoa;
        private DevExpress.XtraEditors.MemoEdit mmLyDoKhoa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;

    }
}