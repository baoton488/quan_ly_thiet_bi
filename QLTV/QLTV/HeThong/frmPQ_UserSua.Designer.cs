﻿namespace QLKhachHang
{
    partial class frmPQ_UserSua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserTen = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.mmLyDoKhoa = new DevExpress.XtraEditors.MemoEdit();
            this.lookUpEditNhanVien = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditUserNhom = new DevExpress.XtraEditors.LookUpEdit();
            this.ckBiKhoa = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMatKhau = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mmLyDoKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditUserNhom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckBiKhoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatKhau.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Người dùng";
            // 
            // txtUserTen
            // 
            this.txtUserTen.Location = new System.Drawing.Point(106, 23);
            this.txtUserTen.Name = "txtUserTen";
            this.txtUserTen.Size = new System.Drawing.Size(247, 20);
            this.txtUserTen.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.simpleButton2);
            this.groupControl1.Controls.Add(this.mmLyDoKhoa);
            this.groupControl1.Controls.Add(this.lookUpEditNhanVien);
            this.groupControl1.Controls.Add(this.lookUpEditUserNhom);
            this.groupControl1.Controls.Add(this.ckBiKhoa);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.label6);
            this.groupControl1.Controls.Add(this.label5);
            this.groupControl1.Controls.Add(this.label3);
            this.groupControl1.Controls.Add(this.label4);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.txtMatKhau);
            this.groupControl1.Controls.Add(this.txtUserTen);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(384, 286);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Thông tin Người dùng";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(304, 251);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 7;
            this.simpleButton2.Text = "Lưu";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // mmLyDoKhoa
            // 
            this.mmLyDoKhoa.Location = new System.Drawing.Point(106, 143);
            this.mmLyDoKhoa.Name = "mmLyDoKhoa";
            this.mmLyDoKhoa.Size = new System.Drawing.Size(247, 66);
            this.mmLyDoKhoa.TabIndex = 4;
            // 
            // lookUpEditNhanVien
            // 
            this.lookUpEditNhanVien.Location = new System.Drawing.Point(106, 218);
            this.lookUpEditNhanVien.Name = "lookUpEditNhanVien";
            this.lookUpEditNhanVien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditNhanVien.Size = new System.Drawing.Size(247, 20);
            this.lookUpEditNhanVien.TabIndex = 5;
            this.lookUpEditNhanVien.Visible = false;
            // 
            // lookUpEditUserNhom
            // 
            this.lookUpEditUserNhom.Location = new System.Drawing.Point(106, 78);
            this.lookUpEditUserNhom.Name = "lookUpEditUserNhom";
            this.lookUpEditUserNhom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditUserNhom.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GROUP_ID", "Name1"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GROUP_MA", "Name2"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GROUP_TEN", "Name3")});
            this.lookUpEditUserNhom.Size = new System.Drawing.Size(247, 20);
            this.lookUpEditUserNhom.TabIndex = 2;
            // 
            // ckBiKhoa
            // 
            this.ckBiKhoa.Location = new System.Drawing.Point(106, 109);
            this.ckBiKhoa.Name = "ckBiKhoa";
            this.ckBiKhoa.Properties.Caption = "";
            this.ckBiKhoa.Size = new System.Drawing.Size(247, 19);
            this.ckBiKhoa.TabIndex = 3;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(223, 251);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "Lưu";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Nhân viên";
            this.label6.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Lý do khóa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Bị khóa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nhóm người dùng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mật khẩu";
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(106, 49);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.Properties.PasswordChar = '*';
            this.txtMatKhau.Size = new System.Drawing.Size(247, 20);
            this.txtMatKhau.TabIndex = 1;
            // 
            // frmPQ_UserSua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 286);
            this.Controls.Add(this.groupControl1);
            this.MaximizeBox = false;
            this.Name = "frmPQ_UserSua";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User";
            this.Load += new System.EventHandler(this.frmPQ_UserSua_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mmLyDoKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditUserNhom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckBiKhoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatKhau.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtUserTen;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit ckBiKhoa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txtMatKhau;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditUserNhom;
        private DevExpress.XtraEditors.MemoEdit mmLyDoKhoa;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditNhanVien;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;

    }
}