﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Configuration;
using Dapper;
using QLKhachHang.Query;
using System.Collections;

namespace QLTB.QLTV.HeThong
{
    public partial class frmPQ_THIETBI : DevExpress.XtraEditors.XtraForm
    {
        private int mid_group;
        
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        public frmPQ_THIETBI(int id_group)
        {
            InitializeComponent();
            mid_group = id_group;
        }

        private void frmPQ_THIETBI_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = connection.Query<NHOM_BO>("select * from NHOM_BO where SU_DUNG = @SU_DUNG", new { SU_DUNG = 1 }, commandType: CommandType.Text);
        }
        
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            gridControl1.EmbeddedNavigator.NavigatableControl.DoAction(DevExpress.XtraEditors.NavigatorButtonType.EndEdit);

            List<string> rows = new List<string>();
            for (int i = 0; i < gridView1.SelectedRowsCount; i++)
            {
                if (gridView1.GetSelectedRows()[i] >= 0)
                {
                    Console.WriteLine(gridView1.GetSelectedRows()[i]);
                    dynamic currentRow = gridView1.GetRow(gridView1.GetSelectedRows()[i]);
                    int a = currentRow.NHOM_BO_ID;
                    rows.Add(a.ToString());

                    //if (currentRow != null)
                    //{
                    //    System.Reflection.PropertyInfo pi = currentRow.GetType().GetProperty("NHOM_BO_ID");
                    //    String name = (String)(pi.GetValue(currentRow, null));
                    //    rows.Add(name);
                    //}                
                }
            }
            update(rows);
        }
        void update(List<string> r)
        {
            string sql = "UPDATE `PQ_GROUP` SET OPTION1 = @OPTION1 WHERE GROUP_ID = @GROUP_ID";
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString))
            {
                connection.Open();
                var list = new List<PQ_GROUP>();
                list.Add(new PQ_GROUP()
                {
                    GROUP_ID = mid_group,
                    OPTION1 = string.Join(",",r.ToArray())
                });
                var affectedRows = connection.Execute(sql,
                    list
                );
                if (affectedRows > 0)
                {

                }
            }
        }
    }
}