﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using System.Security.Cryptography;
using System.Configuration;
using Dapper;
using QLKhachHang.Query;

namespace QLKhachHang
{
    public partial class frmPQ_UserSua : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        string getgroup;
        Connect cn = new Connect();
        Query.QueryPQ_User qr = new Query.QueryPQ_User();
        Query.QueryPQ_GROUP qr_pq = new Query.QueryPQ_GROUP();
        public XRPictureBox picture = new XRPictureBox();
        private readonly frmPQ_User frm1;
        int userid1;
        int groupid1;
        string userma1;
        string userten1;
        string matkhau1;
        bool bikhoa1;
        string lydokhoa1;
        bool sudung1;
        DateTime ngaytao1;
        int maid;
        string encryptpasss;
        public frmPQ_UserSua(frmPQ_User frm,int userid,int groupid,string userma,string userten,string matkhau,bool bikhoa,string lydokhoa,DateTime ngaytao,bool sudung)
        {
            InitializeComponent();
            frm1 = frm;
            userid1 = userid;
            groupid1 = groupid;
            userma1 = userma;
            userten1 = userten;
            matkhau1 = matkhau;
            bikhoa1 = bikhoa;
            lydokhoa1 = lydokhoa;
            ngaytao1 = ngaytao;
            sudung1 = sudung;
        }

        void loadSua()
        {
            txtUserTen.Text = userten1;
            //txtMatKhau.Text = matkhau1;
            lookUpEditUserNhom.Properties.DataSource = qr_pq.getDataTable();
            lookUpEditUserNhom.Properties.DisplayMember = "GROUP_TEN";
            lookUpEditUserNhom.Properties.ValueMember = "GROUP_ID";

            lookUpEditNhanVien.Properties.DataSource = connection.Query<NHAN_VIEN>("select * from NHAN_VIEN where SU_DUNG = @SU_DUNG ", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhanVien.Properties.ValueMember = "MA_NHAN_VIEN";
            lookUpEditNhanVien.Properties.DisplayMember = "TEN_NHAN_VIEN";

            lookUpEditUserNhom.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            lookUpEditUserNhom.Properties.AutoSearchColumnIndex = 2;
            lookUpEditUserNhom.ItemIndex = groupid1 - 1;
         //   lookUpEditUserNhom.Properties.AutoSearchColumnIndex = groupid1;
            //lookUpEditUserNhom.ItemIndex = groupid1;
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //   Thread t = new Thread(new ThreadStart(StartForm));
                //   t.Start();
                //selectid();
                Encrypt();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                string sql = qr.queryupdatepq_user();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.USER_ID, MySqlDbType.Int32).Value = userid1;
                cmd.Parameters.Add(qr.USER_TEN, MySqlDbType.VarChar).Value = txtUserTen.Text;
                cmd.Parameters.Add(qr.MAT_KHAU, MySqlDbType.VarChar).Value = encryptpasss;
                if (ckBiKhoa.Checked == true)
                {
                    // ckNu.CheckState = unchecked;
                    cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 1;
                }
                else
                    cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 0;
                cmd.Parameters.Add(qr.LY_DO_KHOA, MySqlDbType.VarChar).Value = mmLyDoKhoa.Text;
                cmd.Parameters.Add(qr.GROUP_ID, MySqlDbType.Int32).Value = lookUpEditUserNhom.EditValue.ToString();
               // cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtKyHieu.Text;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    //f1.loadgv();
                    //    t.Abort();
                    frm1.frmKho_Load(sender, e);
                    XtraMessageBox.Show("Đã lưu");
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
                //Zen.Barcode.Code128BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                //picture.Image = barcode.Draw(txtTenNhom.Text, 50);
                //pictureBox1.Image = picture.Image;
                //pictureEdit1.Image = picture.Image;
            }
        }

        public void simpleButton2_Click(object sender, EventArgs e)
        {
            //Report.XtraReport3 rpt = new Report.XtraReport3();
            //rpt.Parameters["parameter1"].Value = txtUserTen.Text;
            ////ReportPrintTool rr
            //XtraSchedulerReport1 rpt2 = new XtraSchedulerReport1();
            ////rpt.DataSource = gload;
            ////rpt.DataMember = "";
            //rpt.CreateDocument();
            //rpt.ShowPreviewDialog();
        }
        void selectid()
        {
            try
            {
                int ma;
                //   Thread t = new Thread(new ThreadStart(StartForm));
                //   t.Start();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                string sql = "select COUNT(USER_ID) from pq_user where su_dung = 1";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    ma = rd.GetInt32(0);
                    maid = ma + 1;

                }
                rd.Close();

            }
            catch
            {

                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        void loadlookup()
        {
            lookUpEditUserNhom.Properties.DataSource = qr_pq.getDataTable();
            lookUpEditUserNhom.Properties.DisplayMember = "GROUP_TEN";
            lookUpEditUserNhom.Properties.ValueMember = "GROUP_ID";
        }

        string hash = "B@oKho@";
        void Encrypt()
        {
            byte[] data = UTF8Encoding.UTF8.GetBytes(txtMatKhau.Text);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(hash));
                using(TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider(){Key = key,Mode=CipherMode.ECB,Padding= PaddingMode.PKCS7})
                {
                    ICryptoTransform tranform = tripDes.CreateEncryptor();
                    byte[] result = tranform.TransformFinalBlock(data, 0, data.Length);
                    encryptpasss = Convert.ToBase64String(result, 0, result.Length);
                }
            }
        }

        private void frmPQ_UserSua_Load(object sender, EventArgs e)
        {
            selectidten();
            loadSua();
            //loadlookup();
        }
        void selectidten()
        {
            try
            {
                //   Thread t = new Thread(new ThreadStart(StartForm));
                //   t.Start();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                string sql = "select * from pq_group where GROUP_ID = '"+groupid1+"' and su_dung = 1";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    string ten = rd.GetString("GROUP_TEN");
                    getgroup = ten;
                }
                rd.Close();

            }
            catch
            {

                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            try
            {             
               // string salt = BCryptHelper.GenerateSalt(12);
                string hashedPassword = BCrypt.Net.BCrypt.HashPassword(txtMatKhau.Text);
                bool a = false;
                a = BCrypt.Net.BCrypt.Verify(txtMatKhau.Text, hashedPassword);
                if (a == false)
                {
                    XtraMessageBox.Show("Có lỗi thêm người dùng");
                    return;
                }
                else
                {
                    cn.openconnection();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandType = CommandType.Text;
                    //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                    //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                    string sql = qr.queryupdatepq_user();
                    cmd.CommandText = sql;
                    cmd.Connection = cn.conn;
                    cmd.Parameters.Add(qr.USER_ID, MySqlDbType.Int32).Value = userid1;
                    cmd.Parameters.Add(qr.USER_TEN, MySqlDbType.VarChar).Value = txtUserTen.Text;
                    cmd.Parameters.Add(qr.MAT_KHAU, MySqlDbType.VarChar).Value = hashedPassword;
                    if (ckBiKhoa.Checked == true)
                    {
                        // ckNu.CheckState = unchecked;
                        cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 1;
                    }
                    else
                        cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 0;
                    cmd.Parameters.Add(qr.LY_DO_KHOA, MySqlDbType.VarChar).Value = mmLyDoKhoa.Text;
                    cmd.Parameters.Add(qr.GROUP_ID, MySqlDbType.Int32).Value = lookUpEditUserNhom.EditValue.ToString();
                    // cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtKyHieu.Text;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        //f1.loadgv();
                        //    t.Abort();
                        frm1.frmKho_Load(sender, e);
                        XtraMessageBox.Show("Đã lưu");
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu thất bại");
                    }
                }               
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
                //Zen.Barcode.Code128BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                //picture.Image = barcode.Draw(txtTenNhom.Text, 50);
                //pictureBox1.Image = picture.Image;
                //pictureEdit1.Image = picture.Image;
            }
        }
    }
}