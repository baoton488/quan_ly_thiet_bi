﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;
using System.Security.Cryptography;
using BCrypt.Net;
using System.Configuration;
using QLKhachHang.Query;
using Dapper;


namespace QLKhachHang
{
    public partial class frmPQ_UserAdd : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MyAppConnection"].ConnectionString);
        Connect cn = new Connect();
        Query.QueryPQ_User qr = new Query.QueryPQ_User();
        Query.QueryPQ_GROUP qr_pq = new Query.QueryPQ_GROUP();
        public XRPictureBox picture = new XRPictureBox();
        private readonly frmPQ_User frm1;
        int maid;
        string encryptpasss;
        public frmPQ_UserAdd(frmPQ_User frm)
        {
            InitializeComponent();
            frm1 = frm;
        }

        

        public void simpleButton2_Click(object sender, EventArgs e)
        {

        }
        void selectid()
        {
            try
            {
                int ma;
                //   Thread t = new Thread(new ThreadStart(StartForm));
                //   t.Start();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                string sql = "select max(USER_ID) from pq_user";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    ma = rd.GetInt32(0);
                    maid = ma + 1;
                }
                else
                    maid = 1;
                rd.Close();

            }
            catch
            {
                maid = 1;
                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
        void loadlookup()
        {
            lookUpEditUserNhom.Properties.DataSource = qr_pq.getDataTable();
            lookUpEditUserNhom.Properties.DisplayMember = "GROUP_TEN";
            lookUpEditUserNhom.Properties.ValueMember = "GROUP_ID";

            lookUpEditNhanVien.Properties.DataSource = connection.Query<NHAN_VIEN>("select * from NHAN_VIEN where SU_DUNG = @SU_DUNG ", new { SU_DUNG = 1 }, commandType: CommandType.Text);
            lookUpEditNhanVien.Properties.ValueMember = "MA_NHAN_VIEN";
            lookUpEditNhanVien.Properties.DisplayMember = "TEN_NHAN_VIEN";
        }

        private void frmPQ_UserAdd_Load(object sender, EventArgs e)
        {
            loadlookup();
        }
        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            try
            {
                //selectid();
                //Encrypt();
                string hashedPassword = BCrypt.Net.BCrypt.HashPassword(txtMatKhau.Text);
                bool a = false;
                a = BCrypt.Net.BCrypt.Verify(txtMatKhau.Text, hashedPassword);
                if(a == false)
                {
                    XtraMessageBox.Show("Có lỗi thêm người dùng");
                    return;
                }
                else
                {
                    cn.openconnection();
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.CommandType = CommandType.Text;
                    string sql = qr.queryaddpq_user();
                    cmd.CommandText = sql;
                    cmd.Connection = cn.conn;
                    if (maid == 0)
                    {
                        cmd.Parameters.Add(qr.USER_MA, MySqlDbType.VarChar).Value = "1";
                    }
                    else
                    {
                        cmd.Parameters.Add(qr.USER_MA, MySqlDbType.VarChar).Value = maid.ToString();
                    }
                    cmd.Parameters.Add(qr.USER_TEN, MySqlDbType.VarChar).Value = txtUserTen.Text;
                    cmd.Parameters.Add(qr.MAT_KHAU, MySqlDbType.VarChar).Value = hashedPassword;
                    if (ckBiKhoa.Checked == true)
                    {
                        cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 1;
                    }
                    else
                        cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 0;
                    cmd.Parameters.Add(qr.LY_DO_KHOA, MySqlDbType.VarChar).Value = mmLyDoKhoa.Text;
                    cmd.Parameters.Add(qr.GROUP_ID, MySqlDbType.Int32).Value = lookUpEditUserNhom.EditValue.ToString();
                    cmd.Parameters.Add(qr.NGAY_TAO, MySqlDbType.DateTime).Value = DateTime.Now.Date;
                    cmd.Parameters.Add(qr.MA_NHAN_VIEN, MySqlDbType.Int32).Value = lookUpEditNhanVien.EditValue;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        frm1.frmKho_Load(sender, e);
                        XtraMessageBox.Show("Đã lưu");
                    }
                    else
                    {
                        XtraMessageBox.Show("Lưu thất bại");
                    }
                }
                
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        private void lookUpEditNhanVien_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}