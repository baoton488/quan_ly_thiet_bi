﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;

namespace QLKhachHang
{
    public partial class frmPQ_GROUPAdd : DevExpress.XtraEditors.XtraForm
    {
        Connect cn = new Connect();
        Query.QueryPQ_GROUP qr = new Query.QueryPQ_GROUP();
        public XRPictureBox picture = new XRPictureBox();
        private readonly frmPQ_GROUP frm1;
        int maid;
        public frmPQ_GROUPAdd(frmPQ_GROUP frm)
        {
            InitializeComponent();
            frm1 = frm;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //   Thread t = new Thread(new ThreadStart(StartForm));
                //   t.Start();
                selectid();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                string sql = qr.queryaddpq_group();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                if (maid == 0)
                {
                    cmd.Parameters.Add(qr.GROUP_MA, MySqlDbType.VarChar).Value = "1";
                }
                else
                {
                    cmd.Parameters.Add(qr.GROUP_MA, MySqlDbType.VarChar).Value = maid.ToString();
                }
                cmd.Parameters.Add(qr.GROUP_TEN, MySqlDbType.VarChar).Value = txtGroupTen.Text;
                if (ckBiKhoa.Checked == true)
                {
                    // ckNu.CheckState = unchecked;
                    cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 1;
                }
                else
                    cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 0;
                cmd.Parameters.Add(qr.LY_DO_KHOA, MySqlDbType.VarChar).Value = mmLyDoKhoa.Text;
               // cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtKyHieu.Text;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    //f1.loadgv();
                    //    t.Abort();
                    frm1.frmKho_Load(sender, e);
                    XtraMessageBox.Show("Đã lưu");
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
                //Zen.Barcode.Code128BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.Code128WithChecksum;
                //picture.Image = barcode.Draw(txtTenNhom.Text, 50);
                //pictureBox1.Image = picture.Image;
                //pictureEdit1.Image = picture.Image;
            }
        }

        public void simpleButton2_Click(object sender, EventArgs e)
        {
            //Report.XtraReport3 rpt = new Report.XtraReport3();
            //rpt.Parameters["parameter1"].Value = txtGroupTen.Text;
            ////ReportPrintTool rr
            //XtraSchedulerReport1 rpt2 = new XtraSchedulerReport1();
            ////rpt.DataSource = gload;
            ////rpt.DataMember = "";
            //rpt.CreateDocument();
            //rpt.ShowPreviewDialog();
        }
        void selectid()
        {
            try
            {
                int ma;
                //   Thread t = new Thread(new ThreadStart(StartForm));
                //   t.Start();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                string sql = "select max(GROUP_ID) from pq_group";
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                MySqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    ma = rd.GetInt32(0);
                    maid = ma + 1;
                }
                else
                    maid = 1;
                rd.Close();

            }
            catch
            {
                maid = 1;
                //  XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }
    }
}