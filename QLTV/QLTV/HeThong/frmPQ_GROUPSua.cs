﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using MySql.Data.MySqlClient;
using DevExpress.XtraReports.UI;

namespace QLKhachHang
{
    public partial class frmPQ_GROUPSua : DevExpress.XtraEditors.XtraForm
    {
        Connect cn = new Connect();
        Query.QueryPQ_GROUP qr = new Query.QueryPQ_GROUP();
        public XRPictureBox picture = new XRPictureBox();
        private readonly frmPQ_GROUP frm1;
        int groupid1;
        string groupma1;
        string groupten1;
        bool bikhoa1;
        string lydokhoa1;
        bool sudung1;
        DateTime ngaytao1;
        public frmPQ_GROUPSua(frmPQ_GROUP frm,int groupid,string groupma,string groupten,bool bikhoa,string lydokhoa,bool sudung,DateTime ngaytao)
        {
            InitializeComponent();
            frm1 = frm;
            groupid1 = groupid;
            groupma1 = groupma;
            groupten1 = groupten;
            bikhoa1 = bikhoa;
            lydokhoa1 = lydokhoa;
            sudung1 = sudung;
            ngaytao1 = ngaytao;
        }

        void loadSua()
        {
            txtGroupTen.Text = groupten1;
            mmLyDoKhoa.Text = lydokhoa1;
            ckBiKhoa.Checked = bikhoa1;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                //   Thread t = new Thread(new ThreadStart(StartForm));
                //   t.Start();
                cn.openconnection();
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandType = CommandType.Text;
                //string sql = "insert into nhom_hang(Nhom_Ten,Don_Gia_Von,Don_Gia_Mua,Don_Gia_Ban,Don_Gia_Cam) " +
                //                         " values(@nhomten,@dongiavon,@dongiamua,@dongiaban,@dongiacam) ";
                string sql = qr.queryupdatepq_group();
                cmd.CommandText = sql;
                cmd.Connection = cn.conn;
                cmd.Parameters.Add(qr.GROUP_ID, MySqlDbType.VarChar).Value = groupid1;
                cmd.Parameters.Add(qr.GROUP_TEN, MySqlDbType.VarChar).Value = txtGroupTen.Text;
                if (ckBiKhoa.Checked == true)
                {
                    // ckNu.CheckState = unchecked;
                    cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 1;
                }
                else
                    cmd.Parameters.Add(qr.BIKHOA, MySqlDbType.Int16).Value = 0;
                cmd.Parameters.Add(qr.LY_DO_KHOA, MySqlDbType.VarChar).Value = mmLyDoKhoa.Text;
               // cmd.Parameters.Add(qr.GHI_CHU, MySqlDbType.VarChar).Value = txtKyHieu.Text;
                if (cmd.ExecuteNonQuery() == 1)
                {
                    //f1.loadgv();
                    //    t.Abort();
                    frm1.frmKho_Load(sender, e);
                    XtraMessageBox.Show("Đã lưu");
                }
                else
                {
                    XtraMessageBox.Show("Lưu thất bại");
                }
            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
            finally
            {
                cn.closeconnection();
                cn.conn.Dispose();
            }
        }

        public void simpleButton2_Click(object sender, EventArgs e)
        {
            //Report.XtraReport3 rpt = new Report.XtraReport3();
            //rpt.Parameters["parameter1"].Value = txtGroupTen.Text;
            ////ReportPrintTool rr
            //XtraSchedulerReport1 rpt2 = new XtraSchedulerReport1();
            ////rpt.DataSource = gload;
            ////rpt.DataMember = "";
            //rpt.CreateDocument();
            //rpt.ShowPreviewDialog();
        }

        private void frmPQ_GROUPSua_Load(object sender, EventArgs e)
        {
            loadSua();
        }
    }
}