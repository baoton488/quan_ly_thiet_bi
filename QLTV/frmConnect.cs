﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Net;
using System.Net.Sockets;
using System.Data.SqlClient;
using System.Configuration;

namespace QLKhachHang
{
    public partial class frmConnect : DevExpress.XtraEditors.XtraForm
    {
        Connect cn = new Connect();
        public frmConnect()
        {
            
            InitializeComponent();
        }

        private void frmConnect_Load(object sender, EventArgs e)
        {
            try
            {
                string connectString = ConfigurationManager.ConnectionStrings["MyAppConnection"].ToString();
                
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder(connectString);
                // Retrieve the DataSource property.   
                string sv = builder.Server;
                string p = builder.Port.ToString();
                string db = builder.Database;
                string user = builder.UserID;
                string pw = builder.Password;
                txtServer.Text = sv;
                txtPort.Text = p;
                txtDb.Text = db;
                txtUser.Text = user;
                txtPassword.Text = pw;

            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                cn.server = txtServer.Text;
                cn.port = txtPort.Text;
                cn.database = txtDb.Text;
                cn.user_id = txtUser.Text;
                cn.password = txtPassword.Text;
                cn.connn();
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                cn.server = txtServer.Text;
                cn.port = txtPort.Text;
                cn.database = txtDb.Text;
                cn.user_id = txtUser.Text;
                cn.password = txtPassword.Text;
                cn.connn();
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
    }
}