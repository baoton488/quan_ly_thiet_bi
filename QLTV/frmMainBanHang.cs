﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraSplashScreen;
using QLKhachHang.QLTV;
using QLKhachHang.Default;
using QLKhachHang.QLTV.GiaoDichVang;
using QLKhachHang.QLTV.HeThong;
using QLTV;
using System.Diagnostics;
using QLTB.Properties;

namespace QLKhachHang
{
    public partial class frmMainBanHang : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        QuyenClass qc = new QuyenClass();
        public int check;
        SplashScreenManager splash = new SplashScreenManager();
        bool statesplash = false;
        private void StartForm()
        {
            Application.Run(new frmWait());
        }
        public frmMainBanHang()
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                InitializeComponent();

                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }
                
                ////       t.Abort();
                DevExpress.XtraBars.Helpers.SkinHelper.InitSkinGallery(skinRibbonGalleryBarItem1, true, true);
                skinRibbonGalleryBarItem1.GalleryItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(skinRibbonGalleryBarItem1_GalleryItemClick);
            }
            catch 
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }
                
            }
                //   Control.CheckForIllegalCrossThreadCalls = false;
        }
        
        private void AddTabControl(UserControl usercontrol, string itemTabname)
        {
            bool kq = false;
            foreach (XtraTabPage tabitem in xtraTabControl1.TabPages)
            {
                if (tabitem.Text == itemTabname)
                {
                    kq = true;
                    xtraTabControl1.SelectedTabPage = tabitem;
                }
            }
            if (kq == false)
            {
                Addtab addtab = new Addtab();
                addtab.AddtabControl(xtraTabControl1, itemTabname, usercontrol);
            }
        }
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    //frmKhachHang f = new frmKhachHang();
            //    //AddTabControl(f, "Khách hàng");
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        //  splashScreenManager.ShowWaitForm();
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    //     Thread t = new Thread(new ThreadStart(StartForm));
            //    //     t.Start();
            //    frmKhachHang f = new frmKhachHang();
            //    AddTabControl(f, "Khách hàng");
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        //  splashScreenManager.ShowWaitForm();
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    //      Thread t = new Thread(new ThreadStart(StartForm));
            //    //      t.Start();
            //    frmGDWUAll f = new frmGDWUAll();
            //    AddTabControl(f, "Giao dịch");
            //    //      t.Abort();
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
        }

        private void xtraTabControl1_ControlAdded(object sender, ControlEventArgs e)
        {
            xtraTabControl1.SelectedTabPageIndex = xtraTabControl1.TabPages.Count - 1;
        }

        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)
        {
            xtraTabControl1.TabPages.RemoveAt(xtraTabControl1.SelectedTabPageIndex);
            xtraTabControl1.SelectedTabPageIndex = xtraTabControl1.TabPages.Count - 1;
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        //  splashScreenManager.ShowWaitForm();
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    //      Thread t = new Thread(new ThreadStart(StartForm));
            //    //       t.Start();
            //    frmTangQua f = new frmTangQua();
            //    AddTabControl(f, "Tặng quà");
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        //  splashScreenManager.ShowWaitForm();
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    //     Thread t = new Thread(new ThreadStart(StartForm));
            //    //     t.Start();
            //    frmTangQuaKH f = new frmTangQuaKH();
            //    AddTabControl(f, "Tặng quà khách hàng");
            //    //     t.Abort();
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    frmDsKhNhanQua f = new frmDsKhNhanQua();
            //    AddTabControl(f, "Ds khách hàng đã nhận quà");
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
        }

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    frmDsKhChuaTang f = new frmDsKhChuaTang();
            //    AddTabControl(f, "Ds khách hàng chưa nhận quà");
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }
                
            //}
        }

        string FileName = "UserSettings.txt";
        private void SaveSettings(UserSettings us)
        {
            BinaryFormatter binFormat = new BinaryFormatter();
            using (Stream fStream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, us);
                fStream.Close();
            }
        }

        private void skinRibbonGalleryBarItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            UserSettings us = new UserSettings();
            us.SkinName = DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveSkinName;
            SaveSettings(us);
        }
        private void skinRibbonGalleryBarItem1_GalleryItemClick(object sender, GalleryItemClickEventArgs e)
        {
            UserSettings us = new UserSettings();
            us.SkinName = DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveSkinName;
            SaveSettings(us);
        }

        [Serializable]
        public class UserSettings
        {
            public string SkinName;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if(check == 2)
            {
                ribbonPage1.Visible = false;
            }
            if (File.Exists(FileName))
                DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(LoadSettings(FileName).SkinName);
        }

        private UserSettings LoadSettings(string fileName)
        {
            UserSettings us = null;
            BinaryFormatter binFormat = new BinaryFormatter();
            Stream fStream = new FileStream(fileName, FileMode.Open);
            try { us = binFormat.Deserialize(fStream) as UserSettings; }
            finally { fStream.Close(); }
            return us;
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            //try
            //{
            //    if (!splash.IsSplashFormVisible)
            //    {
            //        //  splashScreenManager.ShowWaitForm();
            //        SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
            //    }
            //    statesplash = true;
            //    //      Thread t = new Thread(new ThreadStart(StartForm));
            //    //       t.Start();
            //    frmConnect f = new frmConnect();
            //    AddTabControl(f, "Kết nối");
            //    //     t.Abort();
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }

            //}
            //catch
            //{
            //    if (splash.IsSplashFormVisible || statesplash == true)
            //    {
            //        SplashScreenManager.CloseForm();
            //        statesplash = false;
            //    }

            //}
        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem10_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem11_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem12_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem15_ItemClick(object sender, ItemClickEventArgs e)
        {
           
        }

        private void barButtonItem16_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmPQ_GROUP f = new frmPQ_GROUP();
                AddTabControl(f, "Nhóm Người Dùng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }
               
            }
        }

        private void barButtonItem17_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!splash.IsSplashFormVisible)
                {
                    //  splashScreenManager.ShowWaitForm();
                    SplashScreenManager.ShowForm(this, typeof(frmWait), true, true);
                }
                statesplash = true;
                //     Thread t = new Thread(new ThreadStart(StartForm));
                //    t.Start();
                frmPQ_User f = new frmPQ_User();
                AddTabControl(f, "Người Dùng");
                //     t.Abort();
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
            catch
            {
                if (splash.IsSplashFormVisible || statesplash == true)
                {
                    SplashScreenManager.CloseForm();
                    statesplash = false;
                }

            }
        }

        private void barButtonItem18_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem19_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            frmDangNhap f = new frmDangNhap();
            f.Close();
        }

        private void barButtonItem20_ItemClick(object sender, ItemClickEventArgs e)
        {          

        }

        private void barButtonItem21_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmHeThong f = new frmHeThong();
            f.Show();
        }

        private void barButtonItem22_ItemClick(object sender, ItemClickEventArgs e)
        {
             
        }

        private void barButtonItem23_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem24_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem25_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem26_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem27_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem28_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem30_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem31_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void barButtonItem29_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }
        ImageList ListImage;
        int count = 0;
        int i = 0;
        private ImageList Split(Bitmap image, int width, int height)
        {
            ImageList rows = new ImageList();
            rows.ImageSize = new Size(width, height);
            rows.Images.AddStrip(image);
            ImageList cells = new ImageList();
            cells.ImageSize = new Size(width, height);
            foreach (Image row in rows.Images)
            {
                cells.Images.AddStrip(row);
            }
            return cells;
        }
        int s = 0;

        private void timer2_Tick(object sender, EventArgs e)
        {
            pictureBox1.Image = ListImage.Images[i];
            i++;
            if (i == count) { i = 0; }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            s++;
            if (s == 3)
            {
                Thread.Sleep(500);
                timer1.Enabled = false;
                Process.GetCurrentProcess().Kill();
            }
        }

        private void frmMainBanHang_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = true;
            ListImage = Split(Resources.bye, 130, 130);
            count = ListImage.Images.Count;
            //label1.Visible = false;
            xtraTabControl1.Visible = false;
            //pictureBox2.Visible = false;
            //this.CenterToScreen();
            pictureBox1.Location = new Point((this.Width / 2) - (pictureBox1.Width / 2),
                      (this.Height / 2) - (pictureBox1.Height / 2));
            pictureBox1.Refresh();
            this.FormBorderStyle = FormBorderStyle.None;
            this.TransparencyKey = SystemColors.Control;
            timer2.Enabled = true;
            e.Cancel = true;
        }
    }
}