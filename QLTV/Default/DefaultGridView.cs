﻿using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLKhachHang.Default
{
    public class DefaultGridView
    {
        public static void SetDefaultGridView(GridView gv)
        {
            gv.OptionsBehavior.ReadOnly = true;
            //gv.OptionsBehavior.Editable = false;
            gv.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            gv.OptionsSelection.EnableAppearanceFocusedCell = false;
        }

    }
}
