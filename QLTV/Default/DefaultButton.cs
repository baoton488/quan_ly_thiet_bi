﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System.Drawing;

namespace QLKhachHang.Default
{
    public class DefaultButton
    {
        public static void SetDefaultButton(DevExpress.XtraEditors.SimpleButton btnThem, DevExpress.XtraEditors.SimpleButton btnSua, DevExpress.XtraEditors.SimpleButton btnXoa, DevExpress.XtraLayout.LayoutControlItem layout)
        {
            btnThem.Text = "Thêm";
            btnSua.Text = "Sửa";
            btnXoa.Text = "Xóa";
            //btn.Text = "Thêm mới";
            //btn.Visible = false;
            //btn.Size.Height = 22;
            //btn.Size.Width = 54;
            layout.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            layout.SizeConstraintsType = SizeConstraintsType.Custom;
            layout.MinSize = new System.Drawing.Size(33, 26);
            layout.MaxSize = new System.Drawing.Size(0, 26);
        }

    }
}
