/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     08/10/2020 2:40:06 PM                        */
/*==============================================================*/



/*==============================================================*/
/* Table: BO_THIET_BI                                           */
/*==============================================================*/
create table BO_THIET_BI
(
   MA_BO                int not null auto_increment,
   NHOM_BO_ID           int,
   TEN_BO               varchar(50),
   GHI_CHU              varchar(100),
   MUC_DO_SU_DUNG       int,
   NGAY_SU_DUNG         datetime,
   SU_DUNG              smallint,
   SO_LAN_SU_DUNG       bigint,
   TRANG_THAI           int comment 'trang thai hien tai cua bo thiet bi',
   primary key (MA_BO)
);

/*==============================================================*/
/* Table: LICH_SU_KIEM_TRA_THIET_BI                             */
/*==============================================================*/
create table LICH_SU_KIEM_TRA_THIET_BI
(
   ID                   int not null auto_increment,
   MA_BO_THIET_BI       int,
   TEN_BO_THIET_BI      varchar(100),
   NGAY                 datetime,
   GHI_CHU              varchar(200),
   IS_HOAN_TAT          bool,
   primary key (ID)
);

/*==============================================================*/
/* Table: NHAN_VIEN                                             */
/*==============================================================*/
create table NHAN_VIEN
(
   MA_NHAN_VIEN         int not null auto_increment,
   TEN_NHAN_VIEN        varchar(100),
   GIOI_TINH            bool,
   NGAY_SINH            date,
   GHI_CHU              varchar(100),
   SU_DUNG              bool,
   primary key (MA_NHAN_VIEN)
);

/*==============================================================*/
/* Table: NHAT_KY_SU_DUNG                                       */
/*==============================================================*/
create table NHAT_KY_SU_DUNG
(
   ID_NK_SD             int not null auto_increment,
   USER_ID              int,
   TEN_CA_MO            varchar(100),
   NGAY                 datetime default CURRENT_TIMESTAMP,
   TEN_BAC_SI           varchar(100),
   PHONG                varchar(100),
   GHI_CHU              varchar(100),
   NGUOI_NHAN           varchar(100),
   IS_SU_DUNG           bool,
   NGAY_TRA             datetime,
   IS_DAT               bool,
   TEN_BO               varchar(100),
   primary key (ID_NK_SD)
);

alter table NHAT_KY_SU_DUNG comment 'nhat ky cua bo thiet bi';

/*==============================================================*/
/* Table: NHAT_KY_SU_DUNG_CHI_TIET                              */
/*==============================================================*/
create table NHAT_KY_SU_DUNG_CHI_TIET
(
   ID_NK_SD             int,
   MA_BO                int,
   TEN_BO               varchar(100),
   IS_DAT               int,
   NGAY_UPDATE          datetime,
   TRANG_THAI           int comment 'trang thai tai thoi diem giao nhan'
);

/*==============================================================*/
/* Table: NHAT_KY_SU_DUNG_THIET_BI                              */
/*==============================================================*/
create table NHAT_KY_SU_DUNG_THIET_BI
(
   MA_THIET_BI          int,
   ID_NK_SDTB           int not null auto_increment,
   ID_NK_SD             int,
   USER_ID              int,
   KIEU_QUET            int,
   NGAY_TAO             timestamp,
   NGAY_TREN_THIET_BI   datetime,
   TEN_THIET_BO         varchar(100),
   TRANG_THAI_THIET_BI  int comment 'vd: hu hong, d?i tr?',
   primary key (ID_NK_SDTB)
);

/*==============================================================*/
/* Table: NHOM_BO                                               */
/*==============================================================*/
create table NHOM_BO
(
   NHOM_BO_ID           int not null auto_increment,
   NHOM_BO_TEN          varchar(100),
   SU_DUNG              bool,
   primary key (NHOM_BO_ID)
);

/*==============================================================*/
/* Table: NHOM_THIET_BI                                         */
/*==============================================================*/
create table NHOM_THIET_BI
(
   MA_NTB               int not null auto_increment,
   TEN_NTB              varchar(50),
   SU_DUNG              bool,
   primary key (MA_NTB)
);

/*==============================================================*/
/* Table: PHONG                                                 */
/*==============================================================*/
create table PHONG
(
   ID                   int not null auto_increment,
   TEN_PHONG            varchar(100),
   GHI_CHU              varchar(100),
   SU_DUNG              bool,
   primary key (ID)
);

/*==============================================================*/
/* Table: PQ_GROUP                                              */
/*==============================================================*/
create table PQ_GROUP
(
   GROUP_ID             int not null auto_increment,
   GROUP_MA             varchar(50),
   GROUP_TEN            national varchar(50),
   BIKHOA               bool,
   LY_DO_KHOA           national varchar(50),
   SU_DUNG              bool,
   NGAY_TAO             datetime,
   primary key (GROUP_ID)
);

/*==============================================================*/
/* Table: PQ_USER                                               */
/*==============================================================*/
create table PQ_USER
(
   USER_ID              int not null auto_increment,
   GROUP_ID             int,
   USER_MA              varchar(50),
   USER_TEN             national varchar(50),
   MAT_KHAU             national varchar(100),
   BIKHOA               bool,
   LY_DO_KHOA           national varchar(50),
   NGAY_TAO             datetime,
   SU_DUNG              bool,
   REALM                varchar(32),
   EMAIL                varchar(32),
   EMAILVERIFIED        varchar(32),
   VERIFICATIONTOKEN    varchar(100),
   MAC                  varchar(20),
   primary key (USER_ID)
);

/*==============================================================*/
/* Table: THIET_BI                                              */
/*==============================================================*/
create table THIET_BI
(
   MA_THIET_BI          int not null auto_increment,
   ID                   int,
   MA_BO                int,
   MA_NTB               int,
   THIET_BI_MA          varchar(50),
   TEN_THIET_BI         varchar(100),
   QUY_CACH             varchar(100),
   NGAY                 datetime,
   GHI_CHU              varchar(100),
   ANH_THIET_BI         longblob,
   SO_LAN_SU_DUNG       int,
   SU_DUNG              smallint,
   VI_TRI_KHO           varchar(100),
   primary key (MA_THIET_BI)
);

/*==============================================================*/
/* Table: TON_KHO                                               */
/*==============================================================*/
create table TON_KHO
(
   MA_THIET_BI          int,
   MA_BO                int,
   THIET_BI_MA          varchar(50),
   SL_TON               int,
   ID                   int,
   SL_SU_DUNG           int
);

alter table BO_THIET_BI add constraint FK_REFERENCE_8 foreign key (NHOM_BO_ID)
      references NHOM_BO (NHOM_BO_ID) on delete restrict on update restrict;

alter table NHAT_KY_SU_DUNG add constraint FK_REFERENCE_9 foreign key (USER_ID)
      references PQ_USER (USER_ID) on delete restrict on update restrict;

alter table NHAT_KY_SU_DUNG_CHI_TIET add constraint FK_REFERENCE_10 foreign key (ID_NK_SD)
      references NHAT_KY_SU_DUNG (ID_NK_SD) on delete restrict on update restrict;

alter table NHAT_KY_SU_DUNG_CHI_TIET add constraint FK_REFERENCE_11 foreign key (MA_BO)
      references BO_THIET_BI (MA_BO) on delete restrict on update restrict;

alter table NHAT_KY_SU_DUNG_THIET_BI add constraint FK_REFERENCE_12 foreign key (MA_THIET_BI)
      references THIET_BI (MA_THIET_BI) on delete restrict on update restrict;

alter table NHAT_KY_SU_DUNG_THIET_BI add constraint FK_REFERENCE_13 foreign key (ID_NK_SD)
      references NHAT_KY_SU_DUNG (ID_NK_SD) on delete restrict on update restrict;

alter table NHAT_KY_SU_DUNG_THIET_BI add constraint FK_REFERENCE_14 foreign key (USER_ID)
      references PQ_USER (USER_ID) on delete restrict on update restrict;

alter table PQ_USER add constraint FK_REFERENCE_5 foreign key (GROUP_ID)
      references PQ_GROUP (GROUP_ID) on delete restrict on update restrict;

alter table THIET_BI add constraint FK_REFERENCE_7 foreign key (MA_NTB)
      references NHOM_THIET_BI (MA_NTB) on delete restrict on update restrict;

alter table THIET_BI add constraint FK_RELATIONSHIP_1 foreign key (MA_BO)
      references BO_THIET_BI (MA_BO) on delete restrict on update restrict;

alter table TON_KHO add constraint FK_REFERENCE_4 foreign key (MA_THIET_BI)
      references THIET_BI (MA_THIET_BI) on delete cascade on update cascade;

alter table TON_KHO add constraint FK_REFERENCE_6 foreign key (MA_BO)
      references BO_THIET_BI (MA_BO) on delete restrict on update restrict;

